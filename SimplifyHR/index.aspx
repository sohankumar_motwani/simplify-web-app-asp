﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Simplify: Breaking employment barriers</title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/paper-bootstrap-wizard.css" rel="stylesheet" />
    <link href="Content/font-awesome.min.css" rel="stylesheet" />
    <link href="css/mdb.min.css" rel="stylesheet" />
    <link href="Content/materialize.min.css" rel="stylesheet" />

    <link href="css/AutoComplete.css" rel="stylesheet" />
</head>
<body style="background-color: snow">
   <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '114395319284537', // App ID
                status: true, // check login status
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true  // parse XFBML
            });
            // Additional initialization code here
            FB.Event.subscribe('auth.authResponseChange', function (response) {
                if (response.status === 'connected') {
                    // the user is logged in and has authenticated your
                    // app, and response.authResponse supplies
                    // the user's ID, a valid access token, a signed
                    // request, and the time the access token 
                    // and signed request each expire
                    var uid = response.authResponse.userID;
                    var accessToken = response.authResponse.accessToken;

                    // TODO: Handle the access token
                    // Do a post to the server to finish the logon
                    // This is a form post since we don't want to use AJAX
                    var form = document.createElement("form");
                    form.setAttribute("method", 'post');
                    form.setAttribute("action", 'User/FacebookLogin.ashx');

                    var field = document.createElement("input");
                    field.setAttribute("type", "hidden");
                    field.setAttribute("name", 'accessToken');
                    field.setAttribute("value", accessToken);
                    form.appendChild(field);

                    document.body.appendChild(form);
                    form.submit();

                } else if (response.status === 'not_authorized') {
                    // the user is logged in to Facebook, 
                    // but has not authenticated your app
                    FB.login(function (response) {
                        if (response.authResponse) {
                            // user sucessfully logged in
                            var accessToken = response.authResponse.accessToken;
                            // TODO: Handle the access token
                            // Do a post to the server to finish the logon
                            // This is a form post since we don't want to use AJAX
                            $("#btnFbLogin").click(function () {
                                var form = document.createElement("form");
                                form.setAttribute("method", 'post');
                                form.setAttribute("action", 'User/FacebookLogin.ashx');

                                var field = document.createElement("input");
                                field.setAttribute("type", "hidden");
                                field.setAttribute("name", 'accessToken');
                                field.setAttribute("value", accessToken);
                                form.appendChild(field);

                                document.body.appendChild(form);
                                form.submit();
                            });
                        }
                    }, { scope: 'public_profile,email,user_posts' });

                } else {
                    // the user isn't logged in to Facebook.
                }
            });

        };

        // Load the SDK Asynchronously
        (function (d) {
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
        }(document));
    </script>
    <!--below code was taken from: https://developers.facebook.com/docs/facebook-login/web/login-button-->
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=114395319284537";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!--replaced code-->
    <!--<div class="fb-login-button" data-show-faces="true" data-width="400" data-max-rows="1"></div>-->

    <div class="full-bg-img flex-center">

        <form runat="server" class="form-inline">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>


            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog" id="modal" style="z-index: 1000;">
                    <div class="modal-content" id="msform">
                        <div class="modal-header">
                            <div class="modal-body" id="myWizard">

                                <%-- PROGRESS BAR --%>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 20%;">
                                    </div>
                                </div>

                                <%-- TABS --%>
                                <div class="tab-content">

                                    <%-- TAB 1 --%>
                                    <div class="tab-pane fade in active" id="step1">

                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-title">
                                                    <h5 class="center">Create an Account Now</h5>
                                                </div>
                                                <hr />
                                                <div class="form-group">
                                                    <label>Email <small>*</small></label>
                                                    <asp:TextBox ID="txtemail" TextMode="Email" runat="server" placeholder="loremipsum@gmail.com" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Password<small>*</small></label>
                                                    <asp:TextBox runat="server" TextMode="Password" CssClass="col-lg-10" ID="txtPWD" placeholder="*********" />
                                                </div>

                                                <div class="form-group">
                                                    <label>Confirm Password<small>*</small></label>
                                                    <asp:TextBox runat="server" TextMode="Password" CssClass="col-lg-7" ID="txtPWR" placeholder="*********" />
                                                    <asp:CompareValidator ID="cvPW" runat="server"
                                                        ControlToValidate="txtPWR" ControlToCompare="txtPWD"
                                                        Display="Dynamic" ErrorMessage="Password does not match"
                                                        ForeColor="Red" SetFocusOnError="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <a class="btn btn-default btn-lg next" style="background-color: #4C79CE;" href="#step2" data-toggle="tab" data-step="2">Continue</a>
                                    </div>
                                    <%-- END TAB 1 --%>

                                    <%-- TAB 2 --%>
                                    <div class="tab-pane fade" id="step2">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-title">
                                                    <h3 class="center">Personal</h3>
                                                    <h5 class="center">Please tell us more about yourself.</h5>
                                                </div>
                                                <hr />
                                                <div class="form-group">
                                                    <label>First Name <small>*</small></label>
                                                    <asp:TextBox ID="txtFName" runat="server" placeholder="Lorem" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Last Name<small>*</small></label>
                                                    <asp:TextBox runat="server" ID="txtLName" placeholder="Ipsum" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Middle<small></small></label>
                                                    <asp:TextBox runat="server" ID="txtMName" placeholder="Is" />
                                                </div>
                                                <div class="form-group">
                                                    <label>BirthDate<small>*</small></label>
                                                    <asp:TextBox TextMode="Date" runat="server" ID="txtBday" placeholder="7/11/1998" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Gender<small>*</small></label>
                                                    <asp:DropDownList CssClass="form-control" runat="server" ID="ddlgender">
                                                        <asp:ListItem>---Select Gender ---</asp:ListItem>
                                                        <asp:ListItem>Male</asp:ListItem>
                                                        <asp:ListItem>Female</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="form-group">
                                                    <label>Summary<small>*</small></label>
                                                    <asp:TextBox runat="server" TextMode="MultiLine" ID="txtsum" placeholder="Lorem Ipsum" />
                                                </div>
                                            </div>
                                        </div>
                                        <a class="btn btn-default btn-lg" href="#step1" data-toggle="tab" data-step="1">previous</a>
                                        <a class="btn btn-default btn-lg next" style="background-color: #4C79CE;" href="#step3" data-toggle="tab" data-step="3">Continue</a>


                                    </div>
                                    <%-- END TAB 2 --%>

                                    <%-- TAB 3 --%>
                                    <div class="tab-pane fade" id="step3">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-title">
                                                    <h3 class="center">Education</h3>
                                                    <h5 class="center">Tell us where you studied</h5>
                                                </div>
                                                <hr />
                                                <div class="form-group">
                                                    <label>Education Level<small>*</small></label>
                                                    <asp:DropDownList runat="server" ID="ddleduclvl" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                <div class="form-group">
                                                    <label>Primary Education<small>*</small></label>

                                                    <asp:TextBox ID="txtpri" runat="server" CssClass="col-lg-10" autocomplete="off" placeholder="De La Salle GreenHills" />
                                                    <ajax:AutoCompleteExtender ID="autoComplete1" runat="server"
                                                        EnableCaching="true"
                                                        BehaviorID="AutoCompleteEx"
                                                        MinimumPrefixLength="1"
                                                        TargetControlID="txtpri"
                                                        ServicePath="AutoComplete.asmx"
                                                        ServiceMethod="SearchCustomers"
                                                        CompletionInterval="10"
                                                        CompletionSetCount="20"
                                                        CompletionListCssClass="autocomplete_completionListElement"
                                                        CompletionListItemCssClass="autocomplete_listItem"
                                                        CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                        DelimiterCharacters=";, :"
                                                        ShowOnlyCurrentWordInCompletionListItem="true">
                                                    </ajax:AutoCompleteExtender>
                                                </div>
                                                <div class="form-group">
                                                    <label>Secondary Education<small>*</small></label>
                                                    <asp:TextBox runat="server" ID="txtsec" CssClass="col-lg-10" placeholder="De La Salle GreenHills" autocomplete="off" />
                                                    <ajax:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server"
                                                        EnableCaching="true"
                                                        BehaviorID="AutoCompleteEx1"
                                                        MinimumPrefixLength="1"
                                                        TargetControlID="txtsec"
                                                        ServicePath="AutoComplete.asmx"
                                                        ServiceMethod="SearchCustomers1"
                                                        CompletionInterval="10"
                                                        CompletionSetCount="20"
                                                        CompletionListCssClass="autocomplete_completionListElement"
                                                        CompletionListItemCssClass="autocomplete_listItem"
                                                        CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                        DelimiterCharacters=";, :"
                                                        ShowOnlyCurrentWordInCompletionListItem="true">
                                                    </ajax:AutoCompleteExtender>
                                                </div>

                                                <div class="form-group">
                                                    <label>College<small>*</small></label>
                                                    <asp:TextBox runat="server" ID="txtcol" CssClass="col-lg-10" placeholder="De La Salle - College Of Saint Benilde" autocomplete="off" />
                                                    <ajax:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server"
                                                        EnableCaching="true"
                                                        BehaviorID="AutoCompleteEx2"
                                                        MinimumPrefixLength="1"
                                                        TargetControlID="txtcol"
                                                        ServicePath="AutoComplete.asmx"
                                                        ServiceMethod="SearchCustomers"
                                                        CompletionInterval="10"
                                                        CompletionSetCount="20"
                                                        CompletionListCssClass="autocomplete_completionListElement"
                                                        CompletionListItemCssClass="autocomplete_listItem"
                                                        CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                        DelimiterCharacters=";, :"
                                                        ShowOnlyCurrentWordInCompletionListItem="true">
                                                    </ajax:AutoCompleteExtender>
                                                </div>
                                                <div class="form-group">
                                                    <label>Degree Type<small>*</small></label>
                                                    <asp:DropDownList runat="server" ID="ddldegree" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                <div class="form-group">
                                                    <label>Course<small>*</small></label>
                                                    <asp:TextBox runat="server" ID="txtcourse" CssClass="col-lg-10" placeholder="Information System" />
                                                </div>
                                                <div class="form-group">
                                                    <asp:CheckBox runat="server" ID="chxbxpolicy" Text="terms and Conditions" />
                                                    <a href="privacy.aspx">Click here</a>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="btn btn-default btn-lg" href="#step2" data-toggle="tab" data-step="2">previous</a>
                                        <asp:Button runat="server" ID="btnSubmit" OnClick="btnsignup_Click" class='btn btn-fill btn-wd' Style="background-color: #4C79CE;" name='finish' Text="Submit" />
                                    </div>
                                    <%-- END TAB 3 --%>
                                </div>
                                <%-- END TABS --%>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="animate-bottom">
                <div class="card" style="background-color: white; padding-left: 20px; padding-right: 40px;">

                    <div class="col-lg-6 col-lg-offset-3">
                        <br />
                        <img src="../images/SimplifyHR(CroppedTransBg).png" />
                    </div>
                    <br />
                    <br />
                    <br />
                    <br />
                    <hr />
                    <asp:Label CssClass="red-text" runat="server" ID="error" Visible="false">Email or Password is incorrect</asp:Label>
                    <br />
                    <div class="row">
                        <div class="row">
                            <div class="input-field col s6">
                                <i class="fa fa-user prefix" style="color: #4C79CE;"></i>
                                <asp:TextBox runat="server" ID="Luser" TextMode="Email" CssClass="black-text form-control"></asp:TextBox>
                                <label for="Luser">Email</label>
                            </div>
                            <div class="input-field col s6">
                                <i class="fa fa-key prefix" style="color: #4C79CE;"></i>
                                <asp:TextBox runat="server" TextMode="Password" ID="Lpass" CssClass="black-text form-control"></asp:TextBox>
                                <label for="Lpass">Password </label>
                            </div>
                        </div>
                    </div>
                    <br />
                    <a class="btn btn-lg" style="background-color: #4C79CE; align-content: center;" data-toggle="modal" data-target="#myModal">Sign up!</a>
                    <asp:Button runat="server" ID="login1" CssClass="btn" Style="background-color: #4C79CE; align-content: center;" Text="login" OnClick="login1_Click" />
                    <br />

                    <div class="fb-login-button" data-max-rows="1" scope="public_profile,email,user_posts" data-size="large" data-button-type="continue_with" data-show-faces="false" onclick="checkLoginState();" data-auto-logout-link="false" data-use-continue-as="false"></div>
             <%-- <div id="btnConnect" class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="true" scope="public_profile,email,user_about_me,user_posts"></div>
 --%>   </div>
            </div>
        </form>
    </div>

    <script src="Scripts/jquery-3.1.1.js"></script>
    <script src="Scripts/tether.min.js"></script>
    <script src="Scripts/bootstrap.js"></script>
    <script src="Scripts/mdb.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js" type="text/javascript"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/themes/blitzer/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <script>
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

            //update progress
            var step = $(e.target).data('step');
            var percent = (parseInt(step) / 3) * 100;

            $('.progress-bar').css({ width: percent + '%' });
            $('.progress-bar').text("Step " + step + " of 5");

            //e.relatedTarget // previous tab

        })

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

            //update progress
            var step = $(e.target).data('step');
            var percent = (parseInt(step) / 5) * 100;

            $('.progress-bar').css({ width: percent - '%' });
            $('.progress-bar').text("Step " + step + " of 5");

            //e.relatedTarget // previous tab

        })

        $('.first').click(function () {

            $('#myWizard a:first').tab('show')

        })
    </script>



</body>
</html>
