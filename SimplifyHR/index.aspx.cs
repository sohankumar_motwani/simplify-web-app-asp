﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        educlevel();
        degree();
    }

    void educlevel()
    {
        String SQL = @"Select EducationLvlId,EducationLvlName from tbl_EducationLvl ";
        Helper.Select(SQL, ddleduclvl, "EducationLvlName", "EducationLvlId");
    }

    void degree()
    {
        String SQL = @"Select DegreeId,DegreeType from tbl_Degree ";
        Helper.Select(SQL, ddldegree, "DegreeType", "DegreeId");

    }


    protected void login1_Click(object sender, EventArgs e)
    {
        string SQL = @"SELECT ApplicantID FROM tbl_Applicant WHERE Email = @Email AND Password = @Password AND IsActivated = 1";
        String returnedValue = Helper.Login(SQL, "ApplicantID", "@Email", Luser.Text, "@Password", Lpass.Text);
        if (returnedValue != null)
        {
            Session["ApplicantID"] = returnedValue;
            Response.Redirect("User/Home.aspx");
        }
        else
        {
            error.Visible = true;
        }

    }

    protected void btnsignup_Click(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            string sql = @"select count(Email) as Email from tbl_Applicant where Email= '" + txtemail.Text + "'";
            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
                if (txtemail.Text == null || txtPWD.Text == null || txtFName.Text == null || txtLName.Text == null || txtBday.Text == null || txtsum.Text == null || txtpri.Text == null || txtsec == null || txtcol.Text == null)
                {
                    Response.Write("<script>alert('All Fields are required. Thank You')</script>");
                }
                else
                {
                    string iemail = cmd.ExecuteScalar().ToString();
                    if (iemail == "0")
                    {
                        //TBL_Applicant
                        string[] cols = new string[] { "@Email", "@Password", "@FirstName", "@LastName", "@MiddleName", "@Birthdate", "@Gender", "@Summary", "@ProfilePicture","@Result", "@IsActivated" };
                        string[] txt = new string[] { txtemail.Text, Helper.CreateSHAHash(txtPWD.Text), txtFName.Text, txtLName.Text, txtMName.Text, txtBday.Text, ddlgender.SelectedValue, txtsum.Text, "defaultpic.jpg", "0", "0" };
                        string[] tbcols = new string[] { "Email", "Password", "FirstName", "LastName", "MiddleName", "Birthdate", "Gender", "Summary", "ProfilePicture","Result", "IsActivated" };
                        string applicant = Convert.ToString(Helper.InsertScope("tbl_Applicant", cols, txt, tbcols));

                        string message = "<h1>Thank you for registering with Simplify!</h1><br><p>In order to proceed with your registration, please click <a href='http://simplifyhr.azurewebsites.net/confirmation.aspx?id=" + applicant + "'>here</a>.</p><br><p>Best regards,<br>The Simplify Team";
                        Helper.SendEmail(txtemail.Text, "Account Activation", message);

                        //TBL_School
                        string[] cols1 = new string[] { "@SchoolName" };
                        string[] txt1 = new string[] { txtpri.Text, };
                        string[] tbcols1 = new string[] { "SchoolName" };
                        string schoolid1 = Convert.ToString(Helper.InsertScope("tbl_School", cols1, txt1, tbcols1));

                        string[] cols2 = new string[] { "@SchoolName" };
                        string[] txt2 = new string[] { txtsec.Text, };
                        string[] tbcols2 = new string[] { "SchoolName" };
                        string schoolid2 = Helper.InsertScope("tbl_School", cols2, txt2, tbcols2);

                        string[] cols3 = new string[] { "@SchoolName" };
                        string[] txt3 = new string[] { txtcol.Text, };
                        string[] tbcols3 = new string[] { "SchoolName" };
                        string schoolid3 = Helper.InsertScope("tbl_School", cols3, txt3, tbcols3);

                        //TBL_Degree
                        //no longer needed because degree types must be predefined.
                        //predefined values (i.e. Bachelor, Master, Doctorate) must be pre-stored in the database table associated.
                        //string[] cols4 = new string[] { "@DegreeType" };
                        //string[] txt4 = new string[] { ddldegree.SelectedValue };
                        //string[] tbcols4 = new string[] { "DegreeType" };
                        //string degree = Helper.InsertScope("tbl_Degree", cols4, txt4, tbcols4);

                        //TBL_Course
                        string[] cols5 = new string[] { "@CourseName", "@DegreeId" };
                        string[] txt5 = new string[] { txtcourse.Text, ddldegree.SelectedValue };
                        string[] tbcols5 = new string[] { "CourseName", "DegreeId" };
                        string course = Helper.InsertScope("tbl_Course", cols5, txt5, tbcols5);

                        //System.Diagnostics.Debug.WriteLine(applicant + "," + schoolid1 + "," + schoolid2 + "," + schoolid3 + "," + degree + "," + course);


                        //TBL_EducLvl
                        string[] cols6 = new string[] { "@ApplicantID", "@EducLvID", "@SchoolID" };
                        string[] txt6 = new string[] { applicant, "1", schoolid1 };
                        string[] tbcols6 = new string[] { "ApplicantID", "EducLvID", "SchoolID" };
                        Helper.InsertScope("tbl_ApplicantEducLvl", cols6, txt6, tbcols6);

                        string[] cols7 = new string[] { "@ApplicantID", "@EducLvID", "@SchoolID" };
                        string[] txt7 = new string[] { applicant, "2", schoolid2 };
                        string[] tbcols7 = new string[] { "ApplicantID", "EducLvID", "SchoolID" };
                        Helper.InsertScope("tbl_ApplicantEducLvl", cols7, txt7, tbcols7);

                        string[] cols8 = new string[] { "@ApplicantID", "@EducLvID", "@SchoolID", "@CourseID" };
                        string[] txt8 = new string[] { applicant, "3", schoolid3, course };
                        string[] tbcols8 = new string[] { "ApplicantID", "EducLvID", "SchoolID", "CourseID" };
                        Helper.InsertScope("tbl_ApplicantEducLvl", cols8, txt8, tbcols8);

                        Response.Write("<script>alert('Signup Successful. Check your email for confirmation')</script>");
                    }
                    else if (iemail == "1")
                    {
                        Response.Write("<script>alert('Email already taken.')</script>");
                    }
                }
            }

        }

    }
}