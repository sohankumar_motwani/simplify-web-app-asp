﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class confirmation : System.Web.UI.Page
{
    public string id;
    protected void Page_Load(object sender, EventArgs e)
    {
        string id = Request.QueryString["id"].ToString();
        if (String.IsNullOrEmpty(id))
        {
            //display error message 
            lblFailed.Visible = true;
}//closing if
        else
        {
            this.id = id;
            bool selectResult = selectAccount();
            if (selectResult)
            {
                updateAccount();
                lblFailed.Visible = false;
                lblActivated.Visible = true;
            }//if-closing
            else
            {
                updateAccount();
                lblFailed.Visible = true;
                lblActivated.Visible = false;

            }//closing else
        }//closing else
    }

    bool selectAccount()
    {
        bool result = false;
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            string sql = @"SELECT ApplicantID FROM tbl_Applicant WHERE ApplicantID = @ApplicantID AND IsActivated = @active";
            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
                cmd.Parameters.AddWithValue("@ApplicantID", this.id);
                cmd.Parameters.AddWithValue("@active", "0");
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result = true;
                        }//close while
                    }//close if-statement
                    else
                    {
                        result = false;
                    }//close else-block
                }//close data reader
            }// SQL Command

        }//close SQL connection        
        return result;
    }
    void updateAccount()
    {
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            string sql = @"UPDATE tbl_Applicant SET IsActivated = @active WHERE ApplicantID = @ApplicantID";
            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
                cmd.Parameters.AddWithValue("@ApplicantID", this.id);
                cmd.Parameters.AddWithValue("@active", "1");
                cmd.ExecuteNonQuery();
            }
        }
    }

            }