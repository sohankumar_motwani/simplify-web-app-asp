﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="confirmation.aspx.cs" Inherits="confirmation" %>

<!DOCTYPE html>
<html>
<head>
    <title>Email Confirmation</title>
</head>
<body>

    <h1>Account Activation</h1>
    <asp:Label runat="server" ID="lblActivated" Visible="false">Your account has successfully been activated. Click <a href="index.aspx">here</a> to sign in.</asp:Label>
    <asp:Label runat="server" ID="lblFailed" Visible="false">Your account activation was unsuccessful. Please try again later.</asp:Label>
</body>
</html>
