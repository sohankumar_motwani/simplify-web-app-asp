﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="about.aspx.cs" Inherits="about" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <title>About</title>
    <link href="../Content/font-awesome.min.css" rel="stylesheet" />
    <link href="../Content/materialize.min.css" rel="stylesheet" />

</head>
<body>
    <nav class="navbar-default" id="navbar">
        <div class="nav-wrapper">
            <a href="index.aspx" class="brand-logo">&nbsp;<img src="images/SimplifyHR(CroppedTransBg).png" width="60%" /></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="index.aspx" style="color: black;">Home</a></li>
                <li><a href="about.aspx" style="color: black;">About</a></li>
            </ul>
        </div>
    </nav>
</body>
</html>
