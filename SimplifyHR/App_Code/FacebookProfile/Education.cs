﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Education
/// </summary>
public class School
{
    public string id { get; set; }
    public string name { get; set; }
}

public class With
{
    public string name { get; set; }
    public string id { get; set; }
}

public class Year
{
    public string id { get; set; }
    public string name { get; set; }
}

public class Concentration
{
    public string id { get; set; }
    public string name { get; set; }
}

public class Education
{
    public School school { get; set; }
    public string type { get; set; }
    public List<With> with { get; set; }
    public Year year { get; set; }
    public string id { get; set; }
    public List<Concentration> concentration { get; set; }
}

public class Employer
{
    public string id { get; set; }
    public string name { get; set; }
}

public class Position
{
    public string id { get; set; }
    public string name { get; set; }
}

public class Work
{
    public string end_date { get; set; }
    public Employer employer { get; set; }
    public Position position { get; set; }
    public string start_date { get; set; }
    public string id { get; set; }
}

public class Location
{
    public string id { get; set; }
    public string name { get; set; }
}

public class Hometown
{
    public string id { get; set; }
    public string name { get; set; }
}

public class RootObjectFB
{
    public string gender { get; set; }
    public string id { get; set; }
    public string first_name { get; set; }
    public string last_name { get; set; }
    public List<Education> education { get; set; }
    public string birthday { get; set; }
    public List<Work> work { get; set; }
    public Location location { get; set; }
    public Hometown hometown { get; set; }
    public string email { get; set; }
    public string about { get; set; }
    public string website { get; set; }
    public string relationship_status { get; set; }
}