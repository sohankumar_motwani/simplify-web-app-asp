﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Facebook;


/// <summary>
/// Summary description for Helper
/// </summary>
public class Helper
{
    public Helper()
    {

    }

    public static string GetConnection()
    {
        return ConfigurationManager.ConnectionStrings["MyCon"].ConnectionString;
    }

    public static string CreateSHAHash(string Phrase)
    {
        SHA512Managed HashTool = new SHA512Managed();
        Byte[] PhraseAsByte = System.Text.Encoding.UTF8.GetBytes(string.Concat(Phrase));
        Byte[] EncryptedBytes = HashTool.ComputeHash(PhraseAsByte);
        HashTool.Clear();
        return Convert.ToBase64String(EncryptedBytes);
    }
    public static void SendEmail(string email, string subject, string message)
    {
        MailMessage emailMessage = new MailMessage();
        emailMessage.From = new MailAddress("erwinthompson.fung@benilde.edu.ph", "Administrator");
        emailMessage.To.Add(new MailAddress(email));
        emailMessage.Subject = subject;
        emailMessage.Body = message;
        emailMessage.IsBodyHtml = true;
        emailMessage.Priority = MailPriority.Normal;
        SmtpClient MailClient = new SmtpClient("smtp.gmail.com", 587);
        MailClient.EnableSsl = true;
        MailClient.Credentials = new System.Net.NetworkCredential("erwinthompson.fung@benilde.edu.ph", "erwinthompson01");
        MailClient.Send(emailMessage);

    }


    public static object parameter;
    public static string paramcol;
    public static object textbox2;

    public static Boolean Insert(string table, string[] columns, string[] textbox, string[] tableCols)
    {
        if (columns.Length == textbox.Length)
        {
            String queryValues = "";
            String queryColumns = "";
            System.Diagnostics.Debug.WriteLine("The table is: " + table);
            for (int i = 0; i < columns.Length; i++)
            {
                System.Diagnostics.Debug.WriteLine("Column is: " + columns[i] + ", " + textbox[i]);
                queryValues = queryValues + ", " + columns[i];
                queryColumns = queryColumns + ", " + tableCols[i];
            }
            String adjustedColumns = queryColumns.Substring(2);
            String adjustedValues = queryValues.Substring(2);
            System.Diagnostics.Debug.WriteLine("Query columns are: " + adjustedColumns);
            System.Diagnostics.Debug.WriteLine("Query values are: " + adjustedValues);

            using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
            {
                con.Open();
                string sql = @"Insert Into " + table + " (" + adjustedColumns + ") Values(" + adjustedValues + ")";
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    //parameter = cmd.Parameters.AddWithValue(paramcol, textbox2);
                    for (int i = 0; i < columns.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(columns[i], textbox[i]);
                    }
                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
        }
        else
        {
            return false;
        }
    }

    public static Boolean Insert(string table, string[] columns, string[] textbox)
    {
        if (columns.Length == textbox.Length)
        {
            String queryValues = "";
            System.Diagnostics.Debug.WriteLine("The table is: " + table);
            for (int i = 0; i < columns.Length; i++)
            {
                System.Diagnostics.Debug.WriteLine("Column is: " + columns[i] + ", " + textbox[i]);
                queryValues = queryValues + ", " + columns[i];
            }
            String adjustedValues = queryValues.Substring(2);
            System.Diagnostics.Debug.WriteLine("Query values are: " + adjustedValues);

            using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
            {
                con.Open();
                string sql = @"Insert Into " + table + " Values(" + adjustedValues + ")";
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    //parameter = cmd.Parameters.AddWithValue(paramcol, textbox2);
                    for (int i = 0; i < columns.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(columns[i], textbox[i]);
                    }
                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
        }
        else
        {
            return false;
        }
    }

    public static int InsertScope(string table, string[] columns, string[] textbox)
    {
        if (columns.Length == textbox.Length)
        {
            String queryValues = "";
            System.Diagnostics.Debug.WriteLine("The table is: " + table);
            for (int i = 0; i < columns.Length; i++)
            {
                System.Diagnostics.Debug.WriteLine("Column is: " + columns[i] + ", " + textbox[i]);
                queryValues = queryValues + ", " + columns[i];
            }
            String adjustedValues = queryValues.Substring(2);
            System.Diagnostics.Debug.WriteLine("Query values are: " + adjustedValues);

            using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
            {
                con.Open();
                string sql = @"Insert Into " + table + " Values(" + adjustedValues + ")" + "SET @newId = SCOPE_IDENTITY(); ";
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    //parameter = cmd.Parameters.AddWithValue(paramcol, textbox2);
                    for (int i = 0; i < columns.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(columns[i], textbox[i]);
                    }
                    cmd.Parameters.Add("@newId", SqlDbType.Int).Direction = ParameterDirection.Output;
                    //add reader here
                    //int queryValue = 100;
                    //return cmd.ExecuteScalar();
                    cmd.ExecuteScalar();

                    return (int)cmd.Parameters["@newId"].Value;
                }
            }
        }
        else
        {
            return 0;
        }
    }

    public static string InsertScope(string table, string[] columns, string[] textbox, string[] tableCols)
    {
        if (columns.Length == textbox.Length)
        {
            String queryValues = "";
            String queryColumns = "";
            System.Diagnostics.Debug.WriteLine("The table is: " + table);
            for (int i = 0; i < columns.Length; i++)
            {
                System.Diagnostics.Debug.WriteLine("Column is: " + columns[i] + ", " + textbox[i]);
                queryValues = queryValues + ", " + columns[i];
                queryColumns = queryColumns + ", " + tableCols[i];
            }
            String adjustedColumns = queryColumns.Substring(2);
            String adjustedValues = queryValues.Substring(2);
            System.Diagnostics.Debug.WriteLine("Query columns are: " + adjustedColumns);
            System.Diagnostics.Debug.WriteLine("Query values are: " + adjustedValues);

            using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
            {
                con.Open();
                string sql = @"Insert Into " + table + " (" + adjustedColumns + ") Values(" + adjustedValues + ")" + "SET @newId = SCOPE_IDENTITY(); ";
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    //parameter = cmd.Parameters.AddWithValue(paramcol, textbox2);
                    for (int i = 0; i < columns.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(columns[i], textbox[i]);
                    }
                    cmd.Parameters.Add("@newId", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.ExecuteScalar();

                    return cmd.Parameters["@newId"].Value.ToString();
                }
            }
        }
        else
        {
            return "0";
        }
    }

    public static void Select(string SQL, DropDownList ddl, string name, string id)
    {
        ddl.AppendDataBoundItems = true;
        using (SqlConnection conn = new SqlConnection(Helper.GetConnection()))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(SQL, conn))
            {
                ddl.DataSource = cmd.ExecuteReader();
                ddl.DataTextField = name;
                ddl.DataValueField = id;
                ddl.DataBind();
            }
        }
    }
    public static void ApplicantSelect(String SQL, ListView pageListView, String val, string session)
    {
        using (SqlConnection conn = new SqlConnection(Helper.GetConnection()))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(SQL, conn))
            {
                cmd.Parameters.AddWithValue(val, session);
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds, "ashdkjahsdkajshdkajsd");
                    pageListView.DataSource = ds;
                    pageListView.DataBind();
                }
            }
        }
    }
    public static void Select(String SQL, ListView pageListView)
    {
        using (SqlConnection conn = new SqlConnection(Helper.GetConnection()))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(SQL, conn))
            {
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds, "ashdkjahsdkajshdkajsd");
                    pageListView.DataSource = ds;
                    pageListView.DataBind();
                }
            }
        }
    }

    public static void SessionSelect(String SQL, ListView pageListView, String val, string session)
    {
        using (SqlConnection conn = new SqlConnection(Helper.GetConnection()))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(SQL, conn))
            {
                cmd.Parameters.AddWithValue(val, HttpContext.Current.Session[session].ToString());
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds, "ashdkjahsdkajshdkajsd");
                    pageListView.DataSource = ds;
                    pageListView.DataBind();
                }
            }
        }
    }

    public static void Select(String SQL, ListView pageListView, string[] filter, string[] filValue)
    {
        if (filter.Length == filValue.Length)
        {
            using (SqlConnection conn = new SqlConnection(Helper.GetConnection()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(SQL, conn))
                {
                    for (int i = 0; i < filter.Length; i++)
                        cmd.Parameters.AddWithValue(filter[i], filValue[i]);
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataSet ds = new DataSet();
                        da.Fill(ds, "ashdkjahsdkajshdkajsd");
                        pageListView.DataSource = ds;
                        pageListView.DataBind();
                    }
                }
            }
        }
    }

    public static int SelectRow(string SQL, string filter, string value)
    {
        using (SqlConnection conn = new SqlConnection(Helper.GetConnection()))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(SQL, conn))
            {
                cmd.Parameters.AddWithValue(filter, value);
                return cmd.ExecuteNonQuery();
            }
        }
    }

    public static Boolean Delete(string SQL, string column, int value)
    {
        //if (columns.Length == values.Length)
        //{
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            using (SqlCommand cmd = new SqlCommand(SQL, con))
            {
                //for (int i = 0; i < columns.Length; i++)
                cmd.Parameters.AddWithValue(column, value);
                cmd.ExecuteNonQuery();
                return true;
            }
        }
        //}
        //else
        //{
        //return true;
        //}
    }

    public static Boolean UpdateDelete(string SQL, string[] columns, string[] values)
    {
        if (columns.Length == values.Length)
        {
            using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(SQL, con))
                {
                    for (int i = 0; i < columns.Length; i++)
                        cmd.Parameters.AddWithValue(columns[i], values[i]);
                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
        }
        else
        {
            return false;
        }
    }


    public static Boolean UpdateDelete(string SQL, string[] columns, double[] values)
    {
        if (columns.Length == values.Length)
        {
            using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(SQL, con))
                {
                    for (int i = 0; i < columns.Length; i++)
                        cmd.Parameters.AddWithValue(columns[i], values[i]);
                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
        }
        else
        {
            return false;
        }
    }

    public static String Login(string SQL, string col1, string userNameParam, string userNameValue, string passwordParam, string passwordValue)
    {
        String value = "";
        using (SqlConnection conn = new SqlConnection(Helper.GetConnection()))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand(SQL, conn))
            {
                cmd.Parameters.AddWithValue(userNameParam, userNameValue);
                cmd.Parameters.AddWithValue(passwordParam, Helper.CreateSHAHash(passwordValue));
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            value = dr[col1].ToString();
                        }
                        return value;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
    }

    public static double[] getRequest(string sourceData)
    {
        var request = (HttpWebRequest)WebRequest.Create("https://gateway.watsonplatform.net/personality-insights/api/v2/profile");
        var postData = sourceData;
        var data = Encoding.ASCII.GetBytes(postData);
        request.Method = "POST";
        request.ContentType = "text/plain";
        request.ContentLength = data.Length;
        request.Credentials = new NetworkCredential("55e712e2-b9a2-4496-a7eb-a6f84d2481d5", "sGEExsosNllV");
        using (var stream = request.GetRequestStream())
        {
            stream.Write(data, 0, data.Length);
        }
        var response = (HttpWebResponse)request.GetResponse();
        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
        System.Diagnostics.Debug.WriteLine("Server response is: " + responseString);
        //JObject jsonDat = JObject.Parse(responseString);
        RootObject rootClass = new RootObject();
        var newResource = JsonConvert.DeserializeObject<RootObject>(responseString);

        double[] roundedPercent = new double[25];
        double[] personalityArray = new double[5];
        double[] needsArray = new double[12];
        double[] valuesArray = new double[5];

        int personalityCounter = 1;
        int needCounter = 1;
        int valueCounter = 1;

        double personalityPercent = 0.00;
        double needPercent = 0.00;
        double valuePercent = 0.00;
        System.Diagnostics.Debug.WriteLine("IBM Labels start here: ");
        for (int i = 0; i < newResource.tree.children[0].children[0].children.Count(); i++)
        {
            personalityPercent += newResource.tree.children[0].children[0].children[i].percentage;
            personalityArray[i] = Math.Round(newResource.tree.children[0].children[0].children[i].percentage * 100);
            System.Diagnostics.Debug.WriteLine(newResource.tree.children[0].children[0].children[i].name);
            personalityCounter++;
        }
        roundedPercent[0] = Math.Round((personalityPercent / personalityCounter) * 100);
        for (int i = 0; i < newResource.tree.children[1].children[0].children.Count(); i++)
        {
            needPercent += newResource.tree.children[1].children[0].children[i].percentage;
            needsArray[i] = Math.Round(newResource.tree.children[1].children[0].children[i].percentage * 100);
            System.Diagnostics.Debug.WriteLine(newResource.tree.children[1].children[0].children[i].name);

            needCounter++;
        }
        roundedPercent[1] = Math.Round((needPercent / needCounter) * 100);
        for (int i = 0; i < newResource.tree.children[2].children[0].children.Count(); i++)
        {
            valuePercent += newResource.tree.children[2].children[0].children[i].percentage;
            valuesArray[i] = Math.Round(newResource.tree.children[2].children[0].children[i].percentage * 100);
            System.Diagnostics.Debug.WriteLine(newResource.tree.children[2].children[0].children[i].name);

            valueCounter++;
        }
        roundedPercent[2] = Math.Round((valuePercent / valueCounter) * 100);
        for (int i = 0; i < personalityArray.Length; i++)
            System.Diagnostics.Debug.WriteLine("Personality: " + personalityArray[i]);

        for (int i = 0; i < needsArray.Length; i++)
            System.Diagnostics.Debug.WriteLine("Needs: " + needsArray[i]);
        for (int i = 0; i < valuesArray.Length; i++)
            System.Diagnostics.Debug.WriteLine("Values: " + valuesArray[i]);

        personalityArray.CopyTo(roundedPercent, 3);
        needsArray.CopyTo(roundedPercent, 8);
        valuesArray.CopyTo(roundedPercent, 20);
        System.Diagnostics.Debug.WriteLine("round count" + roundedPercent.Count());
        for (int i = 0; i < roundedPercent.Length; i++)
            System.Diagnostics.Debug.WriteLine("Rating " + i + ": " + roundedPercent[i]);
        //+ "\n" + "Need: " + roundedPercent[1] + "\n" + "Values: " + roundedPercent[2]

        return roundedPercent;
    }

    public static string findUserTwitter(string resource_url, string q)
    {
        int pos = q.LastIndexOf("/") + 2;
        if (pos != 0)
        {
            q = q.Substring(pos, q.Length - pos);

            // oauth application keys
            var oauth_token = "134663854-Q6xnTKQk4qfXancBelyZHvEAEPmaPbCWHL4XD9OI"; //"insert here...";
            var oauth_token_secret = "4RndlV43u1vWsEbxua133ewvdxMOUELY03hx1f3NRoFzk"; //"insert here...";
            var oauth_consumer_key = "ClA4AtRKC7V7F316fYplMFAwI";// = "insert here...";
            var oauth_consumer_secret = "70oY8dH9g7OK4HvOKcgCT4q1EN0TDCj1ajVDhcWG6n87hGJR80";// = "insert here...";
                                                                                             // oauth implementation details
            var oauth_version = "1.0";
            var oauth_signature_method = "HMAC-SHA1";
            // unique request details
            var oauth_nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            var timeSpan = DateTime.UtcNow
            - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();
            // create oauth signature
            var baseFormat = "oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
    "&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}&screen_name={6}";
            var baseString = string.Format(baseFormat,
            oauth_consumer_key,
            oauth_nonce,
            oauth_signature_method,
            oauth_timestamp,
            oauth_token,
            oauth_version,
            Uri.EscapeDataString(q)
       );
            baseString = string.Concat("GET&", Uri.EscapeDataString(resource_url), "&", Uri.EscapeDataString(baseString));
            var compositeKey = string.Concat(Uri.EscapeDataString(oauth_consumer_secret),
            "&", Uri.EscapeDataString(oauth_token_secret));
            string oauth_signature;
            using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
            {
                oauth_signature = Convert.ToBase64String(
                hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
            }
            // create the request header
            var headerFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
    "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
    "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
    "oauth_version=\"{6}\"";
            var authHeader = string.Format(headerFormat,
            Uri.EscapeDataString(oauth_nonce),
            Uri.EscapeDataString(oauth_signature_method),
            Uri.EscapeDataString(oauth_timestamp),
            Uri.EscapeDataString(oauth_consumer_key),
            Uri.EscapeDataString(oauth_token),
            Uri.EscapeDataString(oauth_signature),
            Uri.EscapeDataString(oauth_version)
            );
            ServicePointManager.Expect100Continue = false;
            // make the request
            var postBody = "screen_name=" + Uri.EscapeDataString(q);//
            resource_url += "?" + postBody;
            System.Diagnostics.Debug.WriteLine("Query is: " + resource_url);


            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource_url);
            request.Headers.Add("Authorization", authHeader);
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded;charset=UTF-8";
            var response = (HttpWebResponse)request.GetResponse();
            var reader = new StreamReader(response.GetResponseStream());
            var objText = reader.ReadToEnd();
            string html = "";
            try
            {
                JArray jsonDat = JArray.Parse(objText);
                for (int x = 0; x < jsonDat.Count(); x++)
                {
                    html += jsonDat[x]["text"].ToString() + " ";
                }
                System.Diagnostics.Debug.WriteLine("Returned Data: " + objText);
                System.Diagnostics.Debug.WriteLine("Desiarilized text: " + html);
                return html;
            }
            catch (Exception twit_error)
            {
                return "Error: there was a problem retrieving tweets.";
                System.Diagnostics.Debug.WriteLine("Exception: " + twit_error.ToString());
            }
        }
        else
        {
            return null;
        }
    }

    public static string getFacebook(string accessToken)
    {
        //var accessToken = "EAABoCrxeszkBAOBaSZCv58lU77PdJKP5y36qCZBB2u7ZBBSopIH0RCkQZApaosTif9fFSnzXw9Om35XvlBOESL8tPgAZBXw8C4aksJxJivan3Oqt1evM0G8fUUVfQKPLtOfmXvCh078RWULTRiTglbmHwoFpkNx2sr8xZAGPTLcwZDZD";
        var client = new FacebookClient(accessToken);
        dynamic me = client.Get("me/posts?fields=message&limit=300");
        //dynamic personal = client.Get("me?fields=id,first_name,last_name,education,birthday,work,location,hometown,email,about,website,relationship_status");

        //From: https://stackoverflow.com/questions/32584850/facebook-js-sdks-fb-api-me-method-doesnt-return-the-fields-i-expect-in-gra
        //var me = client.Get("me/posts?fields=message") as IDictionary<string, object>;
        //string aboutMe = (string)me["message"];
        //string aboutMe = (string)me.data[16].message;
        //System.Diagnostics.Debug.WriteLine("My Facebook: " + aboutMe + "\n" + "Count:");
        string messageString = "";
        int count = Enumerable.Count(me.data);
        for (int i = 0; i < count; i++)
        {
            if (!object.ReferenceEquals(me.data[i].message, null))
            {
                messageString += me.data[i].message + " ";
            }
        }
        //System.Diagnostics.Debug.WriteLine("My message is: " + messageString);
        System.Diagnostics.Debug.WriteLine("My access token: " + accessToken);
        return messageString;
    }

    public static string GetPictureUrl(string faceBookId)
    {
        WebResponse response = null;
        string pictureUrl = string.Empty;
        try
        {
            WebRequest request = WebRequest.Create(string.Format("https://graph.facebook.com/{0}/picture", faceBookId));
            response = request.GetResponse();
            pictureUrl = response.ResponseUri.ToString();
        }
        catch (Exception ex)
        {
            //? handle
        }
        finally
        {
            if (response != null) response.Close();
        }
        return pictureUrl;
    }
    public static string email;
    public static string gender;
    public static string about;
    public static string birthday;
    public static string firstName;
    public static string lastName;
    public static string location;
    public static string website;
    public static string Url;

    public static void createFacebookBased(string accessToken)
    {
        //var accessToken = "EAABoCrxeszkBAOBaSZCv58lU77PdJKP5y36qCZBB2u7ZBBSopIH0RCkQZApaosTif9fFSnzXw9Om35XvlBOESL8tPgAZBXw8C4aksJxJivan3Oqt1evM0G8fUUVfQKPLtOfmXvCh078RWULTRiTglbmHwoFpkNx2sr8xZAGPTLcwZDZD";
        var client = new FacebookClient(accessToken);
        //dynamic me = client.Get("me/posts?fields=message&limit=300");
        //dynamic personal = client.Get("me?fields=id,first_name,last_name,education,birthday,work,location,hometown,email,about,website,relationship_status");
        var personal = client.Get("me?fields=id,first_name,last_name,email") as IDictionary<string, object>;
        //dynamic profilePicture = client.Get("me/picture?type=normal");
        email = personal["email"].ToString();
        //From: https://stackoverflow.com/questions/32584850/facebook-js-sdks-fb-api-me-method-doesnt-return-the-fields-i-expect-in-gra
        //var me = client.Get("me/posts?fields=message") as IDictionary<string, object>;
        //string aboutMe = (string)me["message"];
        //string aboutMe = (string)me.data[16].message;
        //System.Diagnostics.Debug.WriteLine("My Facebook: " + aboutMe + "\n" + "Count:");
        //System.Diagnostics.Debug.WriteLine("My message is: " + messageString);
        System.Diagnostics.Debug.WriteLine("My access token: " + accessToken);
        //int personalCount = Enumerable.Count(personal);
        //int personalCount = personal.Count;
        var newResource = JsonConvert.DeserializeObject<RootObjectFB>(personal.ToString());
        System.Diagnostics.Debug.WriteLine("JSON: " + personal.ToString());
        string profId = (string)newResource.id;
        Url = GetPictureUrl(profId);
        firstName = newResource.first_name;
        lastName = newResource.last_name;
        //location = (newResource.location == null)?"": newResource.location.name;
        //website = (newResource.website == null)?"":newResource.website;
        //gender = (newResource.gender == null) ? null : newResource.gender;
        //birthday = (newResource.birthday == null) ? null : newResource.birthday;
        //email = newResource.email;
        //about = (newResource.about == null) ? null : newResource.about;
        //System.Diagnostics.Debug.WriteLine("Data: " + newResource.education[1].school.name + " - " + newResource.gender + "\n" + "Personal: " + personalCount);
        int degreeId = 4;
        //if (!!Object.ReferenceEquals(newResource.education[1].concentration[0].name, null))
        //{
        //if (newResource.education[1].concentration[0].name.Contains("Bachelor"))
        //{
        //degreeId = 1;
        //}
        //else if (newResource.education[1].concentration[0].name.Contains("Master"))
        //{
        //degreeId = 2;
        //}
        //else if (newResource.education[1].concentration[0].name.Contains("Doctor"))
        //{
        //degreeId = 3;
        //}
        //else if (newResource.education[1].concentration[0].name.Contains("Undergraduate"))
        //{
        //degreeId = 4;
        //}
        //}
        System.Diagnostics.Debug.WriteLine("URL: " + Url);

        //System.Diagnostics.Debug.WriteLine(newResource.email + " " + "12345678" + " " + newResource.first_name + " " + newResource.last_name + " " + newResource.about + " " + newResource.birthday + " " + newResource.gender + " " + degreeId.ToString() + " " + newResource.education[1].concentration[0].name + " " + null + " " + newResource.education[0].school.name + " " + newResource.education[1].school.name + " " + null);
    }

    public static string insertMessage;

    public static bool facebookApplicant(string Email, string Password, string FirstName, string LastName, string Image)
    {
        bool result = false;
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            string sql = @"select count(Email) as Email from tbl_Applicant where Email= '" + Email + "'";
            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
                if (Email == null || FirstName == null || LastName == null || Password == null)
                {
                    insertMessage = "<script>alert('Some values required for the system to work are missing. Please double-check your Facebook account. Thank You')</script>";
                }
                else
                {
                    string iemail = cmd.ExecuteScalar().ToString();
                    if (iemail == "0")
                    {
                        //TBL_Applicant
                        string path = "../images/";
                        string picture = DateTime.Now.ToString("yyyyMMdd hmmtt") + ".jpg";
                        //path = Path.Combine(Image, picture);
                        //WebClient wc = new WebClient();
                        //wc.DownloadFile(Helper.Url, path);
                        string[] cols = new string[] { "@Email", "@password", "@FirstName", "@LastName", "@ProfilePicture", "@Facebook" };
                        string[] txt = new string[] { Email, Helper.CreateSHAHash(Password), FirstName, LastName, Image, HttpContext.Current.Session["AccessToken"].ToString() };
                        string[] tbcols = new string[] { "Email", "Password", "FirstName", "LastName", "ProfilePicture", "Facebook" };
                        string applicant = Convert.ToString(Helper.InsertScope("tbl_Applicant", cols, txt, tbcols));

                        /*
                        //TBL_School
                        if (primary != null)
                        {
                            string[] cols1 = new string[] { "@SchoolName" };
                            string[] txt1 = new string[] { primary };
                            string[] tbcols1 = new string[] { "SchoolName" };
                            string schoolid1 = Convert.ToString(Helper.InsertScope("tbl_School", cols1, txt1, tbcols1));
                            string[] cols6 = new string[] { "@ApplicantID", "@EducLvID", "@SchoolID" };
                            string[] txt6 = new string[] { applicant, "1", schoolid1 };
                            string[] tbcols6 = new string[] { "ApplicantID", "EducLvID", "SchoolID" };
                            Helper.InsertScope("tbl_ApplicantEducLvl", cols6, txt6, tbcols6);
                        }

                        if (secondary != null)
                        {
                            string[] cols2 = new string[] { "@SchoolName" };
                            string[] txt2 = new string[] { secondary };
                            string[] tbcols2 = new string[] { "SchoolName" };
                            string schoolid2 = Helper.InsertScope("tbl_School", cols2, txt2, tbcols2);
                            string[] cols7 = new string[] { "@ApplicantID", "@EducLvID", "@SchoolID" };
                            string[] txt7 = new string[] { applicant, "2", schoolid2 };
                            string[] tbcols7 = new string[] { "ApplicantID", "EducLvID", "SchoolID" };
                            Helper.InsertScope("tbl_ApplicantEducLvl", cols7, txt7, tbcols7);
                        }

                        string courseId = null;

                        //TBL_Course
                        if (course != null && degreeId != null)
                        {
                            string[] cols5 = new string[] { "@CourseName", "@DegreeId" };
                            string[] txt5 = new string[] { course, degreeId };
                            string[] tbcols5 = new string[] { "CourseName", "DegreeId" };
                            courseId = Helper.InsertScope("tbl_Course", cols5, txt5, tbcols5);
                        }

                        if (tertiary != null || courseId != null)
                        {
                            string[] cols3 = new string[] { "@SchoolName" };
                            string[] txt3 = new string[] { tertiary };
                            string[] tbcols3 = new string[] { "SchoolName" };
                            string schoolid3 = Helper.InsertScope("tbl_School", cols3, txt3, tbcols3);
                            string[] cols8 = new string[] { "@ApplicantID", "@EducLvID", "@SchoolID", "@CourseID" };
                            string[] txt8 = new string[] { applicant, "3", schoolid3, courseId };
                            string[] tbcols8 = new string[] { "ApplicantID", "EducLvID", "SchoolID", "CourseID" };
                            Helper.InsertScope("tbl_ApplicantEducLvl", cols8, txt8, tbcols8);
                        }

                        //TBL_Degree
                        //string[] cols4 = new string[] { "@DegreeType" };
                        //string[] txt4 = new string[] { degreeType };
                        //string[] tbcols4 = new string[] { "DegreeType" };
                        //string degree = Helper.InsertScope("tbl_Degree", cols4, txt4, tbcols4);


                        //System.Diagnostics.Debug.WriteLine(applicant + "," + schoolid1 + "," + schoolid2 + "," + schoolid3 + "," + degreeId + "," + course);


                        //TBL_EducLvl
                        if (primary != null)
                        {
                            //code for primary was here earlier.
                            //removed due to schoolId concern.
                        }

                        if (secondary != null)
                        {
                            //removed for same reason as above.
                        }


                        if (tertiary != null)
                        {
                            //removed for same reason as above.
                        }
                        */
                        insertMessage = "<script>alert('Signup Successful')</script>";
                        HttpContext.Current.Session["ApplicantID"] = applicant;
                        result = true;
                    }
                    else if (iemail == "1")
                    {
                        insertMessage = "<script>alert('Email already taken.')</script>";
                        result = false;
                    }
                }
            }
        }
        return result;
    }


}