﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Child2
/// </summary>
public class Child2
{
    public Child2()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string id { get; set; }
    public string name { get; set; }
    public string category { get; set; }
    public double percentage { get; set; }
    public List<Child3> children { get; set; }
}