﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Child3
/// </summary>
public class Child3
{
    public Child3()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string id { get; set; }
    public string name { get; set; }
    public string category { get; set; }
    public double percentage { get; set; }
    public double sampling_error { get; set; }
    public List<Child4> children { get; set; }
}