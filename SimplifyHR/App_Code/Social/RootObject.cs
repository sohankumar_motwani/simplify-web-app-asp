﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RootObject
/// </summary>
public class RootObject
{
    public RootObject()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string id { get; set; }
    public string source { get; set; }
    public int word_count { get; set; }
    public string word_count_message { get; set; }
    public string processed_lang { get; set; }
    public Tree tree { get; set; }
    public List<Warning> warnings { get; set; }
}