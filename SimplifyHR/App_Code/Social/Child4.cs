﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Child4
/// </summary>
public class Child4
{
    public Child4()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string id { get; set; }
    public string name { get; set; }
    public string category { get; set; }
    public double percentage { get; set; }
    public double sampling_error { get; set; }
}