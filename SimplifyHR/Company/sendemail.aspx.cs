﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Company_sendemail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["ID"] == null)
        {
            Response.Redirect("Applicants.aspx");
        }
        else
        {
            int authorID = 0;
            bool validAuthor = int.TryParse(Request.QueryString["ID"].ToString(),
                out authorID);
            if (validAuthor)
            {
                if (!IsPostBack)
                {
                    GetData(authorID);
                }
            }
            else
            {
                Response.Redirect("Applicants.aspx");
            }
        }
    }

    void GetData(int ID)
    {
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            string SQL = @"Select Email from tbl_Applicant where ApplicantID=@ApplicantID";
            using (SqlCommand cmd = new SqlCommand(SQL, con))
            {
                cmd.Parameters.AddWithValue("@ApplicantID", ID);
                using (SqlDataReader dr = cmd.ExecuteReader())
                {

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            txtto.Text = dr["Email"].ToString();
                        }
                    }
                    else
                    {
                        Response.Redirect("Applicants.aspx");
                    }
                }
            }
        }
    }

    protected void btnsend_Click(object sender, EventArgs e)
    {
        Helper.SendEmail(txtto.Text,txtsubj.Text,txtbody.Text);
    }
}