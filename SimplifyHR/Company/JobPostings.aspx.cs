﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Company_AddJobs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CompanyID"] == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
            showCity();
            showJobs();
            showSkills();
        }
    }
    void showSkills()
    {
        string SQL = @"SELECT SKillID, Skill FROM tbl_Skills";
        Helper.Select(SQL, ddlSkills, "Skill", "SKillID");
    }
    void showCity()
    {
        String SQL = @"Select CityID, City From tbl_City inner join tbl_Region ON tbl_City.RegionID = tbl_Region.RegionID inner join tbl_Country ON tbl_Region.CountryCode = tbl_Country.CountryID where tbl_Region.CountryCode = '173'";
        Helper.Select(SQL, ddlcity, "City", "CityID");
    }

    void showJobs()
    {
        String SQL = @"Select JobPostingID,JobTitle,JobDescription,co.ProfilePicture From tbl_JobPosting jb Join tbl_Company co ON jb.CompanyID = co.CompanyID where co.CompanyID = @CompanyID ";
        Helper.SessionSelect(SQL, jbpostings, "@CompanyID", "CompanyID");
    }
    protected void Post_Click(object sender, EventArgs e)
    {
        string[] cols = new string[] { "@JobTitle", "@LocationID", "@JobDescription", "@WorkExperience", "@EducationalAttainment", "@PriorTraining", "@Benefit", "@Responsibilities", "@PayRange", "@TimeStamp", "@CompanyID" };
        string[] txt = new string[] { txtjbttle.Text, ddlcity.SelectedValue, txtjobdesc.Text, txtworke.Text, txteduca.Text, txtPrrT.Text, txtben.Text, txtres.Text, txtpr.Text, DateTime.Now.ToString(), Session["CompanyID"].ToString() };
        string[] tbcols = new string[] { "JobTitle", "LocationID", "JobDescription", "WorkExperience", "EducationalAttainment", "PriorTraining", "Benefit", "Responsibilities", "PayRange", "TimeStamp", "CompanyID" };
        string jobposint = Helper.InsertScope("tbl_JobPosting", cols, txt, tbcols);

        string[] cols1 = new string[] { "@SkillID", "@JobPostingId" };
        string[] txt1 = new string[] { ddlSkills.SelectedValue, jobposint };
        string[] tbcols1 = new string[] { "SkillID", "JobPostingId" };
        Helper.Insert("tbl_JobSkills", cols1, txt1, tbcols1);
        Response.Redirect("JobPostings.aspx");


    }
}