﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.master" AutoEventWireup="true" CodeFile="Details.aspx.cs" Inherits="Company_Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" Runat="Server">
    <div class="col-lg-offset-4 col-lg-6">
            <div class="card white darken-1">
                <div class="card-content black-text">
                    <div class="input-field col s6">
                        <asp:TextBox runat="server" placeholder="Job title" ID="txtjbttle"></asp:TextBox>
                        <asp:TextBox runat="server" placeholder="Job Description" ID="txtjobdesc" TextMode="MultiLine"></asp:TextBox>
                        <asp:TextBox runat="server" placeholder="Educational Attainment" ID="txteduca"></asp:TextBox>
                        <asp:TextBox runat="server" placeholder="Work Experience" ID="txtworke"></asp:TextBox>
                        <asp:TextBox runat="server" placeholder="Prior Training" ID="txtPrrT"></asp:TextBox>
                        <asp:TextBox runat="server" placeholder="Benefit" ID="txtben"></asp:TextBox>
                        <asp:TextBox runat="server" placeholder="Responsibilities" ID="txtres"></asp:TextBox>
                        <asp:TextBox runat="server" placeholder="Pay Range" ID="txtpr"></asp:TextBox>
                        <asp:DropDownList runat="server" ID="ddlcity" CssClass="form-control btn-flat" Style="border-style: solid; border-radius: 5px;">
                            <asp:ListItem>--- Select City ---</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="card-action">
                    <asp:Button ID="Update" runat="server" class="btn btn-primary" OnClick="Update_Click" Text="Update Post"></asp:Button>
                </div>
            </div>
        </div>
</asp:Content>

