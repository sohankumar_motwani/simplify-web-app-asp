﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Company_Applicants : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CompanyID"] == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
            if (Request.QueryString["ID"].ToString() != null)
            {
            string SQL = @"SELECT DISTINCT a.ApplicantID, a.FirstName, a.LastName, a.ProfilePicture, c.City, r.Region, co.Country, a.Summary, aj.Status FROM tbl_Applicant a left JOIN tbl_City c ON a.CityID = c.CityID Left JOIN tbl_Region r ON c.RegionID = r.RegionID Left JOIN tbl_Country co ON r.CountryCode = co.CountryID inner Join tbl_ApplicantJob aj on a.ApplicantID = aj.ApplicantID where aj.Status = 'Applied' AND JobPostingID = '" + Request.QueryString["ID"].ToString() + "' ";
                Helper.Select(SQL, lvUsers);

                string sql2 = @"SELECT        TOP (100) PERCENT F.ApplicantID, F.FirstName, F.MiddleName, F.LastName, F.ProfilePicture, F.Summary, F.Skill, F.City, F.Region, F.Country, CA.CompanyID, F.TotalAverage, F.Personality, F.Need, F.Value, F.Result, 
                         F.progress
FROM            dbo.tbl_ApplicantJob AS AJ INNER JOIN
                         dbo.tbl_JobPosting AS JP ON AJ.JobPostingID = JP.JobPostingID INNER JOIN
                         dbo.CompanyAverage AS CA ON CA.CompanyID = JP.CompanyID INNER JOIN
                         dbo.Final AS F ON AJ.ApplicantID = F.ApplicantID AND CA.APersonality < F.Personality + 15 AND CA.APersonality > F.Personality - 5 AND CA.ANeed < F.Need + 7 AND CA.ANeed > F.Need - 10 AND CA.AValue < F.Value + 5 AND 
                         CA.AValue > F.Value - 15
                WHERE CA.CompanyID = @CompanyID
ORDER BY F.TotalAverage DESC";
                //                    string sql2 = SELECT TOP (100) PERCENT F.ApplicantID, F.FirstName, F.MiddleName, F.LastName, F.ProfilePicture, F.Summary, F.Skill, F.City, F.Region, F.Country
                //FROM            dbo.tbl_ApplicantJob AS AJ INNER JOIN
                //                         dbo.tbl_JobPosting AS JP ON AJ.JobPostingID = JP.JobPostingID INNER JOIN
                //                         dbo.CompanyAverage AS CA ON CA.CompanyID = JP.CompanyID INNER JOIN
                //                         dbo.Final AS F ON AJ.ApplicantID = F.ApplicantID
                //WHERE(F.Personality + 15 > CA.APersonality AND F.Personality - 5 < CA.APersonality) AND(F.Need + 10 > CA.ANeed AND F.Need - 15 < CA.ANeed) AND
                //(F.Value + 5 > CA.AValue AND F.Value - 15 < CA.AValue)

                //ORDER BY F.TotalAverage DESC
                Session["JobPostingID"] = Request.QueryString["id"].ToString();

                Helper.ApplicantSelect(sql2, lvrecommend,"@CompanyID", session);
                
            }
            else
            {
                Response.Redirect("JobPostings.aspx");
            }

        }

    }

    /*
    protected void lnkContact_Click(object sender, EventArgs e)
    {
        string[] statementParams = new string[] { "@ApplicantID", "@JobPostingID", "@Status" };
        string[] columnNames = new string[] { "ApplicantID", "JobPostingID", "Status" };
        string[] values = new string[] { Eval("ApplicantID").ToString(), Request.QueryString["ID"].ToString(), "Contacted" };
        bool result = Helper.Insert("tbl_ApplicantJob", statementParams, values);
        if (result)
            Response.Write("<script>alert('You've contacted this user.');</script>");
        else
            Response.Write("<script>alert('There was an issue contacting this user. You may try again after a while.');</script>");
}
    */

    
}