﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="Company_index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Simplify: Breaking employment barriers</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/paper-bootstrap-wizard.css" rel="stylesheet" />
    <link href="../Content/font-awesome.min.css" rel="stylesheet" />
    <link href="../css/mdb.min.css" rel="stylesheet" />
    <link href="../css/mdb.min.css" rel="stylesheet" />
    <link href="../Content/materialize.min.css" rel="stylesheet" />
    <link href="../Content/themify-icons.css" rel="stylesheet" />

</head>
<body style="background-color: snow">

    <div class="full-bg-img flex-center">
        <form runat="server" class="form-inline">
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog" id="modal">
                    <!-- Modal content-->
                    <div class="modal-content" id="msform">
                        <div class="modal-header">
                            <!-- progressbar -->
                            <div class="modal-title">
                                <h2 class="black-text text-center">Register</h2>
                            </div>
                            <hr />
                            <div class="modal-body">
                                <label for="ProfilePicture">Profile Picture</label>
                               <input type="file" id="ProfilePicture" runat="server" name="pic" class="input-lg" accept="image/*| file_extension" />
                                <label for="CompanyName">CompanyName</label>
                                <asp:TextBox runat="server" CssClass="form-control" ID="CompanyName" placeholder="Accenture" />
                                <label for="StreetAddress">StreetAddress</label>
                                <asp:TextBox runat="server" CssClass="form-inline" ID="StreetAddress" placeholder="123 st." />
                                <label for="City">City</label>
                                <br />
                                <asp:DropDownList runat="server" ID="ddlcity" CssClass="form-control">
                                    <asp:ListItem>--- Select City ---</asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <label for="ZipCode">ZipCode</label>
                                <asp:TextBox runat="server" CssClass="form-control" ID="ZipCode" TextMode="Number" placeholder="1012" />
                                <label for="Telephone">Telephone</label>
                                <asp:TextBox runat="server" CssClass="form-control" ID="Telephone" TextMode="Number" placeholder="7315121"  />
                                <label for="Email">Email</label>
                                <asp:TextBox runat="server" CssClass="form-control" ID="Email" TextMode="Email" placeholder="accenture_1@gmail.com" />
                                <label for="Password">Password</label>
                                <asp:TextBox runat="server" CssClass="form-control" ID="Password" TextMode="Password"  placeholder="*********" />
                                <label>Confirm Password<small>*</small></label>
                                <asp:TextBox runat="server" TextMode="Password" CssClass="col-lg-7" ID="txtPWR" placeholder="*********" />
                                <asp:CompareValidator ID="cvPW" runat="server"
                                    ControlToValidate="txtPWR" ControlToCompare="Password"
                                    Display="Dynamic" ErrorMessage="Password does not match"
                                    ForeColor="Red" SetFocusOnError="true" />
                                <br />
                                <asp:Button runat="server" CssClass="btn btn-lg" style="background-color:#4C79CE" ID="submit" Text="SignUp!" OnClick="btnsignup_Click" />
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="animate-bottom">
                <div class="card" style="background-color: white; padding-left: 20px; padding-right: 40px;">

                    <div class="col-lg-6 col-lg-offset-3">
                        <br />
                        <img src="../images/SimplifyHR(CroppedTransBg).png" />
                    </div>
                    <br />
                    <br />
                    <br />
                    <br />
                    <hr />
                    <h5 class="center">Company Login</h5>
                    <asp:Label CssClass="red-text" runat="server" ID="error" Visible="false">Email or Password is incorrect</asp:Label>
                    <br />
                    <div class="row">
                        <div class="input-field col s6">
                            <i class="fa fa-user prefix" style="color: #4C79CE;"></i>
                            <asp:TextBox runat="server" ID="Luser" TextMode="Email" CssClass="black-text form-control"></asp:TextBox>
                            <label for="Luser">Email</label>
                        </div>
                        <div class="input-field col s6">
                            <i class="fa fa-key prefix" style="color: #4C79CE;"></i>
                            <asp:TextBox runat="server" TextMode="Password" ID="Lpass" CssClass="black-text form-control"></asp:TextBox>
                            <label for="Lpass">Password </label>
                        </div>
                    </div>
                    <br />
                    <a class="btn btn-lg" style="background-color: #4C79CE;" data-toggle="modal" data-target="#myModal">Sign up!</a>
                    <asp:Button runat="server" ID="login1" CssClass="btn pull-right" Style="background-color: #4C79CE;" Text="login" OnClick="login1_Click" />
                </div>
            </div>
        </form>
    </div>


    <script src="../Scripts/jquery-3.1.1.min.js"></script>
    <script src="../Scripts/tether.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/jquery.bootstrap.wizard.js"></script>
    <script src="../Scripts/paper-bootstrap-wizard.js"></script>
    <script src="../Scripts/jquery.validate.min.js"></script>
    <script src="../Scripts/mdb.min.js"></script>

</body>
</html>

