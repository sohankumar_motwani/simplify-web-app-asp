﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Company_Contact : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CompanyID"] == null)
        {
            Response.Redirect("~/Company/index.aspx");
        }//closing if
        else
        {
            if (Request.QueryString["id"] != null & Session["JobPostingID"] != null)
            {
                bool result = checkUser();
                if (result)
                {
                    //show error
                }
                else
                {
                    string[] statementParams = new string[] { "@ApplicantID", "@JobPostingID", "@Status" };
                    string[] columnNames = new string[] { "ApplicantID", "JobPostingID", "Status" };
                    string[] values = new string[] { Request.QueryString["id"].ToString(), Request.QueryString["ID"].ToString(), "Contacted" };
                    Helper.Insert("tbl_ApplicantJob", statementParams, values);
                }//closing else
            }//closing if
            else
            {
                Response.Redirect("~/Company/Applicants.aspx");
            }//closing else
        }//closing else


    }

    Boolean checkUser()
    {
        bool result = false;
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            string sql = @"SELECT ApplicantID FROM tbl_ApplicantJob WHERE ApplicantID = @ApplicantID AND JobPostingID = @JobPostingID";
            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
                cmd.Parameters.AddWithValue("@ApplicantID", Request.QueryString["id"].ToString());
                cmd.Parameters.AddWithValue("@JobPostingID", Session["JobPostingID"].ToString());
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                        result = true;
                    else
                        result = false;
                }//closing reader
            }//closing command
        }//closing connection
        return result;
    }//close select block


}