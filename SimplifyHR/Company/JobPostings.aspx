﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.master" AutoEventWireup="true" CodeFile="JobPostings.aspx.cs" Inherits="Company_AddJobs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>JobPosting</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" runat="Server">
    <h1>
        <span class="col-lg-offset-8">
            <small>
                <a data-toggle="collapse" data-target="#demo" style="text-decoration: none;"><i class="fa fa-plus"></i>Add Job</a>
            </small></span>
    </h1>
    <div id="demo" class="collapse">
        <div class="col-lg-offset-4 col-lg-6">
            <div class="card white darken-1">
                <div class="card-content black-text">
                    <div class="input-field col s6">
                        <asp:TextBox runat="server" placeholder="Job title" ID="txtjbttle"></asp:TextBox>
                        <asp:TextBox runat="server" placeholder="Job Description" ID="txtjobdesc" TextMode="MultiLine"></asp:TextBox>
                        <asp:TextBox runat="server" placeholder="Educational Attainment" ID="txteduca"></asp:TextBox>
                        <asp:TextBox runat="server" placeholder="Work Experience" ID="txtworke"></asp:TextBox>
                        <asp:TextBox runat="server" placeholder="Prior Training" ID="txtPrrT"></asp:TextBox>
                        <asp:TextBox runat="server" placeholder="Benefit" ID="txtben"></asp:TextBox>
                        <asp:TextBox runat="server" placeholder="Responsibilities" ID="txtres"></asp:TextBox>
                        <asp:TextBox runat="server" placeholder="Pay Range" ID="txtpr"></asp:TextBox>
                         <asp:DropDownList CssClass="form-control btn-flat" Style="border-style: solid; border-radius: 5px;" ID="ddlSkills" runat="server" >
                            <asp:ListItem>--Choose a Skill--</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <br />
                        <asp:DropDownList runat="server" ID="ddlcity" CssClass="form-control btn-flat" Style="border-style: solid; border-radius: 5px;">
                            <asp:ListItem>--- Select City ---</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="card-action">
                    <asp:Button ID="Post" runat="server" class="btn btn-primary" OnClick="Post_Click" Text="Submit"></asp:Button>
                </div>
            </div>
        </div>
    </div>

    <asp:ListView ID="jbpostings" runat="server">
        <ItemTemplate>
            <div class="row">
                <div class="col-lg-offset-4 col-lg-6">
                    <div class="card">
                        <div class="card-image waves-effect waves-block waves-light">
                           <img src='<%# "../CompanyProfpic/" + Eval("ProfilePicture") %>' class="img-responsive activator" />
                        </div>
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4"><%# Eval("JobTitle") %></span>
                            <hr />
                            <h5><%# Eval("JobDescription") %></h5>
                        </div>
                        <div class="card-action">
                              <a href='Applicants.aspx?ID=<%# Eval("JobPostingID")%>' class="btn btn-default" style="background-color:#4C79CE;">
                                View Applicants
                            </a>
                            <a href='Details.aspx?ID=<%# Eval("JobPostingID")%>' class="btn btn-default">
                                Edit
                            </a>
                            <a href='Delete.aspx?ID=<%# Eval("JobPostingID")%>' class="btn btn-danger" style="background-color:red;" onclick='return confirm("Delete this Record?")'>
                                Delete
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
        <EmptyDataTemplate>
            <tr>
                <td>
                    <h1 class="text-center">No records found.</h1>
                </td>
            </tr>
        </EmptyDataTemplate>
    </asp:ListView>
</asp:Content>

