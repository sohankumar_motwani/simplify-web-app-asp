﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Company_ApplicantProfile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CompanyID"] == null)
        {
            Response.Redirect("~/Company/index.aspx");
        }
        else
        {
            string ApplicantID = Request.QueryString["id"].ToString();
            if (String.IsNullOrEmpty(ApplicantID))
            {
                Response.Redirect("JobPostings.aspx");
            }
            else
            {
                string SQL = @"SELECT ApplicantID FROM tbl_Applicant WHERE ApplicantID = @ApplicantID";
                int numRows = Helper.SelectRow(SQL, "@ApplicantID", ApplicantID);
                if (numRows <= 0)
                {
                    ltApplicantID.Text = "It seems you're trying to view an invalid profile; go back to the previous page and try again.";
                    lnkApplicant.Visible = true;
                    heading.InnerText = "Sorry!";
                }
            }
        }


    }

    void showProfile(string ApplicantID) {
        string SQL = @"SELECT a.ApplicantID, a.FirstName, a.MiddleName, a.LastName, a.BirthDate, a.MaritalStatus, a.Email, a.Telephone, a.Mobile, a.CallTime, a.CallMedium, a.Nationality, a.Website, a.Facebook, a.Twitter, a.Instagram, a.Personality, a.Needs, a.Values, a.ProfilePicture, c.City, r.Region, co.Country, a.Summary, a.Result, d.Day FROM tbl_Applicant a INNER JOIN tbl_City c ON a.CityID = c.CityID INNER JOIN tbl_Region r ON c.RegionID = r.RegionID INNER JOIN tbl_Country co ON r.CountryCode = co.CountryCode LEFT JOIN tbl_Day d ON d.DayID = a.DayID";
        string[] filter = new string[1] { "@ApplicantID" };
        string[] value = new string[1] { ApplicantID };
Helper.Select(SQL, lvProfile, filter, value);
    }
    void showSkills() { }
    void showInterests() { }
    void showEducation() { }
    void showLanguage() { }
    void showJob() { }
    void showPublication() { }
    void showPatent() { }
    void showProject() { }

}
