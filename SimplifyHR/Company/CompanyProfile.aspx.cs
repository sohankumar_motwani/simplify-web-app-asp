﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Company_CompanyProfile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["CompanyID"] == null)
        {
            Response.Redirect("index.aspx");
        }
        else
        {
            showuser();
        }
    }

    void showuser()
    {
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            string SQL = @"Select ap.CompanyName,ap.StreetAddress,ap.Telephone,ap.ProfilePicture,b.Branch,c.City,c.RegionID  
                        from tbl_Company AS ap 
                        Left JOIN tbl_City AS c
                        ON ap.CityID = C.CityID 
                        Left Join tbl_CompanyBranch as cb
                        ON ap.CompanyID = cb.CompanyID
                        Left Join tbl_Branch as b
                        ON cb.BranchID = b.BranchId
                        Where ap.CompanyID = @CompanyID";
            using (SqlCommand cmd = new SqlCommand(SQL, con))
            {
                cmd.Parameters.AddWithValue("@CompanyID", Session["CompanyID"].ToString());

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        profpic.ImageUrl = "/CompanyProfpic/" + dr["ProfilePicture"].ToString(); ;
                        user.Text = dr["CompanyName"].ToString();
                        StreetAddress.Text = dr["StreetAddress"].ToString();
                        city.Text = " " + dr["City"].ToString() + ", " + dr["RegionID"].ToString();
                        Branch.Text = " " + dr["Branch"].ToString();
                        Telephone.Text = " " + dr["Telephone"].ToString();
                    }
                }
            }
        }
    }
}