﻿<%@ Page Title="Users" Language="C#" MasterPageFile="~/Company/Company.master" AutoEventWireup="true" CodeFile="Applicants.aspx.cs" Inherits="Company_Applicants" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" runat="Server">
    
    <div class="col-lg-8 col-lg-offset-3">
        <h3>Recommended Applicants</h3>
        <asp:ListView runat="server" ID="lvrecommend">
            <ItemTemplate>
                <div class="column">
                    <div class="col-lg-6">
                        <div class="card">
                            <br />
                            <div class="card-content">
                                <asp:Image ID="Image2" runat="server" ImageUrl='<%# string.Format("~/images/{0}",Eval("ProfilePicture"))%>' Height="150" Width="150" AlternateText="User's Profile Picture" />
                                <h3><%# Eval("FirstName") %> <%# Eval("LastName") %></h3>
                                <p><b>Location: <%# Eval("City") %>, <%# Eval("Region") %>, <%# Eval("Country") %></b></p>
                                <br />
                                <h5>Summary:</h5>
                                <p style="overflow-wrap: break-word"><%# Eval("Summary") %></p>
                            </div>
                            <div class="card-action">
                                <a href='UserProfile.aspx?id=<%# Eval("ApplicantID") %>'>View Profile</a>
                                
                                <%-- <asp:LinkButton ID="lnkContact" Text="Contact" OnClick="lnkContact_Click" runat="server"></asp:LinkButton>--%>
                                <a href='Contact.aspx?id=<%# Eval("ApplicantID") %>'>Contact</a>

                            </div>
<hr />
                        </div>
                    </div>
            </ItemTemplate>
            <EmptyDataTemplate>
                <h2>No users to display for now.</h2>
            </EmptyDataTemplate>
        </asp:ListView>
    </div>
    <br />

    <div >
        <h3>Job Applicants</h3>
        <br />
        <asp:ListView runat="server" ID="lvUsers">
            <ItemTemplate>
                <div class="column">
                    <div class="col-lg-6">
                        <div class="card">
                            <br />
                            <div class="card-content">
                                <asp:Image ID="Image2" runat="server" ImageUrl='<%# string.Format("~/images/{0}",Eval("ProfilePicture"))%>' Height="150" Width="150" AlternateText="User's Profile Picture" />
                                <h3><%# Eval("FirstName") %> <%# Eval("LastName") %></h3>
                                <p><b>Location: <%# Eval("City") %>, <%# Eval("Country") %></b></p>
                                <br />
                                <h5>Summary:</h5>
                                <p style="overflow-wrap: break-word"><%# Eval("Summary") %></p>
                            </div>
                            <div class="card-action">
                                <a href='UserProfile.aspx?id=<%# Eval("ApplicantID") %>'>View Profile</a>

                                <a href="sendemail.aspx?id=<%# Eval("ApplicantID") %>" class="pull-right">Send Schedule</a>
                            </div>
                            <hr />
                        </div>
                    </div>
            </ItemTemplate>
            <EmptyDataTemplate>
                <h2>No users Applied for this Job.</h2>
            </EmptyDataTemplate>
        </asp:ListView>
    </div>
</asp:Content>

