﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.master" AutoEventWireup="true" CodeFile="Settings.aspx.cs" Inherits="Company_Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" runat="Server">
    <div class="col-lg-offset-3 col-lg-6">
        <h2>Settings</h2>
    </div>
    <div class="col-lg-offset-3 col-lg-2">
        <div class="row">
            <ul class="nav nav-tabs  nav-stacked">
                <li class="active"><a data-toggle="tab" href="#gen">General</a></li>
                <li><a data-toggle="tab" href="#notif">Notification</a></li>
                <li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
                <li><a data-toggle="tab" href="#menu3">Menu 3</a></li>
            </ul>
        </div>
    </div>
    <div class="col-lg-offset-5 row col-lg-6">
        <div class="card col-lg-offset-8">
            <div class="card-content">
                <div class="tab-content">
                    <div id="gen" class="tab-pane fade in active">
                        <h4>General Settings</h4>
                        <hr />
                        <label id="lblemail">Email</label>
                        <asp:TextBox ID="txtemail" runat="server" CssClass="form-control" placeholder="lorem.ipsum@gmail.com" TextMode="Email"></asp:TextBox>
                        <label id="lblpass">Change Password</label>
                        <asp:TextBox ID="txtchngePass" runat="server" CssClass="form-control" placeholder="*********" TextMode="Password"></asp:TextBox>
                        <label id="lblcpass">Confirm Password</label>
                        <asp:TextBox ID="txtconchngePass" runat="server" CssClass="form-control" placeholder="*********" TextMode="Password"></asp:TextBox>
                        asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary pull-right" Text="Submit" />
                        <br />
                    </div>
                    <div id="notif" class="tab-pane fade">
                        <h4>Notification Settings</h4>
                        <hr />
                        <div class="container">
                            <div class="switch">
                                <label>Allow Notification</label>
                                <label class="pull-right">
                                    Off
                                <asp:CheckBox runat="server" ID="chknot" />
                                    <span class="lever"></span>
                                    On
                                </label>
                            </div>

                        </div>
                    </div>
                    <div id="menu2" class="tab-pane fade">
                        <h3>Menu 2</h3>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                    </div>
                    <div id="menu3" class="tab-pane fade">
                        <h3>Menu 3</h3>
                        <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

