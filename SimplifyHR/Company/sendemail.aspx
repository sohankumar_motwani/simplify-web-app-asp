﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.master" AutoEventWireup="true" CodeFile="sendemail.aspx.cs" Inherits="Company_sendemail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" runat="Server">
    <div class="col-lg-6 col-lg-offset-4">
        <div class="card">
            <div class="card-title">
                <br />
                <h4 class="center">Schedule for Interview</h4>
                <hr />
            </div>
            <div class="card-content">
                <label for="txtto">To:</label>
                <asp:TextBox runat="server" ID="txtto" CssClass="form-control" Enabled="false"></asp:TextBox>
                <label for="txtsubj">Subject:</label>
                <asp:TextBox runat="server" ID="txtsubj" CssClass="form-control"></asp:TextBox>
                <label for="txtbody">Message:</label>
                <asp:TextBox runat="server" ID="txtbody" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
            </div>
            <br />
            <div class="card-action">
                <a href="Applicants.aspx" class="btn btn-default">Back</a>
                <asp:Button runat="server" ID="btnsend" CssClass="btn btn-success pull-right" Text="Send Email" OnClick="btnsend_Click" />
            </div>
        </div>
    </div>
</asp:Content>

