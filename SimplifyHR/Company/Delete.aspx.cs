﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Company_Delete : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["ID"] == null)
        {
            Response.Redirect("JobPostings.aspx");
        }
        else
        {
            int authorID = 0;
            bool validAuthor = int.TryParse(Request.QueryString["ID"].ToString(),
                out authorID);
            if (validAuthor)
            {
                if (!IsPostBack)
                {
                    DeleteRecord(authorID);
                }
            }
            else
            {
                Response.Redirect("JobPostings.aspx");
            }
        }

    }

    void DeleteRecord(int ID)
    {
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            string SQL = @"Delete from tbl_ApplicantJob where JobPostingID=@JobPostingID;Delete from tbl_JobPosting where JobPostingID=@JobPostingID;";

            using (SqlCommand cmd = new SqlCommand(SQL, con))
            {
                cmd.Parameters.AddWithValue("@JobPostingID", ID);
                cmd.ExecuteNonQuery();
                Response.Redirect("JobPostings.aspx");
            }
        }
    }
}