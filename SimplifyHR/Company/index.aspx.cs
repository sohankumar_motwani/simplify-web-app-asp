﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Company_index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        showCity();
    }
    void showCity()
    {
        String SQL = @"Select CityID, City From tbl_City inner join tbl_Region ON tbl_City.RegionID = tbl_Region.RegionID inner join tbl_Country ON tbl_Region.CountryCode = tbl_Country.CountryID where tbl_Region.CountryCode = '173'";
        Helper.Select(SQL, ddlcity, "City", "CityID");
    }
    protected void login1_Click(object sender, EventArgs e)
    {
        string SQL = @"SELECT CompanyID FROM tbl_Company WHERE Email = @Email AND Password = @Password";
        String returnedValue = Helper.Login(SQL, "CompanyID", "@Email", Luser.Text, "@Password", Lpass.Text);
        if (returnedValue != null)
        {
            Session["CompanyID"] = returnedValue;
            Response.Redirect("JobPostings.aspx");
        }
        else
        {
            error.Visible = true;
        }

    }

    protected void btnsignup_Click(object sender, EventArgs e)
    {
        string fileName1 = Path.GetFileName(ProfilePicture.PostedFile.FileName);
        ProfilePicture.PostedFile.SaveAs(Server.MapPath("/CompanyProfpic/") + fileName1);

        string[] cols = new string[] { "@CompanyName", "@StreetAddress", "@CityID", "@ZipCode", "@Telephone", "@Email", "@Password", "@Timestamp", "@ProfPic" };
        string[] txt = new string[] { CompanyName.Text, StreetAddress.Text, ddlcity.SelectedValue, ZipCode.Text, Telephone.Text, Email.Text, Helper.CreateSHAHash(Password.Text), DateTime.Now.ToString(), ProfilePicture.Value };
        string[] tbcols = new string[] { "CompanyName", "StreetAddress", "CityID", "ZipCode", "Telephone", "Email", "Password", "Timestamp", "ProfilePicture" };
        Helper.Insert("tbl_Company", cols, txt, tbcols);
    }
}