﻿<%@ Page Title="Applicant Profile" Language="C#" MasterPageFile="~/Company/Company.master" AutoEventWireup="true" CodeFile="ApplicantProfile.aspx.cs" Inherits="Company_ApplicantProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" Runat="Server">
    <h1 id="heading" runat="server">Simplify Profile</h1>
    <asp:Literal runat="server" ID="ltApplicantID"></asp:Literal>
    <br />
    <a href="Applicants.aspx" runat="server" id="lnkApplicant" visible="false">Go Back</a>
    <asp:ListView runat="server" ID="lvProfile">
        <ItemTemplate>
            <h2>Personal Information</h2>
            <label>First Name:</label>
            <asp:Literal runat="server" ID="ltFirstName" Text='<%# Eval("FirstName") %>'></asp:Literal>

        </ItemTemplate>
        <EmptyDataTemplate>

        </EmptyDataTemplate>
</asp:ListView>

</asp:Content>