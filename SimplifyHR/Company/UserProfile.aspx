﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Company/Company.master" AutoEventWireup="true" CodeFile="UserProfile.aspx.cs" Inherits="Company_UserProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Profile</title>
    <link href="../css/UserProfile.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <br />
    <br />

    <asp:UpdatePanel runat="server" ID="updatepanel">
        <ContentTemplate>
            <%-- User Interface --%>
            <div class="col-lg-8 col-lg-offset-3">

                <%-- User Frame --%>
                <div class="user-menu-container">
                    <div class="user-details white card">
                        <div class="row ">
                            <div class="col-md-8 no-pad">
                                <div class="user-pad">
                                    <h3 style="color: black;">
                                        <asp:Literal runat="server" ID="user"></asp:Literal></h3>
                                    <h4><i class="fa fa-check-circle-o"></i>
                                        <asp:Literal runat="server" ID="city"></asp:Literal>
                                        <asp:Literal runat="server" ID="region"></asp:Literal></h4>
                                    <h4><a>
                                        <asp:Literal runat="server" ID="personalWebsite"></asp:Literal></a></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class=" img-responsive" style="">
                                    <asp:Image runat="server" ID="profpic" alt="Profile picture" Height="300px" Width="300px" class="img-responsive" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br />

                <%-- User stats --%>
                <div class="col-md-12 user-menu user-pad card white">
                    <div class="user-menu-content active">

                        <%-- USER PROFILE --%>
                        <h2>User Profile</h2>
                        <hr />
                        <asp:ListView runat="server" ID="UserProfile">
                            <ItemTemplate>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h5>
                                            <b>Gender: </b>
                                            <%# Eval("Gender") %>
                                        </h5>
                                        <h5>
                                            <b>Birthdate: </b>
                                            <%# Eval("Birthdate") %>
                                        </h5>
                                        <h5>
                                            <b>MaritalStatus: </b>
                                            <%# Eval("MaritalStatus") %>
                                        </h5>
                                        <h5>
                                            <b>Telephone: </b>
                                            <%# Eval("Telephone") %>
                                        </h5>
                                        <h5>
                                            <b>Mobile: </b>
                                            <%# Eval("Mobile") %>
                                        </h5>

                                    </div>
                                    <div class="col-lg-6">
                                        <h5>
                                            <b>Nationality: </b>
                                            <%# Eval("Nationality") %>
                                        </h5>
                                        <h5>
                                            <b>StreetAddress: </b>
                                            <%# Eval("StreetAddress") %>
                                        </h5>
                                        <h5>
                                            <b>CallTime:</b>
                                            <%# Eval("CallTime") %>
                                        </h5>
                                        <h5>
                                            <b>CallMedium:</b>
                                            <%# Eval("CallMedium") %>
                                        </h5>
                                        <h5 style="overflow-wrap: break-word">
                                            <b>Summary:</b>
                                            <%# Eval("Summary") %>
                                        </h5>
                                    </div>

                                </div>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                                <div class="col-lg-6">
                                    <p>
                                        <b>MaritalStatus: </b>
                                    </p>
                                    <p>
                                        <b>StreetAddress: </b>

                                    </p>
                                    <p>
                                        <b>Telephone: </b>

                                    </p>
                                    <p>
                                        <b>Mobile: </b>

                                    </p>
                                    <p>
                                        <b>CallTime:</b>

                                    </p>
                                    <p>
                                        <b>CallMedium:</b>

                                    </p>
                                    <p>
                                        <b>Website: </b>

                                    </p>
                                    <p>
                                        <b>Summary:</b>

                                    </p>
                                </div>

                            </EmptyDataTemplate>
                        </asp:ListView>

                        <%-- SOCIAL MEDIA --%>
                        <h3>Social Media Analysis</h3>
                        <hr />
                        <p>
                            <a href="#" data-toggle="modal" data-target="#personality">Personality:</a>
                            <asp:Literal ID="PersoPercent" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="personalityT" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>

                            <a href="#" data-toggle="modal" data-target="#needs">Needs: </a>
                            <asp:Literal ID="needPercent" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="NeedsT" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>

                            <a href="#" data-toggle="modal" data-target="#values">Values:</a>
                            <asp:Literal ID="ValuesPercent" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="ValuesT" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                        </p>

                        <%-- ASSETS --%>
                        <h3>Assets</h3>
                        <div>
                            <div class="dropdown">
                                <hr />
                                <div class="row">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <h5>
                                                <b>Certification:</b>
                                            </h5>
                                            <asp:ListView ID="certificate" runat="server">
                                                <ItemTemplate>
                                                    <h5>
                                                        <b><%# Eval("Cetification") %></b>
                                                    </h5>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    <h5>No Certificate </h5>
                                                </EmptyDataTemplate>
                                            </asp:ListView>
                                            <br />
                                            <hr />
                                            <h5>
                                                <b>Interest:</b>
                                            </h5>
                                            <asp:ListView ID="Interest" runat="server">
                                                <ItemTemplate>
                                                    <h5>
                                                        <b><%# Eval("Interest") %></b>
                                                    </h5>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    <h5>No Interest </h5>
                                                </EmptyDataTemplate>
                                            </asp:ListView>
                                            <br />
                                            <hr />
                                            <h5>
                                                <b>Language:</b>
                                            </h5>
                                            <asp:ListView ID="Language" runat="server">
                                                <ItemTemplate>
                                                    <h5>
                                                        <b><%# Eval("Language") %></b>
                                                    </h5>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    <h5>No Language </h5>
                                                </EmptyDataTemplate>
                                            </asp:ListView>
                                            <br />
                                            <hr />
                                            <h5>
                                                <b>Organization:</b>
                                            </h5>
                                            <asp:ListView ID="Organization" runat="server">
                                                <ItemTemplate>
                                                    <h5>
                                                        <b><%# Eval("Organization") %></b>
                                                    </h5>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    <h5>No Organization </h5>
                                                </EmptyDataTemplate>
                                            </asp:ListView>
                                            <br />
                                            <hr />

                                        </div>
                                        <div class="col-lg-6">
                                            <h5>
                                                <b>Patent:</b>
                                            </h5>
                                            <asp:ListView ID="Patent" runat="server">
                                                <ItemTemplate>
                                                    <h5>
                                                        <b><%# Eval("Patent") %></b>
                                                    </h5>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    <h5>No Patent </h5>
                                                </EmptyDataTemplate>
                                            </asp:ListView>
                                            <br />
                                            <hr />
                                            <h5>
                                                <b>Project:</b>
                                            </h5>
                                            <asp:ListView ID="Project" runat="server">
                                                <ItemTemplate>
                                                    <h5>
                                                        <b><%# Eval("Project") %></b>
                                                    </h5>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    <h5>No Project </h5>
                                                </EmptyDataTemplate>
                                            </asp:ListView>
                                            <br />
                                            <hr />
                                            <h5>
                                                <b>Skills:</b>
                                            </h5>
                                            <asp:ListView ID="Skills" runat="server">
                                                <ItemTemplate>
                                                    <h5>
                                                        <b><%# Eval("Skill") %></b>
                                                    </h5>
                                                </ItemTemplate>
                                                <EmptyDataTemplate>
                                                    <h5>No Skills </h5>
                                                </EmptyDataTemplate>
                                            </asp:ListView>
                                            <br />
                                            <hr />
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
        </ContentTemplate>
    </asp:UpdatePanel>

       <div id="personality" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <b>Openness:</b>
                    <asp:Literal ID="LPersonality1" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Personality1" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Conscientiousness:</b>
                    <asp:Literal ID="LPersonality2" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Personality2" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Extraversion:</b>
                    <asp:Literal ID="LPersonality3" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Personality3" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Agreeableness:</b>
                    <asp:Literal ID="LPersonality4" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Personality4" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>EmotionalRange:</b>
                    <asp:Literal ID="LPersonality5" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Personality5" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

     <div id="needs" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <b>Challenge:</b>
                    <asp:Literal ID="LNeeds1" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs1" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Closeness:</b>
                    <asp:Literal ID="LNeeds2" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs2" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Curiosity:</b>
                    <asp:Literal ID="LNeeds3" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs3" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Excitement:</b>
                    <asp:Literal ID="LNeeds4" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs4" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Harmony:</b>
                    <asp:Literal ID="LNeeds5" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs5" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Ideal:</b>
                    <asp:Literal ID="LNeeds6" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs6" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Liberty:</b>
                    <asp:Literal ID="LNeeds7" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs7" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Love:</b>
                    <asp:Literal ID="LNeeds8" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs8" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Practicality:</b>
                    <asp:Literal ID="LNeeds9" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs9" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>SelfExpression:</b>
                    <asp:Literal ID="LNeeds10" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs10" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Stability:</b>
                    <asp:Literal ID="LNeeds11" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs11" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Structure:</b>
                    <asp:Literal ID="LNeeds12" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs12" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

      <div id="values" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <b>Conservation:</b>
                    <asp:Literal ID="Lvalues1" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Values1" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>OpennessToChange:</b>
                    <asp:Literal ID="Lvalues2" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Values2" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Hedonism:</b>
                    <asp:Literal ID="Lvalues3" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Values3" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>SelfEnhancement:</b>
                    <asp:Literal ID="Lvalues4" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Values4" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>SelfTranscendence:</b>
                    <asp:Literal ID="Lvalues5" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Values5" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</asp:Content>

