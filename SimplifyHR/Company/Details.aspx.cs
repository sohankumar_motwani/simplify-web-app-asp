﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Company_Details : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["ID"] == null)
        {
            Response.Redirect("JobPostings.aspx");
        }
        else
        {
            int authorID = 0;
            bool validAuthor = int.TryParse(Request.QueryString["ID"].ToString(),
                out authorID);
            if (validAuthor)
            {
                if (!IsPostBack)
                {
                    GetData(authorID);
                    getcity();
                }
            }
            else
            {
                Response.Redirect("JobPostings.aspx");
            }
        }
    }

    void getcity()
    {
        String SQL = @"Select City, CityID From tbl_City";
        Helper.Select(SQL, ddlcity, "City", "CityID");
    }

    void GetData(int ID)
    {
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            string SQL = @"Select JobTitle,LocationID,JobDescription,WorkExperience,EducationalAttainment,
                           PriorTraining,Benefit,Responsibilities,PayRange from tbl_JobPosting where JobPostingID=@JobPostingID";
            using (SqlCommand cmd = new SqlCommand(SQL, con))
            {
                cmd.Parameters.AddWithValue("@JobPostingID", ID);
                using (SqlDataReader dr = cmd.ExecuteReader())
                {

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            txtjbttle.Text = dr["JobTitle"].ToString();
                            txtjobdesc.Text = dr["JobDescription"].ToString();
                            txteduca.Text = dr["EducationalAttainment"].ToString();
                            txtworke.Text = dr["WorkExperience"].ToString();
                            txtPrrT.Text = dr["PriorTraining"].ToString();
                            txtben.Text = dr["Benefit"].ToString();
                            txtres.Text = dr["Responsibilities"].ToString();
                            txtpr.Text = dr["PayRange"].ToString();
                            ddlcity.SelectedValue = dr["LocationID"].ToString();
                        }
                    }
                    else
                    {
                        Response.Redirect("JobPostings.aspx");
                    }
                }
            }
        }
    }

    protected void Update_Click(object sender, EventArgs e)
    {
        String SQL = @"Update tbl_JobPosting SET JobTitle=@JobTitle, JobDescription=@JobDescription,
                              EducationalAttainment=@EducationalAttainment, WorkExperience=@WorkExperience, PriorTraining=@PriorTraining,
                               Benefit=@Benefit, Responsibilities=@Responsibilities,PayRange=@PayRange,LocationID=@LocationID where JobPostingID=@JobPostingID";
        string[] col = { "@JobTitle", "@JobDescription", "@EducationalAttainment", "@WorkExperience", "@PriorTraining", "@Benefit", "@Responsibilities", "@PayRange", "@LocationID", "@JobPostingID" };
        string[] textbox = { txtjbttle.Text, txtjobdesc.Text, txteduca.Text, txtworke.Text, txtPrrT.Text, txtben.Text, txtres.Text, txtpr.Text, ddlcity.SelectedValue, Request.QueryString["ID"].ToString()  };
        Helper.UpdateDelete(SQL,col, textbox);
        Response.Redirect("JobPostings.aspx");
    }
}