﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Certification_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        getData();
    }

    void getData()
    {
        string session = Session["ApplicantID"].ToString();
        string[] filter = new string[] { "@UserID" };
        string[] filvalue = new string[] { session };
        string SQL = @"SELECT        dbo.tbl_Project.ProjectName, dbo.tbl_Project.CertificationAuthor, dbo.tbl_Project.StartMonth, dbo.tbl_Project.StartYear, dbo.tbl_Project.EndMonth, dbo.tbl_Project.EndYear, dbo.tbl_Project.IsOngoing, 
                         dbo.tbl_Project.ProjectURL, dbo.tbl_Applicant.ApplicantID
FROM            dbo.tbl_Project INNER JOIN
                         dbo.tbl_Applicant ON dbo.tbl_Project.ApplicantId = dbo.tbl_Applicant.ApplicantID
                           WHERE tbl_Applicant.ApplicantID = @UserID";
        Helper.Select(SQL, lvSkills, filter, filvalue);
    }
}