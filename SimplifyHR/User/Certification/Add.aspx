﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="User_Certification_Add" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Certification</title>
    <link href="../../Content/font-awesome.min.css" rel="stylesheet" />
    <link href="../../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../../Content/materialize.min.css" rel="stylesheet" />
</head>
<body>
    <div class="col-lg-offset-3 col-lg-6">
        <div class="card">
            <div class="card-title">
                <br />
                <h4 class="center">Add Certificate</h4>
            </div>
            <form id="RegisterForm" runat="server">
                <div class="card-content">
                    <div class="form-group">
                        <div class="">
                            <label class="control-label">Certification:</label>
                        </div>
                        <div class="">
                            <asp:DropDownList CssClass="form-control" ID="ddlCertification" runat="server">
                                <asp:ListItem>--Choose a Certification--</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class=" ">
                            <label class="control-label">Certification Author:</label>
                        </div>
                        <div class=" ">
                            <asp:TextBox ID="txtAuthor" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class=" ">
                            <label class="control-label">License Number:</label>
                        </div>
                        <div class=" ">
                            <asp:TextBox ID="txtLicense" CssClass="form-control" runat="server" type="Number" min="1" required="required"></asp:TextBox>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class=" ">
                            <label class="control-label">Start Month:</label>
                        </div>
                        <div class=" ">
                            <asp:DropDownList CssClass="form-control" ID="ddlStartMonth" runat="server">
                                <asp:ListItem>--Choose a Month--</asp:ListItem>
                                <asp:ListItem Value="1">January</asp:ListItem>
                                <asp:ListItem Value="2">February</asp:ListItem>
                                <asp:ListItem Value="3">March</asp:ListItem>
                                <asp:ListItem Value="4">April</asp:ListItem>
                                <asp:ListItem Value="5">May</asp:ListItem>
                                <asp:ListItem Value="6">June</asp:ListItem>
                                <asp:ListItem Value="7">July</asp:ListItem>
                                <asp:ListItem Value="8">August</asp:ListItem>
                                <asp:ListItem Value="9">September</asp:ListItem>
                                <asp:ListItem Value="10">October</asp:ListItem>
                                <asp:ListItem Value="11">November</asp:ListItem>
                                <asp:ListItem Value="12">December</asp:ListItem>

                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class=" ">
                            <label class="control-label">End Month:</label>
                        </div>
                        <div class=" ">
                            <asp:DropDownList CssClass="form-control" ID="ddlEndMonth" runat="server">
                                <asp:ListItem>--Choose a Month--</asp:ListItem>
                                <asp:ListItem Value="1">January</asp:ListItem>
                                <asp:ListItem Value="2">February</asp:ListItem>
                                <asp:ListItem Value="3">March</asp:ListItem>
                                <asp:ListItem Value="4">April</asp:ListItem>
                                <asp:ListItem Value="5">May</asp:ListItem>
                                <asp:ListItem Value="6">June</asp:ListItem>
                                <asp:ListItem Value="7">July</asp:ListItem>
                                <asp:ListItem Value="8">August</asp:ListItem>
                                <asp:ListItem Value="9">September</asp:ListItem>
                                <asp:ListItem Value="10">October</asp:ListItem>
                                <asp:ListItem Value="11">November</asp:ListItem>
                                <asp:ListItem Value="12">December</asp:ListItem>

                            </asp:DropDownList>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class=" ">
                            <label class="control-label">Start Year:</label>
                        </div>
                        <div class=" ">
                            <asp:TextBox ID="txtStartYear" CssClass="form-control" runat="server" type="Number" min="1" required="required"></asp:TextBox>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class=" ">
                            <label class="control-label">End Year:</label>
                        </div>
                        <div class=" ">
                            <asp:TextBox ID="txtEndYear" CssClass="form-control" runat="server" type="Number" min="1" required="required"></asp:TextBox>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class=" ">
                            <label class="control-label">Expires?:</label>
                        </div>
                        <div class=" ">
                            <asp:TextBox ID="txtExpires" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class=" ">
                            <label class="control-label">Certification URL:</label>
                        </div>
                        <div class=" ">
                            <asp:TextBox ID="txtURL" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="card-action">
                    <div class="form-group">
                        <div class="">
                            <asp:Button ID="btnRegister" CssClass="btn btn-success  btn-lg" Text="Save" runat="server" OnClick="btnRegister_Click" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>

</html>
