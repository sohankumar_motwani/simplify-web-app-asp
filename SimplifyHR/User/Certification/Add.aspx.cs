﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Certification_Add : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        showCertification();
    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        //lashdfasdhf
        string[] values = new string[] { "@ApplicantID", "@CertificationID", "@CertificationAuthor", "@LicenseNumber", "@StartMonth", "@StartYear", "@EndMonth", "@EndYear", "@Expires", "@CertURL" };
        string[] col = new string[] { "ApplicantID", "CertificationID", "CertificationAuthor", "LicenseNumber", "StartMonth", "StartYear", "EndMonth", "EndYear", "Expires", "CertURL" };
        string[] txt = new string[] { Session["ApplicantID"].ToString(), ddlCertification.SelectedValue, txtAuthor.Text, txtLicense.Text, ddlStartMonth.SelectedValue, txtStartYear.Text, ddlEndMonth.SelectedValue, txtEndYear.Text, txtExpires.Text, txtURL.Text};
        Helper.Insert("tbl_ApplicantCertification", values, txt, col);
    }

    void showCertification()
    {

        string SQL = @"SELECT CertificationID, CertificationName FROM tbl_Certification";
        Helper.Select(SQL, ddlCertification, "CertificationName", "CertificationID");

    }
}