﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class User_UserProfile : System.Web.UI.Page
{
    public string url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
    private double[] finalResults = new double[25];
    private double[] finalFacebookResults = new double[25];
    private string twitterUrl;
    private double[] averagedValues = new double[25];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ApplicantID"] != null & Session["AccessToken"] != null)
        {

            facebook();
            //Response.Write(Helper.insertMessage);
            twitterUrl = getTwitterURL();
            twitter();
            average();
            personality();
            needs();
            values();
            showCertification();
            showLanguage();
            showSkills();
            showuser();
            showuserprof();
            usercertificate();
            userinterest();
            userlang();
            userorg();
            userpat();
            userskills();
            userproj();
            ProfileProgress();
        }
        else if (Session["AccessToken"] != null & Session["ApplicantID"] == null)
        {
            facebook();
            Response.Write(Helper.insertMessage);
            showCertification();
            showLanguage();
            showSkills();
            personality();
            needs();
            values();
            //showuser();
            //showuserprof();
            //usercertificate();
            //userinterest();
            //userlang();
            //userorg();
            //userpat();
            //userskills();
            //userproj();
            //ProfileProgress();
            showFacebookUser();
            //bool result = Helper.facebookApplicant(Helper.email, "12345678", Helper.firstName, Helper.lastName, Helper.about, Helper.birthday, Helper.gender, "4", null, null, null, null, null);
        }
        else if (Session["ApplicantID"] != null & Session["AccessToken"] == null)
        {
            twitterUrl = getTwitterURL();
            twitter();
            showCertification();
            showLanguage();
            showSkills();
            personality();
            needs();
            values();
            showuser();
            showuserprof();
            usercertificate();
            userinterest();
            userlang();
            userorg();
            userpat();
            userskills();
            userproj();
            ProfileProgress();
        }
        else if (Session["ApplicantID"] == null & Session["AccessToken"] == null)
        {
            Response.Redirect("~/index.aspx");
        }//session checker closing.

    } //method closing

    void showFacebookUser()
    {
        user.Text = Helper.firstName + " " + Helper.lastName;
        city.Text = Helper.location;
        personalWebsite.Text = Helper.website;
        profpic.ImageUrl = Helper.Url;
    }
    void average()
    {
        if (twitterUrl != null && Session["AccessToken"] != null)
        {
            for (int c = 0; c < finalFacebookResults.Length; c++)
                averagedValues[c] = Math.Round((finalFacebookResults[c] + finalResults[c]) / 2);
            //display averageValues array into UI from here
            //this block of code is only applicable when user logs in with Facebook and also provides a twitter URL.
            //indexing is same as of the previous arrays.
            //[0-2] values are the averages of the sub-categories of personality, needs, and values respectively.
        }
        //else-if closing.
    }

    //Social Media Analysis
    void facebook()
    {
        if (Session["AccessToken"] != null)
        {
            int c = 0;
            foreach (double ave in Helper.getRequest(Helper.getFacebook(Session["AccessToken"].ToString())))
            {
                finalFacebookResults[c] = ave;
                c++;
            }

            //display array values from finalFacebookResult[] here.
            //format of array indexed elements is the same as twitter's.
        } //else-if closing
    }
    void twitter()
    {
        if (twitterUrl != "")
        {
            int c = 0;
            foreach (double ave in Helper.getRequest(Helper.findUserTwitter(url, twitterUrl)))
            {
                finalResults[c] = ave;
                c++;
            }


        }
    }


    //Social Media Analysis Progress Bar 
    void personality()
    {
        string sql = @"UPDATE tbl_Applicant SET Openness = @Openness, Conscientiousness = @Conscientiousness, Extraversion = @Extraversion, Agreeableness = @Agreeableness,
EmotionalRange = @EmotionalRange WHERE ApplicantID = @ApplicantID";
        string[] columns = new string[] { "@Openness", "@Conscientiousness", "@Extraversion", "@Agreeableness", "@EmotionalRange", "@ApplicantID" };
        
        if (Session["ApplicantID"] != null & Session["AccessToken"] != null)
        {
            //Personality
            int PercentpersonalityT = (int)averagedValues[0];
            personalityT.Attributes["aria-valuemin"] = "0";
            personalityT.Attributes["aria-valuemax"] = "100";
            personalityT.Attributes["aria-valuenow"] = PercentpersonalityT.ToString();
            personalityT.Style["width"] = String.Format("{0}%;", PercentpersonalityT);
            personalityT.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentpersonalityT)));
            PersoPercent.Text = PercentpersonalityT.ToString();

            //Sub Category Personality

            //Sub-Category Personality1
            int PercentsubpersonalityT1 = (int)averagedValues[3];
            Personality1.Attributes["aria-valuemin"] = "0";
            Personality1.Attributes["aria-valuemax"] = "100";
            Personality1.Attributes["aria-valuenow"] = PercentsubpersonalityT1.ToString();
            Personality1.Style["width"] = String.Format("{0}%;", PercentsubpersonalityT1);
            Personality1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentsubpersonalityT1)));
            LPersonality1.Text = PercentsubpersonalityT1.ToString();

            //Sub-Category Personality2
            int PercentsubpersonalityT2 = (int)averagedValues[4];
            Personality2.Attributes["aria-valuemin"] = "0";
            Personality2.Attributes["aria-valuemax"] = "100";
            Personality2.Attributes["aria-valuenow"] = PercentsubpersonalityT2.ToString();
            Personality2.Style["width"] = String.Format("{0}%;", PercentsubpersonalityT2);
            Personality2.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentsubpersonalityT2)));
            LPersonality2.Text = PercentsubpersonalityT2.ToString();

            //Sub-Category Personality3
            int PercentsubpersonalityT3 = (int)averagedValues[5];
            Personality3.Attributes["aria-valuemin"] = "0";
            Personality3.Attributes["aria-valuemax"] = "100";
            Personality3.Attributes["aria-valuenow"] = PercentsubpersonalityT3.ToString();
            Personality3.Style["width"] = String.Format("{0}%;", PercentsubpersonalityT3);
            Personality3.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentsubpersonalityT3)));
            LPersonality3.Text = PercentsubpersonalityT3.ToString();

            //Sub-Category Personality4
            int PercentsubpersonalityT4 = (int)averagedValues[6];
            Personality4.Attributes["aria-valuemin"] = "0";
            Personality4.Attributes["aria-valuemax"] = "100";
            Personality4.Attributes["aria-valuenow"] = PercentsubpersonalityT4.ToString();
            Personality4.Style["width"] = String.Format("{0}%;", PercentsubpersonalityT4);
            Personality4.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentsubpersonalityT4)));
            LPersonality4.Text = PercentsubpersonalityT4.ToString();

            //Sub-Category Personality5
            int PercentsubpersonalityT5 = (int)averagedValues[7];
            Personality5.Attributes["aria-valuemin"] = "0";
            Personality5.Attributes["aria-valuemax"] = "100";
            Personality5.Attributes["aria-valuenow"] = PercentsubpersonalityT5.ToString();
            Personality5.Style["width"] = String.Format("{0}%;", PercentsubpersonalityT5);
            Personality5.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentsubpersonalityT5)));
            LPersonality5.Text = PercentsubpersonalityT1.ToString();
            string[] perValue = new string[] { averagedValues[3].ToString(), averagedValues[4].ToString(), averagedValues[5].ToString(), averagedValues[6].ToString(), averagedValues[7].ToString(), Session["ApplicantID"].ToString() };
            Helper.UpdateDelete(sql, columns, perValue);
        }
        else if (Session["ApplicantID"] != null & Session["AccessToken"] == null)
        {
            //Personality
            int PercentpersonalityT = (int)finalResults[0];
            personalityT.Attributes["aria-valuemin"] = "0";
            personalityT.Attributes["aria-valuemax"] = "100";
            personalityT.Attributes["aria-valuenow"] = PercentpersonalityT.ToString();
            personalityT.Style["width"] = String.Format("{0}%;", PercentpersonalityT);
            personalityT.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentpersonalityT)));
            PersoPercent.Text = PercentpersonalityT.ToString();

            //Sub Category Personality

            //Sub-Category Personality1
            int PercentsubpersonalityT1 = (int)finalResults[3];
            Personality1.Attributes["aria-valuemin"] = "0";
            Personality1.Attributes["aria-valuemax"] = "100";
            Personality1.Attributes["aria-valuenow"] = PercentsubpersonalityT1.ToString();
            Personality1.Style["width"] = String.Format("{0}%;", PercentsubpersonalityT1);
            Personality1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentsubpersonalityT1)));
            LPersonality1.Text = PercentsubpersonalityT1.ToString();

            //Sub-Category Personality2
            int PercentsubpersonalityT2 = (int)finalResults[4];
            Personality2.Attributes["aria-valuemin"] = "0";
            Personality2.Attributes["aria-valuemax"] = "100";
            Personality2.Attributes["aria-valuenow"] = PercentsubpersonalityT2.ToString();
            Personality2.Style["width"] = String.Format("{0}%;", PercentsubpersonalityT2);
            Personality2.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentsubpersonalityT2)));
            LPersonality2.Text = PercentsubpersonalityT2.ToString();

            //Sub-Category Personality3
            int PercentsubpersonalityT3 = (int)finalResults[5];
            Personality3.Attributes["aria-valuemin"] = "0";
            Personality3.Attributes["aria-valuemax"] = "100";
            Personality3.Attributes["aria-valuenow"] = PercentsubpersonalityT3.ToString();
            Personality3.Style["width"] = String.Format("{0}%;", PercentsubpersonalityT3);
            Personality3.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentsubpersonalityT3)));
            LPersonality3.Text = PercentsubpersonalityT3.ToString();

            //Sub-Category Personality4
            int PercentsubpersonalityT4 = (int)finalResults[6];
            Personality4.Attributes["aria-valuemin"] = "0";
            Personality4.Attributes["aria-valuemax"] = "100";
            Personality4.Attributes["aria-valuenow"] = PercentsubpersonalityT4.ToString();
            Personality4.Style["width"] = String.Format("{0}%;", PercentsubpersonalityT4);
            Personality4.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentsubpersonalityT4)));
            LPersonality4.Text = PercentsubpersonalityT4.ToString();

            //Sub-Category Personality5
            int PercentsubpersonalityT5 = (int)finalResults[7];
            Personality5.Attributes["aria-valuemin"] = "0";
            Personality5.Attributes["aria-valuemax"] = "100";
            Personality5.Attributes["aria-valuenow"] = PercentsubpersonalityT5.ToString();
            Personality5.Style["width"] = String.Format("{0}%;", PercentsubpersonalityT5);
            Personality5.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentsubpersonalityT5)));
            LPersonality5.Text = PercentsubpersonalityT1.ToString();
            string[] perValue = new string[] { finalResults[3].ToString(), finalResults[4].ToString(), finalResults[5].ToString(), finalResults[6].ToString(), finalResults[7].ToString(), Session["ApplicantID"].ToString() };
            Helper.UpdateDelete(sql, columns, perValue);
        }
        else if (Session["AccessToken"] != null & Session["ApplicantID"] == null)
        {
            //Personality
            int PercentpersonalityT = (int)finalFacebookResults[0];
            personalityT.Attributes["aria-valuemin"] = "0";
            personalityT.Attributes["aria-valuemax"] = "100";
            personalityT.Attributes["aria-valuenow"] = PercentpersonalityT.ToString();
            personalityT.Style["width"] = String.Format("{0}%;", PercentpersonalityT);
            personalityT.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentpersonalityT)));
            PersoPercent.Text = PercentpersonalityT.ToString();

            //Sub Category Personality

            //Sub-Category Personality1
            int PercentsubpersonalityT1 = (int)finalFacebookResults[3];
            Personality1.Attributes["aria-valuemin"] = "0";
            Personality1.Attributes["aria-valuemax"] = "100";
            Personality1.Attributes["aria-valuenow"] = PercentsubpersonalityT1.ToString();
            Personality1.Style["width"] = String.Format("{0}%;", PercentsubpersonalityT1);
            Personality1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentsubpersonalityT1)));
            LPersonality1.Text = PercentsubpersonalityT1.ToString();

            //Sub-Category Personality2
            int PercentsubpersonalityT2 = (int)finalFacebookResults[4];
            Personality2.Attributes["aria-valuemin"] = "0";
            Personality2.Attributes["aria-valuemax"] = "100";
            Personality2.Attributes["aria-valuenow"] = PercentsubpersonalityT2.ToString();
            Personality2.Style["width"] = String.Format("{0}%;", PercentsubpersonalityT2);
            Personality2.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentsubpersonalityT2)));
            LPersonality2.Text = PercentsubpersonalityT2.ToString();

            //Sub-Category Personality3
            int PercentsubpersonalityT3 = (int)finalFacebookResults[5];
            Personality3.Attributes["aria-valuemin"] = "0";
            Personality3.Attributes["aria-valuemax"] = "100";
            Personality3.Attributes["aria-valuenow"] = PercentsubpersonalityT3.ToString();
            Personality3.Style["width"] = String.Format("{0}%;", PercentsubpersonalityT3);
            Personality3.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentsubpersonalityT3)));
            LPersonality3.Text = PercentsubpersonalityT3.ToString();

            //Sub-Category Personality4
            int PercentsubpersonalityT4 = (int)finalFacebookResults[6];
            Personality4.Attributes["aria-valuemin"] = "0";
            Personality4.Attributes["aria-valuemax"] = "100";
            Personality4.Attributes["aria-valuenow"] = PercentsubpersonalityT4.ToString();
            Personality4.Style["width"] = String.Format("{0}%;", PercentsubpersonalityT4);
            Personality4.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentsubpersonalityT4)));
            LPersonality4.Text = PercentsubpersonalityT4.ToString();

            //Sub-Category Personality5
            int PercentsubpersonalityT5 = (int)finalFacebookResults[7];
            Personality5.Attributes["aria-valuemin"] = "0";
            Personality5.Attributes["aria-valuemax"] = "100";
            Personality5.Attributes["aria-valuenow"] = PercentsubpersonalityT5.ToString();
            Personality5.Style["width"] = String.Format("{0}%;", PercentsubpersonalityT5);
            Personality5.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentsubpersonalityT5)));
            LPersonality5.Text = PercentsubpersonalityT1.ToString();
            //double[] perValue = new double[] { finalFacebookResults[3], finalFacebookResults[4], finalFacebookResults[5], finalFacebookResults[6], finalFacebookResults[7], Session["ApplicantID"].ToString() };
            //Helper.UpdateDelete(sql, columns, perValue);



        }
    }
    void needs()
    {
        string sql = @"UPDATE tbl_Applicant SET Challenge = @Challenge, Closeness = @Closeness, Curiosity = @Curiosity, Excitement = @Excitement,
Harmony = @Harmony, Ideal= @Ideal, Liberty= @Liberty, Love= @Love, Practicality = @Practicality, SelfExpression = @SelfExpression, Stability=@Stability, Structure = @Structure WHERE ApplicantID = @ApplicantID";
        string[] columns = new string[] { "@Challenge", "@Closeness", "@Curiosity", "@Excitement", "@Harmony", "@Ideal", "@Liberty", "@Love", "@Practicality", "@SelfExpression", "@Stability", "@Structure", "@ApplicantID" };
        if (Session["ApplicantID"] != null & Session["AccessToken"] != null)
        {
            //Needs
            int PercentNeedsT = (int)averagedValues[1];
            NeedsT.Attributes["aria-valuemin"] = "0";
            NeedsT.Attributes["aria-valuemax"] = "100";
            NeedsT.Attributes["aria-valuenow"] = PercentNeedsT.ToString();
            NeedsT.Style["width"] = String.Format("{0}%;", PercentNeedsT);
            NeedsT.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeedsT)));
            needPercent.Text = PercentNeedsT.ToString();

            //Sub Category Personality

            //Sub-Category Needs1
            int PercentNeeds1 = (int)averagedValues[8];
            Needs1.Attributes["aria-valuemin"] = "0";
            Needs1.Attributes["aria-valuemax"] = "100";
            Needs1.Attributes["aria-valuenow"] = PercentNeeds1.ToString();
            Needs1.Style["width"] = String.Format("{0}%;", PercentNeeds1);
            Needs1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds1)));
            LNeeds1.Text = PercentNeeds1.ToString();

            //Sub-Category Needs2
            int PercentNeeds2 = (int)averagedValues[9];
            Needs2.Attributes["aria-valuemin"] = "0";
            Needs2.Attributes["aria-valuemax"] = "100";
            Needs2.Attributes["aria-valuenow"] = PercentNeeds2.ToString();
            Needs2.Style["width"] = String.Format("{0}%;", PercentNeeds2);
            Needs2.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds2)));
            LNeeds2.Text = PercentNeeds2.ToString();

            //Sub-Category Needs3
            int PercentNeeds3 = (int)averagedValues[10];
            Needs3.Attributes["aria-valuemin"] = "0";
            Needs3.Attributes["aria-valuemax"] = "100";
            Needs3.Attributes["aria-valuenow"] = PercentNeeds3.ToString();
            Needs3.Style["width"] = String.Format("{0}%;", PercentNeeds3);
            Needs3.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds3)));
            LNeeds3.Text = PercentNeeds3.ToString();

            //Sub-Category Needs4
            int PercentNeeds4 = (int)averagedValues[11];
            Needs4.Attributes["aria-valuemin"] = "0";
            Needs4.Attributes["aria-valuemax"] = "100";
            Needs4.Attributes["aria-valuenow"] = PercentNeeds4.ToString();
            Needs4.Style["width"] = String.Format("{0}%;", PercentNeeds4);
            Needs4.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds4)));
            LNeeds4.Text = PercentNeeds4.ToString();

            //Sub-Category Needs5
            int PercentNeeds5 = (int)averagedValues[12];
            Needs5.Attributes["aria-valuemin"] = "0";
            Needs5.Attributes["aria-valuemax"] = "100";
            Needs5.Attributes["aria-valuenow"] = PercentNeeds5.ToString();
            Needs5.Style["width"] = String.Format("{0}%;", PercentNeeds5);
            Needs5.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds5)));
            LNeeds5.Text = PercentNeeds5.ToString();

            //Sub-Category Needs6
            int PercentNeeds6 = (int)averagedValues[13];
            Needs6.Attributes["aria-valuemin"] = "0";
            Needs6.Attributes["aria-valuemax"] = "100";
            Needs6.Attributes["aria-valuenow"] = PercentNeeds6.ToString();
            Needs6.Style["width"] = String.Format("{0}%;", PercentNeeds6);
            Needs6.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds6)));
            LNeeds6.Text = PercentNeeds6.ToString();

            //Sub-Category Needs7
            int PercentNeeds7 = (int)averagedValues[14];
            Needs7.Attributes["aria-valuemin"] = "0";
            Needs7.Attributes["aria-valuemax"] = "100";
            Needs7.Attributes["aria-valuenow"] = PercentNeeds7.ToString();
            Needs7.Style["width"] = String.Format("{0}%;", PercentNeeds7);
            Needs7.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds7)));
            LNeeds7.Text = PercentNeeds7.ToString();

            //Sub-Category Needs8
            int PercentNeeds8 = (int)averagedValues[15];
            Needs8.Attributes["aria-valuemin"] = "0";
            Needs8.Attributes["aria-valuemax"] = "100";
            Needs8.Attributes["aria-valuenow"] = PercentNeeds8.ToString();
            Needs8.Style["width"] = String.Format("{0}%;", PercentNeeds8);
            Needs8.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds8)));
            LNeeds8.Text = PercentNeeds8.ToString();

            //Sub-Category Needs9
            int PercentNeeds9 = (int)averagedValues[16];
            Needs9.Attributes["aria-valuemin"] = "0";
            Needs9.Attributes["aria-valuemax"] = "100";
            Needs9.Attributes["aria-valuenow"] = PercentNeeds9.ToString();
            Needs9.Style["width"] = String.Format("{0}%;", PercentNeeds9);
            Needs9.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds9)));
            LNeeds9.Text = PercentNeeds9.ToString();

            //Sub-Category Needs10
            int PercentNeeds10 = (int)averagedValues[17];
            Needs10.Attributes["aria-valuemin"] = "0";
            Needs10.Attributes["aria-valuemax"] = "100";
            Needs10.Attributes["aria-valuenow"] = PercentNeeds10.ToString();
            Needs10.Style["width"] = String.Format("{0}%;", PercentNeeds10);
            Needs10.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds10)));
            LNeeds10.Text = PercentNeeds10.ToString();

            //Sub-Category Needs11
            int PercentNeeds11 = (int)averagedValues[18];
            Needs11.Attributes["aria-valuemin"] = "0";
            Needs11.Attributes["aria-valuemax"] = "100";
            Needs11.Attributes["aria-valuenow"] = PercentNeeds11.ToString();
            Needs11.Style["width"] = String.Format("{0}%;", PercentNeeds11);
            Needs11.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds11)));
            LNeeds11.Text = PercentNeeds11.ToString();

            //Sub-Category Needs12
            int PercentNeeds12 = (int)averagedValues[19];
            Needs12.Attributes["aria-valuemin"] = "0";
            Needs12.Attributes["aria-valuemax"] = "100";
            Needs12.Attributes["aria-valuenow"] = PercentNeeds12.ToString();
            Needs12.Style["width"] = String.Format("{0}%;", PercentNeeds12);
            Needs12.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds12)));
            LNeeds12.Text = PercentNeeds12.ToString();
            string[] perValue = new string[] { averagedValues[8].ToString(), averagedValues[9].ToString(), averagedValues[10].ToString(), averagedValues[11].ToString(), averagedValues[12].ToString(), averagedValues[13].ToString(),
            averagedValues[14].ToString(), averagedValues[15].ToString(), averagedValues[16].ToString(), averagedValues[17].ToString(), averagedValues[18].ToString(), averagedValues[19].ToString(), Session["ApplicantID"].ToString() };
            Helper.UpdateDelete(sql, columns, perValue);
        }
        else if (Session["ApplicantID"] != null & Session["AccessToken"] == null)
        {
            //Needs
            int PercentNeedsT = (int)finalResults[1];
            NeedsT.Attributes["aria-valuemin"] = "0";
            NeedsT.Attributes["aria-valuemax"] = "100";
            NeedsT.Attributes["aria-valuenow"] = PercentNeedsT.ToString();
            NeedsT.Style["width"] = String.Format("{0}%;", PercentNeedsT);
            NeedsT.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeedsT)));
            needPercent.Text = PercentNeedsT.ToString();

            //Sub Category Personality

            //Sub-Category Needs1
            int PercentNeeds1 = (int)finalResults[8];
            Needs1.Attributes["aria-valuemin"] = "0";
            Needs1.Attributes["aria-valuemax"] = "100";
            Needs1.Attributes["aria-valuenow"] = PercentNeeds1.ToString();
            Needs1.Style["width"] = String.Format("{0}%;", PercentNeeds1);
            Needs1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds1)));
            LNeeds1.Text = PercentNeeds1.ToString();

            //Sub-Category Needs2
            int PercentNeeds2 = (int)finalResults[9];
            Needs2.Attributes["aria-valuemin"] = "0";
            Needs2.Attributes["aria-valuemax"] = "100";
            Needs2.Attributes["aria-valuenow"] = PercentNeeds2.ToString();
            Needs2.Style["width"] = String.Format("{0}%;", PercentNeeds2);
            Needs2.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds2)));
            LNeeds2.Text = PercentNeeds2.ToString();

            //Sub-Category Needs3
            int PercentNeeds3 = (int)finalResults[10];
            Needs3.Attributes["aria-valuemin"] = "0";
            Needs3.Attributes["aria-valuemax"] = "100";
            Needs3.Attributes["aria-valuenow"] = PercentNeeds3.ToString();
            Needs3.Style["width"] = String.Format("{0}%;", PercentNeeds3);
            Needs3.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds3)));
            LNeeds3.Text = PercentNeeds3.ToString();

            //Sub-Category Needs4
            int PercentNeeds4 = (int)finalResults[11];
            Needs4.Attributes["aria-valuemin"] = "0";
            Needs4.Attributes["aria-valuemax"] = "100";
            Needs4.Attributes["aria-valuenow"] = PercentNeeds4.ToString();
            Needs4.Style["width"] = String.Format("{0}%;", PercentNeeds4);
            Needs4.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds4)));
            LNeeds4.Text = PercentNeeds4.ToString();

            //Sub-Category Needs5
            int PercentNeeds5 = (int)finalResults[12];
            Needs5.Attributes["aria-valuemin"] = "0";
            Needs5.Attributes["aria-valuemax"] = "100";
            Needs5.Attributes["aria-valuenow"] = PercentNeeds5.ToString();
            Needs5.Style["width"] = String.Format("{0}%;", PercentNeeds5);
            Needs5.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds5)));
            LNeeds5.Text = PercentNeeds5.ToString();

            //Sub-Category Needs6
            int PercentNeeds6 = (int)finalResults[13];
            Needs6.Attributes["aria-valuemin"] = "0";
            Needs6.Attributes["aria-valuemax"] = "100";
            Needs6.Attributes["aria-valuenow"] = PercentNeeds6.ToString();
            Needs6.Style["width"] = String.Format("{0}%;", PercentNeeds6);
            Needs6.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds6)));
            LNeeds6.Text = PercentNeeds6.ToString();

            //Sub-Category Needs7
            int PercentNeeds7 = (int)finalResults[14];
            Needs7.Attributes["aria-valuemin"] = "0";
            Needs7.Attributes["aria-valuemax"] = "100";
            Needs7.Attributes["aria-valuenow"] = PercentNeeds7.ToString();
            Needs7.Style["width"] = String.Format("{0}%;", PercentNeeds7);
            Needs7.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds7)));
            LNeeds7.Text = PercentNeeds7.ToString();

            //Sub-Category Needs8
            int PercentNeeds8 = (int)finalResults[15];
            Needs8.Attributes["aria-valuemin"] = "0";
            Needs8.Attributes["aria-valuemax"] = "100";
            Needs8.Attributes["aria-valuenow"] = PercentNeeds8.ToString();
            Needs8.Style["width"] = String.Format("{0}%;", PercentNeeds8);
            Needs8.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds8)));
            LNeeds8.Text = PercentNeeds8.ToString();

            //Sub-Category Needs9
            int PercentNeeds9 = (int)finalResults[16];
            Needs9.Attributes["aria-valuemin"] = "0";
            Needs9.Attributes["aria-valuemax"] = "100";
            Needs9.Attributes["aria-valuenow"] = PercentNeeds9.ToString();
            Needs9.Style["width"] = String.Format("{0}%;", PercentNeeds9);
            Needs9.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds9)));
            LNeeds9.Text = PercentNeeds9.ToString();

            //Sub-Category Needs10
            int PercentNeeds10 = (int)finalResults[17];
            Needs10.Attributes["aria-valuemin"] = "0";
            Needs10.Attributes["aria-valuemax"] = "100";
            Needs10.Attributes["aria-valuenow"] = PercentNeeds10.ToString();
            Needs10.Style["width"] = String.Format("{0}%;", PercentNeeds10);
            Needs10.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds10)));
            LNeeds10.Text = PercentNeeds10.ToString();

            //Sub-Category Needs11
            int PercentNeeds11 = (int)finalResults[18];
            Needs11.Attributes["aria-valuemin"] = "0";
            Needs11.Attributes["aria-valuemax"] = "100";
            Needs11.Attributes["aria-valuenow"] = PercentNeeds11.ToString();
            Needs11.Style["width"] = String.Format("{0}%;", PercentNeeds11);
            Needs11.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds11)));
            LNeeds11.Text = PercentNeeds11.ToString();

            //Sub-Category Needs12
            int PercentNeeds12 = (int)finalResults[19];
            Needs12.Attributes["aria-valuemin"] = "0";
            Needs12.Attributes["aria-valuemax"] = "100";
            Needs12.Attributes["aria-valuenow"] = PercentNeeds12.ToString();
            Needs12.Style["width"] = String.Format("{0}%;", PercentNeeds12);
            Needs12.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds12)));
            LNeeds12.Text = PercentNeeds12.ToString();
            string[] perValue = new string[] { finalResults[8].ToString(), finalResults[9].ToString(), finalResults[10].ToString(), finalResults[11].ToString(), finalResults[12].ToString(), finalResults[13].ToString(),
            finalResults[14].ToString(), finalResults[15].ToString(), finalResults[16].ToString(), finalResults[17].ToString(), finalResults[18].ToString(), finalResults[19].ToString(), Session["ApplicantID"].ToString() };
            Helper.UpdateDelete(sql, columns, perValue);
        }
        else if (Session["AccessToken"] != null & Session["ApplicantID"] == null)
        {
            //Needs
            int PercentNeedsT = (int)finalFacebookResults[1];
            NeedsT.Attributes["aria-valuemin"] = "0";
            NeedsT.Attributes["aria-valuemax"] = "100";
            NeedsT.Attributes["aria-valuenow"] = PercentNeedsT.ToString();
            NeedsT.Style["width"] = String.Format("{0}%;", PercentNeedsT);
            NeedsT.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeedsT)));
            needPercent.Text = PercentNeedsT.ToString();

            //Sub Category Personality

            //Sub-Category Needs1
            int PercentNeeds1 = (int)finalFacebookResults[8];
            Needs1.Attributes["aria-valuemin"] = "0";
            Needs1.Attributes["aria-valuemax"] = "100";
            Needs1.Attributes["aria-valuenow"] = PercentNeeds1.ToString();
            Needs1.Style["width"] = String.Format("{0}%;", PercentNeeds1);
            Needs1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds1)));
            LNeeds1.Text = PercentNeeds1.ToString();

            //Sub-Category Needs2
            int PercentNeeds2 = (int)finalFacebookResults[9];
            Needs2.Attributes["aria-valuemin"] = "0";
            Needs2.Attributes["aria-valuemax"] = "100";
            Needs2.Attributes["aria-valuenow"] = PercentNeeds2.ToString();
            Needs2.Style["width"] = String.Format("{0}%;", PercentNeeds2);
            Needs2.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds2)));
            LNeeds2.Text = PercentNeeds2.ToString();

            //Sub-Category Needs3
            int PercentNeeds3 = (int)finalFacebookResults[10];
            Needs3.Attributes["aria-valuemin"] = "0";
            Needs3.Attributes["aria-valuemax"] = "100";
            Needs3.Attributes["aria-valuenow"] = PercentNeeds3.ToString();
            Needs3.Style["width"] = String.Format("{0}%;", PercentNeeds3);
            Needs3.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds3)));
            LNeeds3.Text = PercentNeeds3.ToString();

            //Sub-Category Needs4
            int PercentNeeds4 = (int)finalFacebookResults[11];
            Needs4.Attributes["aria-valuemin"] = "0";
            Needs4.Attributes["aria-valuemax"] = "100";
            Needs4.Attributes["aria-valuenow"] = PercentNeeds4.ToString();
            Needs4.Style["width"] = String.Format("{0}%;", PercentNeeds4);
            Needs4.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds4)));
            LNeeds4.Text = PercentNeeds4.ToString();

            //Sub-Category Needs5
            int PercentNeeds5 = (int)finalFacebookResults[12];
            Needs5.Attributes["aria-valuemin"] = "0";
            Needs5.Attributes["aria-valuemax"] = "100";
            Needs5.Attributes["aria-valuenow"] = PercentNeeds5.ToString();
            Needs5.Style["width"] = String.Format("{0}%;", PercentNeeds5);
            Needs5.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds5)));
            LNeeds5.Text = PercentNeeds5.ToString();

            //Sub-Category Needs6
            int PercentNeeds6 = (int)finalFacebookResults[13];
            Needs6.Attributes["aria-valuemin"] = "0";
            Needs6.Attributes["aria-valuemax"] = "100";
            Needs6.Attributes["aria-valuenow"] = PercentNeeds6.ToString();
            Needs6.Style["width"] = String.Format("{0}%;", PercentNeeds6);
            Needs6.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds6)));
            LNeeds6.Text = PercentNeeds6.ToString();

            //Sub-Category Needs7
            int PercentNeeds7 = (int)finalFacebookResults[14];
            Needs7.Attributes["aria-valuemin"] = "0";
            Needs7.Attributes["aria-valuemax"] = "100";
            Needs7.Attributes["aria-valuenow"] = PercentNeeds7.ToString();
            Needs7.Style["width"] = String.Format("{0}%;", PercentNeeds7);
            Needs7.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds7)));
            LNeeds7.Text = PercentNeeds7.ToString();

            //Sub-Category Needs8
            int PercentNeeds8 = (int)finalFacebookResults[15];
            Needs8.Attributes["aria-valuemin"] = "0";
            Needs8.Attributes["aria-valuemax"] = "100";
            Needs8.Attributes["aria-valuenow"] = PercentNeeds8.ToString();
            Needs8.Style["width"] = String.Format("{0}%;", PercentNeeds8);
            Needs8.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds8)));
            LNeeds8.Text = PercentNeeds8.ToString();

            //Sub-Category Needs9
            int PercentNeeds9 = (int)finalFacebookResults[16];
            Needs9.Attributes["aria-valuemin"] = "0";
            Needs9.Attributes["aria-valuemax"] = "100";
            Needs9.Attributes["aria-valuenow"] = PercentNeeds9.ToString();
            Needs9.Style["width"] = String.Format("{0}%;", PercentNeeds9);
            Needs9.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds9)));
            LNeeds9.Text = PercentNeeds9.ToString();

            //Sub-Category Needs10
            int PercentNeeds10 = (int)finalFacebookResults[17];
            Needs10.Attributes["aria-valuemin"] = "0";
            Needs10.Attributes["aria-valuemax"] = "100";
            Needs10.Attributes["aria-valuenow"] = PercentNeeds10.ToString();
            Needs10.Style["width"] = String.Format("{0}%;", PercentNeeds10);
            Needs10.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds10)));
            LNeeds10.Text = PercentNeeds10.ToString();

            //Sub-Category Needs11
            int PercentNeeds11 = (int)finalFacebookResults[18];
            Needs11.Attributes["aria-valuemin"] = "0";
            Needs11.Attributes["aria-valuemax"] = "100";
            Needs11.Attributes["aria-valuenow"] = PercentNeeds11.ToString();
            Needs11.Style["width"] = String.Format("{0}%;", PercentNeeds11);
            Needs11.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds11)));
            LNeeds11.Text = PercentNeeds11.ToString();

            //Sub-Category Needs12
            int PercentNeeds12 = (int)finalFacebookResults[19];
            Needs12.Attributes["aria-valuemin"] = "0";
            Needs12.Attributes["aria-valuemax"] = "100";
            Needs12.Attributes["aria-valuenow"] = PercentNeeds12.ToString();
            Needs12.Style["width"] = String.Format("{0}%;", PercentNeeds12);
            Needs12.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentNeeds12)));
            LNeeds12.Text = PercentNeeds12.ToString();
            string[] perValue = new string[] { finalFacebookResults[8].ToString(), finalFacebookResults[9].ToString(), finalFacebookResults[10].ToString(), finalFacebookResults[11].ToString(), finalFacebookResults[12].ToString(), finalFacebookResults[13].ToString(),
            finalFacebookResults[14].ToString(), finalFacebookResults[15].ToString(), finalFacebookResults[16].ToString(), finalFacebookResults[17].ToString(), finalFacebookResults[18].ToString(), finalFacebookResults[19].ToString(), Session["ApplicantID"].ToString() };
            //Helper.UpdateDelete(sql, columns, perValue);
        }
    }
    void values()
    {
        string sql = @"UPDATE tbl_Applicant SET Conservation= @Conservation, OpennessToChange = @OpennessToChange, Hedonism = @Hedonism,
SelfEnhancement = @SelfEnhancement, SelfTranscendence = @SelfTranscendence WHERE ApplicantID = @ApplicantID";
        string[] columns = new string[] { "@Conservation", "@OpennessToChange", "@Hedonism", "@SelfEnhancement", "@SelfTranscendence", "@ApplicantID" };


        if (Session["ApplicantID"] != null & Session["AccessToken"] != null)
        {
            //Values
            int PercentValuesT = (int)averagedValues[2];
            ValuesT.Attributes["aria-valuemin"] = "0";
            ValuesT.Attributes["aria-valuemax"] = "100";
            ValuesT.Attributes["aria-valuenow"] = PercentValuesT.ToString();
            ValuesT.Style["width"] = String.Format("{0}%;", PercentValuesT);
            ValuesT.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValuesT)));
            ValuesPercent.Text = PercentValuesT.ToString();

            int PercentValues1 = (int)averagedValues[20];
            Values1.Attributes["aria-valuemin"] = "0";
            Values1.Attributes["aria-valuemax"] = "100";
            Values1.Attributes["aria-valuenow"] = PercentValues1.ToString();
            Values1.Style["width"] = String.Format("{0}%;", PercentValues1);
            Values1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValues1)));
            Lvalues1.Text = PercentValues1.ToString();

            int PercentValues2 = (int)averagedValues[21];
            Values2.Attributes["aria-valuemin"] = "0";
            Values2.Attributes["aria-valuemax"] = "100";
            Values2.Attributes["aria-valuenow"] = PercentValues2.ToString();
            Values2.Style["width"] = String.Format("{0}%;", PercentValues2);
            Values2.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValues2)));
            Lvalues2.Text = PercentValues2.ToString();

            int PercentValues3 = (int)averagedValues[22];
            Values3.Attributes["aria-valuemin"] = "0";
            Values3.Attributes["aria-valuemax"] = "100";
            Values3.Attributes["aria-valuenow"] = PercentValues3.ToString();
            Values3.Style["width"] = String.Format("{0}%;", PercentValues3);
            Values3.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValues3)));
            Lvalues3.Text = PercentValues3.ToString();

            int PercentValues4 = (int)averagedValues[23];
            Values4.Attributes["aria-valuemin"] = "0";
            Values4.Attributes["aria-valuemax"] = "100";
            Values4.Attributes["aria-valuenow"] = PercentValues4.ToString();
            Values4.Style["width"] = String.Format("{0}%;", PercentValues4);
            Values4.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValues4)));
            Lvalues4.Text = PercentValues4.ToString();

            int PercentValues5 = (int)averagedValues[24];
            Values5.Attributes["aria-valuemin"] = "0";
            Values5.Attributes["aria-valuemax"] = "100";
            Values5.Attributes["aria-valuenow"] = PercentValues5.ToString();
            Values5.Style["width"] = String.Format("{0}%;", PercentValues5);
            Values5.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValues5)));

            Lvalues5.Text = PercentValues5.ToString();
            string[] perValue = new string[] { averagedValues[20].ToString(), averagedValues[21].ToString(), averagedValues[22].ToString(), averagedValues[23].ToString(), averagedValues[24].ToString(), Session["ApplicantID"].ToString() };
            Helper.UpdateDelete(sql, columns, perValue);
        }
        else if (Session["ApplicantID"] != null & Session["AccessToken"] == null)
        {
            //Values
            int PercentValuesT = (int)finalResults[2];
            ValuesT.Attributes["aria-valuemin"] = "0";
            ValuesT.Attributes["aria-valuemax"] = "100";
            ValuesT.Attributes["aria-valuenow"] = PercentValuesT.ToString();
            ValuesT.Style["width"] = String.Format("{0}%;", PercentValuesT);
            ValuesT.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValuesT)));
            ValuesPercent.Text = PercentValuesT.ToString();

            int PercentValues1 = (int)finalResults[20];
            Values1.Attributes["aria-valuemin"] = "0";
            Values1.Attributes["aria-valuemax"] = "100";
            Values1.Attributes["aria-valuenow"] = PercentValues1.ToString();
            Values1.Style["width"] = String.Format("{0}%;", PercentValues1);
            Values1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValues1)));
            Lvalues1.Text = PercentValues1.ToString();

            int PercentValues2 = (int)finalResults[21];
            Values2.Attributes["aria-valuemin"] = "0";
            Values2.Attributes["aria-valuemax"] = "100";
            Values2.Attributes["aria-valuenow"] = PercentValues2.ToString();
            Values2.Style["width"] = String.Format("{0}%;", PercentValues2);
            Values2.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValues2)));
            Lvalues2.Text = PercentValues2.ToString();

            int PercentValues3 = (int)finalResults[22];
            Values3.Attributes["aria-valuemin"] = "0";
            Values3.Attributes["aria-valuemax"] = "100";
            Values3.Attributes["aria-valuenow"] = PercentValues3.ToString();
            Values3.Style["width"] = String.Format("{0}%;", PercentValues3);
            Values3.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValues3)));
            Lvalues3.Text = PercentValues3.ToString();

            int PercentValues4 = (int)finalResults[23];
            Values4.Attributes["aria-valuemin"] = "0";
            Values4.Attributes["aria-valuemax"] = "100";
            Values4.Attributes["aria-valuenow"] = PercentValues4.ToString();
            Values4.Style["width"] = String.Format("{0}%;", PercentValues4);
            Values4.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValues4)));
            Lvalues4.Text = PercentValues4.ToString();

            int PercentValues5 = (int)finalResults[24];
            Values5.Attributes["aria-valuemin"] = "0";
            Values5.Attributes["aria-valuemax"] = "100";
            Values5.Attributes["aria-valuenow"] = PercentValues5.ToString();
            Values5.Style["width"] = String.Format("{0}%;", PercentValues5);
            Values5.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValues5)));
            Lvalues5.Text = PercentValues5.ToString();
            string[] perValue = new string[] { finalResults[20].ToString(), finalResults[21].ToString(), finalResults[22].ToString(), finalResults[23].ToString(), finalResults[24].ToString(), Session["ApplicantID"].ToString() };
             Helper.UpdateDelete(sql, columns, perValue);
        }
        else if (Session["AccessToken"] != null & Session["ApplicantID"] == null)
        {
            //Values
            int PercentValuesT = (int)finalFacebookResults[2];
            ValuesT.Attributes["aria-valuemin"] = "0";
            ValuesT.Attributes["aria-valuemax"] = "100";
            ValuesT.Attributes["aria-valuenow"] = PercentValuesT.ToString();
            ValuesT.Style["width"] = String.Format("{0}%;", PercentValuesT);
            ValuesT.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValuesT)));
            ValuesPercent.Text = PercentValuesT.ToString();

            int PercentValues1 = (int)finalFacebookResults[20];
            Values1.Attributes["aria-valuemin"] = "0";
            Values1.Attributes["aria-valuemax"] = "100";
            Values1.Attributes["aria-valuenow"] = PercentValues1.ToString();
            Values1.Style["width"] = String.Format("{0}%;", PercentValues1);
            Values1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValues1)));
            Lvalues1.Text = PercentValues1.ToString();

            int PercentValues2 = (int)finalFacebookResults[21];
            Values2.Attributes["aria-valuemin"] = "0";
            Values2.Attributes["aria-valuemax"] = "100";
            Values2.Attributes["aria-valuenow"] = PercentValues2.ToString();
            Values2.Style["width"] = String.Format("{0}%;", PercentValues2);
            Values2.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValues2)));
            Lvalues2.Text = PercentValues2.ToString();

            int PercentValues3 = (int)finalFacebookResults[22];
            Values3.Attributes["aria-valuemin"] = "0";
            Values3.Attributes["aria-valuemax"] = "100";
            Values3.Attributes["aria-valuenow"] = PercentValues3.ToString();
            Values3.Style["width"] = String.Format("{0}%;", PercentValues3);
            Values3.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValues3)));
            Lvalues3.Text = PercentValues3.ToString();

            int PercentValues4 = (int)finalFacebookResults[23];
            Values4.Attributes["aria-valuemin"] = "0";
            Values4.Attributes["aria-valuemax"] = "100";
            Values4.Attributes["aria-valuenow"] = PercentValues4.ToString();
            Values4.Style["width"] = String.Format("{0}%;", PercentValues4);
            Values4.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValues4)));
            Lvalues4.Text = PercentValues4.ToString();

            int PercentValues5 = (int)finalFacebookResults[24];
            Values5.Attributes["aria-valuemin"] = "0";
            Values5.Attributes["aria-valuemax"] = "100";
            Values5.Attributes["aria-valuenow"] = PercentValues5.ToString();
            Values5.Style["width"] = String.Format("{0}%;", PercentValues5);
            Values5.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentValues5)));
            Lvalues5.Text = PercentValues5.ToString();
            //double[] perValue = new double[] { finalFacebookResults[2], finalFacebookResults[20], finalFacebookResults[21], finalFacebookResults[22], finalFacebookResults[23], finalFacebookResults[24] };
            //Helper.UpdateDelete(sql, columns, perValue);
        }
    }


    //PROFILE COMPLETION
    void profilecomplete()
    {
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            string SQL = @"Select DISTINCT ap.MaritalStatus,ap.StreetAddress,ap.Telephone
				,ap.Mobile,ap.CallTime,ap.CallMedium,ap.Summary
				,Gender,Birthdate,Nationality,CertificationName
				,Interest,Language,OrganizationName,PatentTitle,ProjectName
				,Skill
from tbl_Applicant AS ap 
LEFT JOIN (Select top 1 AC.ApplicantID ,C.CertificationName from tbl_ApplicantCertification AS AC Left join tbl_Certification AS C ON AC.CertificationID = C.CertificationID) as UC ON ap.ApplicantID = UC.ApplicantID
LEFT JOIN (Select top 1 AC.ApplicantID ,C.Interest from tbl_Interest AS C INNER JOIN tbl_ApplicantInterest AS AC ON AC.InterestID = C.InterestID) AS UI ON ap.ApplicantID = UI.ApplicantID   
LEFT JOIN (Select top 1 AC.ApplicantID ,C.Language from tbl_ApplicantLanguage AS AC Left join tbl_Language AS C ON AC.LanguageID = C.LanguageID) as UL ON ap.ApplicantID = UL.ApplicantID        
LEFT JOIN (Select top 1 AC.ApplicantID ,C.OrganizationName from tbl_ApplicantOrganization AS AC Left join tbl_Organization AS C ON AC.OrganizationID = C.OrganizationID) AS UO ON ap.ApplicantID = UO.ApplicantID
LEFT JOIN (Select top 1 AC.ApplicantID ,C.PatentTitle from tbl_PatentInventor AS AC Left Join tbl_Patent AS C ON AC.PatentID = C.PatentID) AS UP ON ap.ApplicantID = UP.ApplicantID
LEFT JOIN (Select top 1 AC.ApplicantID ,C.ProjectName from tbl_ProjectMember AS AC Left Join tbl_Project AS C ON AC.ProjectID = C.ProjectID) AS UPR ON ap.ApplicantID = UPR.ApplicantID
LEFT JOIN (Select top 1 AC.ApplicantID ,C.Skill from tbl_ApplicantSkills AS AC Left Join tbl_Skills AS C ON AC.ApplicantSkillsID = C.SKillID) AS US ON ap.ApplicantID = US.ApplicantID Where ApplicantID = @ApplicantID";
            using (SqlCommand cmd = new SqlCommand(SQL, con))
            {
                cmd.Parameters.AddWithValue("@ApplicantID", Session["ApplicantID"].ToString());
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {

                        Genderla.Text = dr["Gender"].ToString();
                        Birthdatela.Text = dr["Birthdate"].ToString();
                        MaritalStatusla.Text = dr["MaritalStatus"].ToString();
                        Mobilela.Text = dr["Mobile"].ToString();
                        Nationalityla.Text = dr["Nationality"].ToString();
                        StreetAddressla.Text = dr["StreetAddress"].ToString();
                        CallTimela.Text = dr["CallTime"].ToString();
                        CallMediumla.Text = dr["CallMedium"].ToString();
                        Summaryla.Text = dr["Summary"].ToString();
                        Cetificationla.Text = dr["CertificationName"].ToString();
                        Interestla.Text = dr["Interest"].ToString();
                        Languagela.Text = dr["Language"].ToString();
                        Organizationla.Text = dr["OrganizationName"].ToString();
                        Patentla.Text = dr["PatentTitle"].ToString();
                        Projectla.Text = dr["ProjectName"].ToString();
                        Skillsla.Text = dr["Skill"].ToString();


                    }
                }
            }
        }
    }
    void ProfileProgress()
    {
        if (profpic.ImageUrl != null && user.Text != null && city.Text != null && personalWebsite.Text != null)
        {
            int PercentPanel1 = 20;
            Panel1.Attributes["aria-valuemin"] = "0";
            Panel1.Attributes["aria-valuemax"] = "100";
            Panel1.Attributes["aria-valuenow"] = PercentPanel1.ToString();
            Panel1.Style["width"] = String.Format("{0}%;", PercentPanel1);
            Panel1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentPanel1)));

            String SQL = @"Update tbl_Applicant SET progress = @progress Where ApplicantID = @ApplicantID";
            string[] col = { "@progress", "@ApplicantID" };
            string[] textbox = { PercentPanel1.ToString(), Session["ApplicantID"].ToString() };
            Helper.UpdateDelete(SQL, col, textbox);
        }
        else if (Genderla.Text != null && Birthdatela.Text != null && MaritalStatusla.Text != null && Mobilela.Text != null && Nationalityla.Text != null && StreetAddressla.Text != null)
        {
            int PercentPanel1 = 50;
            Panel1.Attributes["aria-valuemin"] = "0";
            Panel1.Attributes["aria-valuemax"] = "100";
            Panel1.Attributes["aria-valuenow"] = PercentPanel1.ToString();
            Panel1.Style["width"] = String.Format("{0}%;", PercentPanel1);
            Panel1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentPanel1)));
        }
        else if (CallTimela.Text != null && CallMediumla.Text != null && Summaryla.Text != null)
        {
            int PercentPanel1 = 65;
            Panel1.Attributes["aria-valuemin"] = "0";
            Panel1.Attributes["aria-valuemax"] = "100";
            Panel1.Attributes["aria-valuenow"] = PercentPanel1.ToString();
            Panel1.Style["width"] = String.Format("{0}%;", PercentPanel1);
            Panel1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentPanel1)));

            String SQL = @"Update tbl_Applicant SET progress = @progress Where ApplicantID = @ApplicantID";
            string[] col = { "@progress", "@ApplicantID" };
            string[] textbox = { PercentPanel1.ToString(), Session["ApplicantID"].ToString() };
            Helper.UpdateDelete(SQL, col, textbox);
        }
        else if (Cetificationla.Text != null)
        {
            int PercentPanel1 = 70;
            Panel1.Attributes["aria-valuemin"] = "0";
            Panel1.Attributes["aria-valuemax"] = "100";
            Panel1.Attributes["aria-valuenow"] = PercentPanel1.ToString();
            Panel1.Style["width"] = String.Format("{0}%;", PercentPanel1);
            Panel1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentPanel1)));

            String SQL = @"Update tbl_Applicant SET progress = @progress Where ApplicantID = @ApplicantID";
            string[] col = { "@progress", "@ApplicantID" };
            string[] textbox = { PercentPanel1.ToString(), Session["ApplicantID"].ToString() };
            Helper.UpdateDelete(SQL, col, textbox);

        }
        else if (Interestla.Text != null)
        {
            int PercentPanel1 = 73;
            Panel1.Attributes["aria-valuemin"] = "0";
            Panel1.Attributes["aria-valuemax"] = "100";
            Panel1.Attributes["aria-valuenow"] = PercentPanel1.ToString();
            Panel1.Style["width"] = String.Format("{0}%;", PercentPanel1);
            Panel1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentPanel1)));

            String SQL = @"Update tbl_Applicant SET progress = @progress Where ApplicantID = @ApplicantID";
            string[] col = { "@progress", "@ApplicantID" };
            string[] textbox = { PercentPanel1.ToString(), Session["ApplicantID"].ToString() };
            Helper.UpdateDelete(SQL, col, textbox);
        }
        else if (Languagela.Text != null)
        {
            int PercentPanel1 = 75;
            Panel1.Attributes["aria-valuemin"] = "0";
            Panel1.Attributes["aria-valuemax"] = "100";
            Panel1.Attributes["aria-valuenow"] = PercentPanel1.ToString();
            Panel1.Style["width"] = String.Format("{0}%;", PercentPanel1);
            Panel1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentPanel1)));

            String SQL = @"Update tbl_Applicant SET progress = @progress Where ApplicantID = @ApplicantID";
            string[] col = { "@progress", "@ApplicantID" };
            string[] textbox = { PercentPanel1.ToString(), Session["ApplicantID"].ToString() };
            Helper.UpdateDelete(SQL, col, textbox);
        }
        else if (Skillsla.Text != null)
        {
            int PercentPanel1 = 80;
            Panel1.Attributes["aria-valuemin"] = "0";
            Panel1.Attributes["aria-valuemax"] = "100";
            Panel1.Attributes["aria-valuenow"] = PercentPanel1.ToString();
            Panel1.Style["width"] = String.Format("{0}%;", PercentPanel1);
            Panel1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentPanel1)));

            String SQL = @"Update tbl_Applicant SET progress = @progress Where ApplicantID = @ApplicantID";
            string[] col = { "@progress", "@ApplicantID" };
            string[] textbox = { PercentPanel1.ToString(), Session["ApplicantID"].ToString() };
            Helper.UpdateDelete(SQL, col, textbox);
        }
        else if (Organizationla.Text != null)
        {
            int PercentPanel1 = 90;
            Panel1.Attributes["aria-valuemin"] = "0";
            Panel1.Attributes["aria-valuemax"] = "100";
            Panel1.Attributes["aria-valuenow"] = PercentPanel1.ToString();
            Panel1.Style["width"] = String.Format("{0}%;", PercentPanel1);
            Panel1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentPanel1)));

            String SQL = @"Update tbl_Applicant SET progress = @progress Where ApplicantID = @ApplicantID";
            string[] col = { "@progress", "@ApplicantID" };
            string[] textbox = { PercentPanel1.ToString(), Session["ApplicantID"].ToString() };
            Helper.UpdateDelete(SQL, col, textbox);
        }
        else if (Patentla.Text != null)
        {
            int PercentPanel1 = 95;
            Panel1.Attributes["aria-valuemin"] = "0";
            Panel1.Attributes["aria-valuemax"] = "100";
            Panel1.Attributes["aria-valuenow"] = PercentPanel1.ToString();
            Panel1.Style["width"] = String.Format("{0}%;", PercentPanel1);
            Panel1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentPanel1)));

            String SQL = @"Update tbl_Applicant SET progress = @progress Where ApplicantID = @ApplicantID";
            string[] col = { "@progress", "@ApplicantID" };
            string[] textbox = { PercentPanel1.ToString(), Session["ApplicantID"].ToString() };
            Helper.UpdateDelete(SQL, col, textbox);
        }
        else if (Projectla.Text != null)
        {
            int PercentPanel1 = 100;
            Panel1.Attributes["aria-valuemin"] = "0";
            Panel1.Attributes["aria-valuemax"] = "100";
            Panel1.Attributes["aria-valuenow"] = PercentPanel1.ToString();
            Panel1.Style["width"] = String.Format("{0}%;", PercentPanel1);
            Panel1.Controls.Add(new LiteralControl(String.Format("{0}%;", PercentPanel1)));

            String SQL = @"Update tbl_Applicant SET progress = @progress Where ApplicantID = @ApplicantID";
            string[] col = { "@progress", "@ApplicantID" };
            string[] textbox = { PercentPanel1.ToString(), Session["ApplicantID"].ToString() };
            Helper.UpdateDelete(SQL, col, textbox);
        }
    }

    //USER STATUS
    void showuser()
    {
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            string SQL = @"Select ap.ApplicantID,ap.ProfilePicture,ap.LastName,ap.FirstName,ap.CityID,ap.Website,C.City,CO.Country  
from tbl_Applicant AS ap 
Left JOIN tbl_City AS C 
ON ap.CityID = C.CityID 
Left JOIN tbl_Region AS R 
ON C.RegionID = R.RegionID 
Left JOIN tbl_Country AS CO
ON R.CountryCode = CO.CountryID 
Where ap.ApplicantID = @ApplicantID";
            using (SqlCommand cmd = new SqlCommand(SQL, con))
            {
                cmd.Parameters.AddWithValue("@ApplicantID", Session["ApplicantID"].ToString());
                System.Diagnostics.Debug.WriteLine(Session["ApplicantID"].ToString());

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        profpic.ImageUrl = "/images/" + dr["ProfilePicture"].ToString(); ;
                        user.Text = dr["LastName"].ToString() + ", " + dr["FirstName"].ToString();
                        city.Text = " " + dr["City"].ToString() + ", " + dr["Country"].ToString();
                        personalWebsite.Text = " " + dr["Website"].ToString();


                    }
                }
            }
        }
    }
    void showuserprof()
    {
        String SQL = @"Select ap.MaritalStatus,ap.StreetAddress,ap.Telephone,ap.Mobile,ap.CallTime,ap.CallMedium,ap.Summary,Gender,Birthdate,Nationality
                        from tbl_Applicant AS ap 
                        Where ap.ApplicantID = @ApplicantID";
        Helper.SessionSelect(SQL, UserProfile, "@ApplicantID", "ApplicantID");
    }
    void usercertificate()
    {
        String SQL = @"Select C.CertificationName from tbl_ApplicantCertification AS AC Left join tbl_Certification AS C ON AC.CertificationID = C.CertificationID
                        Where AC.ApplicantID = @ApplicantID";
        Helper.SessionSelect(SQL, certificate, "@ApplicantID", "ApplicantID");
    }
    void userinterest()
    {
        String SQL = @"Select C.Interest from tbl_Interest AS C INNER JOIN tbl_ApplicantInterest AS AC ON AC.InterestID = C.InterestID Where AC.ApplicantID = @ApplicantID";
        Helper.SessionSelect(SQL, Interest, "@ApplicantID", "ApplicantID");
    }
    void userlang()
    {
        String SQL = @"Select Language from tbl_ApplicantLanguage AS AC Left join tbl_Language AS C ON AC.LanguageID = C.LanguageID
                        Where AC.ApplicantID = @ApplicantID";
        Helper.SessionSelect(SQL, Language, "@ApplicantID", "ApplicantID");
    }
    void userorg()
    {
        String SQL = @"Select OrganizationName from tbl_ApplicantOrganization AS AC Left join tbl_Organization AS C ON AC.OrganizationID = C.OrganizationID
                        Where AC.ApplicantID = @ApplicantID";
        Helper.SessionSelect(SQL, Organization, "@ApplicantID", "ApplicantID");
    }
    void userpat()
    {
        String SQL = @"Select PatentTitle from tbl_PatentInventor AS AC Left Join tbl_Patent AS C ON AC.PatentID = C.PatentID
                        Where AC.ApplicantID = @ApplicantID";
        Helper.SessionSelect(SQL, Patent, "@ApplicantID", "ApplicantID");
    }
    void userproj()
    {
        String SQL = @"Select ProjectName from tbl_ProjectMember AS AC Left Join tbl_Project AS C ON AC.ProjectID = C.ProjectID
                        Where AC.ApplicantID = @ApplicantID";
        Helper.SessionSelect(SQL, Project, "@ApplicantID", "ApplicantID");
    }
    void userskills()
    {
        String SQL = @"Select Skill from tbl_ApplicantSkills2 AS AC Left Join tbl_Skills AS C ON AC.ApplicantSkillsID = C.SKillID
                        Where AC.ApplicantID = @ApplicantID";
        Helper.SessionSelect(SQL, Skills, "@ApplicantID", "ApplicantID");
    }

    //showCertification,showLanguage,showSkills
    void showCertification()
    {

        string SQL = @"SELECT CertificationID, CertificationName FROM tbl_Certification";
        Helper.Select(SQL, ddlCertification, "CertificationName", "CertificationID");

    }
    void showLanguage()
    {

        string SQL = @"SELECT LanguageID, Language FROM tbl_Language";
        Helper.Select(SQL, ddlLanguage, "Language", "LanguageID");

    }
    void showSkills()
    {
        string SQL = @"SELECT SKillID, Skill FROM tbl_Skills";
        Helper.Select(SQL, ddlSkills, "Skill", "SKillID");
    }


    //BUTTONS
    protected void btnupdateprof_Click(object sender, EventArgs e)
    {
        ddl1.Visible = true;
        btnupdateprof.Visible = false;
        btnsave.Visible = true;
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        btnupdateprof.Visible = true;
        ddl1.Visible = false;
        btnsave.Visible = false;
    }

    protected void btncert_Click(object sender, EventArgs e)
    {
        string[] values = new string[] { "@ApplicantID", "@CertificationID", "@CertificationAuthor", "@LicenseNumber", "@StartMonth", "@StartYear", "@EndMonth", "@EndYear", "@Expires", "@CertURL" };
        string[] col = new string[] { "ApplicantID", "CertificationID", "CertificationAuthor", "LicenseNumber", "StartMonth", "StartYear", "EndMonth", "EndYear", "Expires", "CertURL" };
        string[] txt = new string[] { Session["ApplicantID"].ToString(), ddlCertification.SelectedValue, txtAuthor.Text, txtLicense.Text, ddlStartMonth.SelectedValue, txtStartYear.Text, ddlEndMonth.SelectedValue, txtEndYear.Text, txtExpires.Text, txtURL.Text };
        Helper.Insert("tbl_ApplicantCertification", values, txt, col);
    }

    protected void btninter_Click(object sender, EventArgs e)
    {
        String[] parameters = new string[1] { "@interestName" };
        String[] content = new string[1] { txtInterest.Text };
        int queryScope = Helper.InsertScope("tbl_Interest", parameters, content);
        if (queryScope != 0)
        {
            String[] secondParameters = new string[2] { "@Applicant", "@Interest" };
            //replace the '19' with logged in user's session (ApplicantID)
            String[] secondContent = new string[2] { Session["ApplicantID"].ToString(), Convert.ToString(queryScope) };
            Helper.Insert("tbl_ApplicantInterest", secondParameters, secondContent);
        }
        else
        {
            //throw error
        }
    }

    protected void btnlang_Click(object sender, EventArgs e)
    {
        string[] values = new string[] { "@ApplicantID", "@LanguageID", "@Profiency" };
        string[] col = new string[] { "ApplicantID", "LanguageID", "Profiency" };
        string[] txt = new string[] { Session["ApplicantID"].ToString(), ddlLanguage.SelectedValue, txtProfiency.Text };
        Helper.Insert("tbl_ApplicantLanguage", values, txt, col);
    }

    protected void btnorg_Click(object sender, EventArgs e)
    {
        string[] values = new string[] { "@ApplicantID", "@OrganizationName", "@PositionHeld", "@Assocation", "@StartMonth", "@StartYear", "@EndMonth", "@EndYear", "@Description", "@IsOnGoing" };
        string[] col = new string[] { "@ApplicantID", "OrganizationName", "PositionHeld", "Assocation", "StartMonth", "StartYear", "EndMonth", "EndYear", "Description", "IsOnGoing" };
        string[] txt = new string[] { Session["ApplicantID"].ToString(), txtOrganization.Text, txtPosition.Text, txtAssociation.Text, DropDownList1.SelectedValue, TextBox1.Text, DropDownList2.SelectedValue, TextBox2.Text, txtDescription.Text, txtOngoing.Text };
        Helper.Insert("tbl_Organization", values, txt, col);
    }

    protected void btnpat_Click(object sender, EventArgs e)
    {
        string[] values = new string[] { "@PatentTitle", "@ApplicationNumber", "@IssueMonth", "@IssueDay", "@IssueYear", "@FileMonth", "@FileDay", "@FileYear", "@PatentUrl", "@Status" };
        string[] col = new string[] { "PatentTitle", "ApplicationNumber", "IssueMonth", "IssueDay", "IssueYear", "FileMonth", "FileDay", "FileYear", "PatentUrl", "Status" };
        string[] txt = new string[] { txtTitle.Text, txtAppNum.Text, txtIMonth.Text, txtIDay.Text, txtIYear.Text, txtFMonth.Text, txtFDay.Text, txtFYear.Text, TextBox3.Text, txtStatus.Text };
        Helper.Insert("tbl_Patent", values, txt, col);
    }

    protected void btnproj_Click(object sender, EventArgs e)
    {
        string[] values = new string[] { "@ApplicantId", "@ProjectName", "@StartMonth", "@StartYear", "@EndMonth", "@EndYear", "@IsOnGoing", "@ProjectURL" };
        string[] col = new string[] { "ApplicantId", "ProjectName", "StartMonth", "StartYear", "EndMonth", "EndYear", "IsOnGoing", "ProjectURL" };
        string[] txt = new string[] { Session["ApplicantID"].ToString(), txtProject.Text, DropDownList3.SelectedValue, TextBox4.Text, DropDownList4.SelectedValue, TextBox5.Text, TextBox6.Text, TextBox7.Text };
        Helper.Insert("tbl_Project", values, txt, col);
    }

    protected void btnskill_Click(object sender, EventArgs e)
    {
        string session = Session["ApplicantID"].ToString();
        string[] tablecols = new string[] { "ApplicantID", "ApplicantSkillsID" };
        string[] values = new string[] { "@ApplicantID", "@ApplicantSkillsID" };
        string[] textbox = new string[] { session, ddlSkills.SelectedValue };
        Helper.Insert("tbl_ApplicantSkills2", values, textbox, tablecols);
    }

    //TWITTER ANALYSISW
    public static string getTwitterURL()
    {
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            string SQL = @"Select twitter from tbl_Applicant Where ApplicantID = @ApplicantID";
            using (SqlCommand cmd = new SqlCommand(SQL, con))
            {
                cmd.Parameters.AddWithValue("@ApplicantID", HttpContext.Current.Session["ApplicantID"].ToString());
                string result = "";
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        result = dr["twitter"].ToString();
                    }
                }
                return result;
            }
        }
    }


}
