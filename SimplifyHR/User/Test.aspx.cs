﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    string[] answers = new string[] {"vast melody", "fjord", "reef", "talent", "27", "48", "c", "trees", "51", "long, pithy", "never look a gift horse in the mouth", "a", "42", "d",
    "polecat", "grill", "feeler", "disparage, praise", "c"};


    int counter = 0;

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        string[] textbox = new string[] { txt1.Text, txt2.Text, txt3.Text, txt4.Text, txt5.Text, txt6.Text, txt7.Text, txt8.Text,
        txt9.Text, txt10.Text, txt11.Text, txt12.Text, txt13.Text, txt14.Text, txt15.Text, txt16.Text, txt17.Text, txt18.Text, txt19.Text, txt20.Text};

        for (int x = 0; x < 19; x++)
        {
            if (textbox[x].ToLower() == answers[x])
            {
                counter++;
            }
        }

        string sql = @"UPDATE tbl_Applicant
SET Result = @Result
WHERE ApplicantID = @ApplicantID;";
        string[] cols = new string[] { "@Result", "@ApplicantID" };
        string[] value = new string[] { counter.ToString(), Session["ApplicantID"].ToString() };
        Helper.UpdateDelete(sql, cols, value);
        Response.Redirect("UserProfile.aspx");
    }
}