﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Project_Add : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        string[] values = new string[] { "@ApplicantId", "@ProjectName", "@StartMonth", "@StartYear", "@EndMonth", "@EndYear", "@IsOnGoing", "@ProjectURL" };
        string[] col = new string[] { "ApplicantId", "ProjectName", "StartMonth", "StartYear", "EndMonth", "EndYear", "IsOnGoing", "ProjectURL" };
        string[] txt = new string[] { Session["ApplicantID"].ToString(), txtProject.Text, ddlStartMonth.SelectedValue, txtStartYear.Text, ddlEndMonth.SelectedValue, txtEndYear.Text, txtExpires.Text, txtURL.Text };
        Helper.Insert("tbl_Project", values, txt, col);
    }
}