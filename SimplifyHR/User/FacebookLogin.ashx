﻿<%@ WebHandler Language="C#" Class="FacebookLogin" %>

using System;
using System.Web;

public class FacebookLogin : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        //context.Response.ContentType = "text/plain";
        //context.Response.Write("Hello World");
        var accessToken = context.Request["accessToken"];
        context.Session["AccessToken"] = accessToken;
        context.Response.Redirect("Home.aspx");

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}