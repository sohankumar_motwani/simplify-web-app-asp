﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="User_Interest_Add" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Interest</title>
    <link href="../../Content/font-awesome.min.css" rel="stylesheet" />
    <link href="../../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../../Content/materialize.min.css" rel="stylesheet" />
</head>
<body>
    <div class="col-lg-offset-3 col-lg-6">
        <div class="card">
            <div class="card-title">
                <br />
                <h4 class="center">Add Interest</h4>
            </div>
            <div class="card-content">


                <form id="RegisterForm" runat="server">
                    <div class="form-group">
                        <div class="">
                            <label class="control-label">Interest:</label>
                        </div>
                        <asp:TextBox ID="txtInterest" runat="server" required></asp:TextBox>
                    </div>
                    <div class="card-action">
                        <div class="form-group">
                            <div class="">
                                <asp:Button ID="btnRegister" CssClass="btn btn-success pull-right btn-lg" Text="Save" runat="server" OnClick="btnAdd_Click" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
