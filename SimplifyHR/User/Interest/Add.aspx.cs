﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Interest_Add : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        String[] parameters = new string[1] { "@interestName" };
        String[] content = new string[1] { txtInterest.Text };
        int queryScope = Helper.InsertScope("tbl_Interest", parameters, content);
        if (queryScope != 0)
        {
            String[] secondParameters = new string[2] { "@Applicant", "@Interest" };
            //replace the '19' with logged in user's session (ApplicantID)
            String[] secondContent = new string[2] { "19", Convert.ToString(queryScope) };
            Helper.Insert("tbl_ApplicantInterest", secondParameters, secondContent);
        }
        else
        {
            //throw error
        }

    }
}