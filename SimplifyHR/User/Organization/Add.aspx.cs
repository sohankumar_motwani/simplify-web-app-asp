﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Organization_Add : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        string[] values = new string[] { "@ApplicantID", "@OrganizationName", "@PositionHeld", "@Assocation", "@StartMonth", "@StartYear", "@EndMonth", "@EndYear", "@Description", "@IsOnGoing" };
        string[] col = new string[] { "@ApplicantID", "OrganizationName", "PositionHeld", "Assocation", "StartMonth", "StartYear", "EndMonth", "EndYear", "Description", "IsOnGoing" };
        string[] txt = new string[] { Session["ApplicantID"].ToString(), txtOrganization.Text, txtPosition.Text, txtAssociation.Text, ddlStartMonth.SelectedValue, txtStartYear.Text, ddlEndMonth.SelectedValue, txtEndYear.Text, txtDescription.Text, txtOngoing.Text };
        Helper.Insert("tbl_Organization", values, txt, col);
    }
}