﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Organization_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        getData();
    }
    void getData()
    {
        string session = Session["ApplicantID"].ToString();
        string[] filter = new string[] { "@UserID" };
        string[] filvalue = new string[] { session };
        string SQL = @"SELECT        dbo.tbl_Organization.OrganizationName, dbo.tbl_Organization.Association, dbo.tbl_Organization.PositionHeld, dbo.tbl_Organization.StartMonth, dbo.tbl_Organization.StartYear, dbo.tbl_Organization.EndMonth, 
                         dbo.tbl_Organization.Description, dbo.tbl_Organization.IsOngoing, dbo.tbl_Organization.EndYear, dbo.tbl_Organization.ApplicantID, dbo.tbl_Applicant.ApplicantID AS Expr1
FROM            dbo.tbl_Organization INNER JOIN
                         dbo.tbl_Applicant ON dbo.tbl_Organization.ApplicantID = dbo.tbl_Applicant.ApplicantID
                           WHERE tbl_Applicant.ApplicantID = @UserID";
        Helper.Select(SQL, lvSkills, filter, filvalue);
    }
}