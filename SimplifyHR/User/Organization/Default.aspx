﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="User_Organization_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div class="col-lg-12">
        <form id="form1" runat="server" class="form-horizontal">
            <table class="table table-hover">
                <thead>
                    <th>Organization Name:</th>
                    <th>Position Held:</th>
                    <th>Association:</th>
                    <th>Start Month:</th>
                    <th>End Month:</th>
                    <th>Start Year:</th>
                    <th>End Year:</th>
                    <th>Description:</th>
                    <th>On Going:</th>



                    <th></th>
                </thead>
                <tbody>
                    <asp:ListView ID="lvSkills" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("OrganizationName") %></td>

                                <td><%# Eval("PositionHeld") %></td>
                                <td><%# Eval("Association") %></td>

                                <td><%# Eval("StartMonth") %></td>

                                <td><%# Eval("EndMonth") %></td>

                                <td><%# Eval("StartYear") %></td>

                                <td><%# Eval("EndYear") %></td>
                                <td><%# Eval("Description") %></td>

                                <td><%# Eval("IsOnGoing") %></td>


                                <td>
                                    <a href='Details.aspx?ID=<%# Eval("OrganizationID") %>' class="btn btn-xs btn-info">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href='Delete.aspx?ID=<%# Eval("OrganizationID") %>' class="btn btn-xs btn-info">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <tr colspan="16">
                                <td>
                                    <h2>No Records Available!</h2>
                                </td>
                            </tr>
                        </EmptyDataTemplate>
                    </asp:ListView>
                </tbody>
            </table>
        </form>
    </div>
</body>
</html>
