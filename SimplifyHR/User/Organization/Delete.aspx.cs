﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Organization_Delete : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ApplicantID"] == null)
        {
            Response.Redirect("~/index.aspx");
        }
        else
        {
            if (Request.QueryString["ID"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                int NewsID = 0;
                bool validNews = int.TryParse(Request.QueryString["ID"], out NewsID);
                if (validNews)
                {
                    if (!IsPostBack)
                    {
                        deleteRecord(NewsID);
                    }
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
        }

    }
    void deleteRecord(int ID)
    {
        string sql = @"DELETE FROM tbl_Organization WHERE OrganizationID = @OrganizationID";
        Helper.Delete(sql, "@OrganizationID", ID);


    }
}