﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Settings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ApplicantID"] == null & Session["AccessToken"] == null)
        {
            Response.Redirect("~/index.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                checkedu();
                getData();
                educlevel();
                degree();
                string SQL = @"SELECT CountryID, Country FROM tbl_Country Where Country = 'Philippines'";
                Helper.Select(SQL, ddlcountry, "Country", "CountryID");

                string SQL1 = @"SELECT RegionID, Region FROM tbl_Region Where CountryCode = '173'";
                Helper.Select(SQL1, ddlregion, "Region", "RegionID");

                string SQL2 = @"SELECT CityID, City FROM tbl_City Inner Join tbl_Region On tbl_City.RegionID = tbl_Region.RegionID inner join tbl_Country on  tbl_region.CountryCode = tbl_Country.CountryID Where tbl_Region.CountryCode = '173' ";
                Helper.Select(SQL2, ddlCity, "City", "CityID");
            }
        }
    }


    void checkedu()
    {
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            string SQL = @"Select Count(ApplicantID) AS AID from tbl_ApplicantEducLvl Where ApplicantID = @ApplicantID";
            using (SqlCommand cmd = new SqlCommand(SQL, con))
            {
                cmd.Parameters.AddWithValue("@ApplicantID", Session["ApplicantID"].ToString());
                string check1 = cmd.ExecuteScalar().ToString();
                if (Convert.ToInt32(check1) >= 1)
                {
                    txtpri.Enabled = false;
                    txtsec.Enabled = false;
                    txtcol.Enabled = false;
                    txtcourse.Enabled = false;
                    ddldegree.Enabled = false;
                    ddleduclvl.Enabled = false;
                }
                else
                {
                    txtpri.Enabled = true;
                    txtsec.Enabled = true;
                    txtcol.Enabled = true;
                    txtcourse.Enabled = true;
                    ddldegree.Enabled = true;
                    ddleduclvl.Enabled = true;
                }
            }
        }
    }

    void educlevel()
    {
        String SQL = @"Select EducationLvlId,EducationLvlName from tbl_EducationLvl ";
        Helper.Select(SQL, ddleduclvl, "EducationLvlName", "EducationLvlId");
    }

    void degree()
    {
        String SQL = @"Select DegreeId,DegreeType from tbl_Degree ";
        Helper.Select(SQL, ddldegree, "DegreeType", "DegreeId");

    }



    protected void btnSM_Click(object sender, EventArgs e)
    {
        String SQL = @"Update tbl_Applicant SET Facebook = @Facebook,Twitter = @Twitter,Instagram = @Instagram where ApplicantID = @ApplicantID";
        string[] col = { "@Facebook", "@Twitter", "@Instagram", "@ApplicantID" };
        string[] textbox = { Session["AccessToken"].ToString(), txtTwiT.Text, txtInsT.Text, Session["ApplicantID"].ToString() };
        Helper.UpdateDelete(SQL, col, textbox);
        Response.Redirect("Settings.aspx");
    }

    protected void btnPer_Click(object sender, EventArgs e)
    {

        String SQL = @"Update tbl_Applicant SET StreetAddress = @StreetAddress, ZipCode = @ZipCode, Mobile = @Mobile, Nationality = @Nationality, MaritalStatus = @MaritalStatus, CityID = @CityID, Telephone = @Telephone, Website = @Website, CallTime = @CallTime, CallMedium = @CallMedium, Summary = @Summary where ApplicantID=@ApplicantID";
        string[] col = { "@MaritalStatus", "@StreetAddress", "@CityID", "@ZipCode", "@Telephone", "@Mobile", "@Nationality", "@Website", "@CallTime", "@CallMedium", "@Summary", "@ApplicantID" };
        string[] textbox = { ddlMarS.SelectedValue, txtStrA.Text, ddlCity.SelectedValue, txtZipC.Text, txtTel.Text, txtMob.Text, txtNat.Text, txtWeb.Text, txtCalT.Text, txtCalM.Text, txtSum.Text, Session["ApplicantID"].ToString() };
        Helper.UpdateDelete(SQL, col, textbox);
        Response.Redirect("Settings.aspx");
    }

    protected void btnGen_Click(object sender, EventArgs e)
    {
        String SQL = @"Update tbl_Applicant SET Email = @Email, Password = @Password where ApplicantID=@ApplicantID";
        string[] col = { "@Email", "@Password", "@ApplicantID" };
        string[] textbox = { txtemail.Text, Helper.CreateSHAHash(txtchngePass.Text), Session["ApplicantID"].ToString() };
        Helper.UpdateDelete(SQL, col, textbox);
        Response.Redirect("Settings.aspx");
        notification.Text = "Email & Password Updated";
    }
    protected void btnprofilepic_Click(object sender, EventArgs e)
    {
        string fileName1 = Path.GetFileName(ProfilePicture.PostedFile.FileName);
        ProfilePicture.PostedFile.SaveAs(Server.MapPath("/images/") + fileName1);
        String SQL = @"Update tbl_Applicant SET  ProfilePicture = @ProfilePicture where ApplicantID=@ApplicantID";
        string[] col = { "@ProfilePicture", "@ApplicantID" };
        string[] textbox = { fileName1, Session["ApplicantID"].ToString() };
        Helper.UpdateDelete(SQL, col, textbox);
        Response.Redirect("Settings.aspx");
    }


    void getData()
    {
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            string SQL = @"Select AP.ApplicantID,AP.ProfilePicture,AP.LastName,AP.FirstName,AP.CityID,AP.Website,C.City,R.Region, AP.Email, AP.Telephone, AP.Mobile, AP.MaritalStatus, AP.ZipCode, AP.CallTime, ap.CallMedium, AP.Summary, AP.Twitter, AP.Facebook, AP.Instagram, AP.MiddleName, AP.Nationality, AP.StreetAddress 
            from tbl_Applicant AS AP
            Left JOIN tbl_City AS C
            ON ap.CityID = C.CityID 
            Left JOIN tbl_Region as R 
            ON C.RegionID = R.RegionID 
            Left JOIN tbl_Country as CO 
            ON R.CountryCode = CO.CountryID 
            Where ap.ApplicantID = @ApplicantID";
            using (SqlCommand cmd = new SqlCommand(SQL, con))
            {
                cmd.Parameters.AddWithValue("@ApplicantID", Session["ApplicantID"].ToString());
                System.Diagnostics.Debug.WriteLine(Session["ApplicantID"].ToString());

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        //ProfilePicture.Value = "/images/" + dr["ProfilePicture"].ToString();
                        ddlMarS.SelectedValue = dr["MaritalStatus"].ToString();
                        txtemail.Text = dr["Email"].ToString();
                        txtWeb.Text = dr["Website"].ToString();
                        txtCalM.Text = dr["CallMedium"].ToString();
                        txtCalT.Text = dr["CallTime"].ToString();
                        txtInsT.Text = dr["Instagram"].ToString();
                        txtMob.Text = dr["Mobile"].ToString();
                        txtNat.Text = dr["Nationality"].ToString();
                        txtStrA.Text = dr["StreetAddress"].ToString();
                        txtSum.Text = dr["Summary"].ToString();
                        txtTel.Text = dr["Telephone"].ToString();
                        txtTwiT.Text = dr["Twitter"].ToString();
                        txtZipC.Text = dr["ZipCode"].ToString();

                    }
                }
            }
        }
    }

    protected void ddlcountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            String SQL = @"select RegionID, Region from tbl_Region where CountryCode=@CountryCode";
            using (SqlCommand cmd = new SqlCommand(SQL, con))
            {
                cmd.Parameters.AddWithValue("@CountryCode", ddlcountry.SelectedItem.Value);
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    ddlregion.DataTextField = "Region";
                    ddlregion.DataValueField = "RegionID";
                    ddlregion.DataBind();
                    if (ddlcountry.Items.Count > 1)
                    {
                        ddlregion.Enabled = true;
                    }
                    else
                    {
                        ddlregion.Enabled = false;
                        ddlCity.Enabled = false;
                    }
                }
            }
        }
    }
    protected void ddlregion_SelectedIndexChanged(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            String SQL = @"select CityID, City from tbl_City where RegionID=@RegionID";
            using (SqlCommand cmd = new SqlCommand(SQL, con))
            {
                cmd.Parameters.AddWithValue("@RegionID", ddlregion.SelectedItem.Value);
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    ddlregion.DataTextField = "City";
                    ddlregion.DataValueField = "CityID";
                    ddlregion.DataBind();
                    if (ddlregion.Items.Count > 1)
                    {
                        ddlCity.Enabled = true;
                    }
                    else
                    {
                        ddlCity.Enabled = false;
                    }
                }
            }
        }
    }

    protected void btnEduc_Click(object sender, EventArgs e)
    {
        string[] cols1 = new string[] { "@SchoolName" };
        string[] txt1 = new string[] { txtpri.Text };
        string[] tbcols1 = new string[] { "SchoolName" };
        string schoolid1 = Helper.InsertScope("tbl_School", cols1, txt1, tbcols1);

        string[] cols2 = new string[] { "@SchoolName" };
        string[] txt2 = new string[] { txtsec.Text };
        string[] tbcols2 = new string[] { "SchoolName" };
        string schoolid2 = Helper.InsertScope("tbl_School", cols2, txt2, tbcols2);

        string[] cols3 = new string[] { "@SchoolName" };
        string[] txt3 = new string[] { txtcol.Text };
        string[] tbcols3 = new string[] { "SchoolName" };
        string schoolid3 = Helper.InsertScope("tbl_School", cols3, txt3, tbcols3);

        //TBL_Course
        string[] cols5 = new string[] { "@CourseName", "@DegreeId" };
        string[] txt5 = new string[] { txtcourse.Text, ddldegree.SelectedValue };
        string[] tbcols5 = new string[] { "CourseName", "DegreeId" };
        string course = Helper.InsertScope("tbl_Course", cols5, txt5, tbcols5);


        //TBL_EducLvl
        string[] cols6 = new string[] { "@ApplicantID", "@EducLvID", "@SchoolID" };
        string[] txt6 = new string[] { Session["ApplicantID"].ToString(), "1", schoolid1 };
        string[] tbcols6 = new string[] { "ApplicantID", "EducLvID", "SchoolID" };
        Helper.InsertScope("tbl_ApplicantEducLvl", cols6, txt6, tbcols6);

        string[] cols7 = new string[] { "@ApplicantID", "@EducLvID", "@SchoolID" };
        string[] txt7 = new string[] { Session["ApplicantID"].ToString(), "2", schoolid2 };
        string[] tbcols7 = new string[] { "ApplicantID", "EducLvID", "SchoolID" };
        Helper.InsertScope("tbl_ApplicantEducLvl", cols7, txt7, tbcols7);

        string[] cols8 = new string[] { "@ApplicantID", "@EducLvID", "@SchoolID", "@CourseID" };
        string[] txt8 = new string[] { Session["ApplicantID"].ToString(), "3", schoolid3, course };
        string[] tbcols8 = new string[] { "ApplicantID", "EducLvID", "SchoolID", "CourseID" };
        Helper.InsertScope("tbl_ApplicantEducLvl", cols8, txt8, tbcols8);
    }
}