﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="User_Test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../Content/font-awesome.min.css" rel="stylesheet" />
    <link href="../../Content/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <div class="col-lg-offset-3 col-lg-6">

        <form id="RegisterForm" runat="server">
            <div class="form-group">
                <div class="col-lg-8 pull-right">
                    <label class=" ">
                        Create two words using the following ten letters each
once only.
Clue: grand tune (4, 6)
MYSEVODLTA</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt1" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-8 pull-right">
                    <label class=" ">
                        Which is the odd one out?
ISTHMUS, FJORD, ATOLL, POLDER, ARCHIPELAGO</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt2" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>


            </div>
            <div class="form-group">
                 <div class="col-lg-8 pull-right">
                    <label class=" ">
                        CARTON, ENJOYMENT, WORDSMITH
Which of the following words continues the above
sequence?
COPY, REEF, COPE, REST, ACHE</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt3" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
               

            </div>


            <div class="form-group">
                 <div class="col-lg-8 pull-right">
                    <label class=" ">
                        What word in brackets means the same as the word in
capitals?
FORTE (endowment, conduct, talent, redoubt, style)</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt4" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
               

            </div>

            <div class="form-group">
                 <div class="col-lg-8 pull-right">
                    <label class=" ">
                        What number comes next in this sequence?
25, 32, 27, 36, ?</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt5" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
               

            </div>

            <div class="form-group">
                 <div class="col-lg-8 pull-right">
                    <label class=" ">
                        A car travels at a speed of 40 mph over a certain distance
and then returns over the same distance at a speed of
60 mph. What is the average speed for the total journey?</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt6" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
               

            </div>

            <div class="form-group">
                <div class="col-lg-8 pull-right">
                    <label class=" ">
                        Choose the letter only.
                        MEANDER: WIND
TRAVERSE:
a) stampede
b) forward
c) across
d) retrace
e) towards</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt7" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
                

            </div>

            <div class="form-group">
                 <div class="col-lg-8 pull-right">
                    <label class=" ">
                        What do the following words have in common?
LEGUMES, QUASHED, AFFIRMS, CLOAKED</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt8" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
               

            </div>

            <div class="form-group">
                  <div class="col-lg-8 pull-right">
                    <label class=" ">
                        What number should replace the question mark?
926 : 24
799 : 72
956 : ?</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt9" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
              

            </div>

            <div class="form-group">
                 <div class="col-lg-8 pull-right">
                    <label class=" ">
                        Add one letter, not necessarily the same letter, to each
word at the front, end or middle to find two words that
are opposite in meaning.
LOG PITY</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt10" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
               

            </div>

            <div class="form-group">
                 <div class="col-lg-8 pull-right">
                    <label class=" ">
                        What well-known proverb is opposite in meaning to the
one below?
Beware of Greeks bearing gifts.</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt11" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
               

            </div>

            <div class="form-group">
                <div class="col-lg-8 pull-right">
                    <label class=" ">
                        Which word means the same as ANCHORITE?
a) recluse
b) hieroglyphics
c) trammel
d) lackey</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt12" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
                

            </div>



            <div class="form-group">
                 <div class="col-lg-8 pull-right">
                    <label class=" ">
                        What is the value of x?
64 – 12 × 2 + 6 ÷ 3 = x</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt13" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
               

            </div>

            <div class="form-group">
                 <div class="col-lg-8 pull-right">
                    <label class=" ">
                        What is JULIENNE?
a) reed bunting
b) an evergreen shrub
c) a sleeveless jacket
d) clear soup
e) a skull-cap</label>
                </div>

                <div class="col-lg-4">
                    <asp:TextBox ID="txt14" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
               
            </div>

            <div class="form-group">
                 <div class="col-lg-8 pull-right">
                    <label class=" ">
                        Solve the one-word anagram.
APE COLT</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt15" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
               

            </div>

            <div class="form-group">
                 <div class="col-lg-8 pull-right">
                    <label class=" ">
                        Insert a word that means the same as the words outside
the brackets.
COOK (. . . . . ) CROSS EXAMINE</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt16" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
               

            </div>

            <div class="form-group">
                 <div class="col-lg-8 pull-right">
                    <label class=" ">
                        Make a six-letter word using only these four letters:
R E
F L</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt17" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
               

            </div>

            <div class="form-group">
                 <div class="col-lg-8 pull-right">
                    <label class=" ">
                        Which two words mean the opposite?
DOLOROUS, CONVENE, DISPARAGE, OFFEND, PRAISE,
TREMBLE</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt18" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
               

            </div>

            <div class="form-group">
                 <div class="col-lg-8 pull-right ">
                    <label class=" ">
                        What is the name given to a group of ROOKS?
a) murmuration
b) park
c) building
d) set
e) business</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt19" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
               

            </div>

            <div class="form-group">
                  <div class="col-lg-8 pull-right pull-right">
                    <label class=" ">
                        What is always associated with a BINNACLE?
a) a jib
b) a life raft
c) a limpet
d) a mast
e) a compass</label>
                </div>
                <div class="col-lg-4">
                    <asp:TextBox ID="txt20" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                </div>
            </div>

            <div class="col-lg-8 pull-right">
                <p>
                    Result: 
            <asp:Literal ID="ltScore" runat="server"></asp:Literal>
                </p>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <asp:Button ID="btnRegister" CssClass="btn-success pull-right btn-lg" Text="Save" runat="server" OnClick="btnRegister_Click" />
                </div>
            </div>
        </form>
    </div>
</body>
</html>
