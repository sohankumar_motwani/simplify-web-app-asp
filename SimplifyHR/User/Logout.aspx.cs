﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Facebook;

public partial class User_Logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ApplicantID"] != null & Session["AccessToken"] == null)
        {
            Session.RemoveAll();
            Response.Redirect("~/index.aspx");
        }
        else if (Session["AccessToken"] != null & Session["ApplicantID"] == null)
        {
            var oauth = new FacebookClient();

            var logoutParameters = new Dictionary<string, object>
                  {
                     {"access_token", Session["AccessToken".ToString()] },
                      { "next", "http://simplifyhr.azurewebsites.net"}
                  };
            //value of next above could be changed to C# function returning domain only.
            var logoutUrl = oauth.GetLogoutUrl(logoutParameters);
            Session.RemoveAll();
            Response.Redirect(logoutUrl.ToString());
        }
        else if (Session["AccessToken"] != null & Session["ApplicantID"] != null)
        {
            var oauth = new FacebookClient();

            var logoutParameters = new Dictionary<string, object>
                  {
                     {"access_token", Session["AccessToken".ToString()] },
                      { "next", "http://simplifyhr.azurewebsites.net"}
                  };
            //value of next above could be changed to C# function returning domain only.
            var logoutUrl = oauth.GetLogoutUrl(logoutParameters);
            Session.RemoveAll();
            Response.Redirect(logoutUrl.ToString());
        }
        else
        {
            Response.Redirect("~/index.aspx");
        }
    }

}