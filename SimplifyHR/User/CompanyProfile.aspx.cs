﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Company_CompanyProfile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["ID"] == null)
        {
            Response.Redirect("Home.aspx");
        }
        else
        {
            int authorID = 0;
            bool validAuthor = int.TryParse(Request.QueryString["ID"].ToString(),
                out authorID);
            if (validAuthor)
            {
                if (!IsPostBack)
                {
                    showuser(authorID);
                }
            }
            else
            {
                Response.Redirect("Home.aspx");
            }
        }
    }

    void showuser(int ID)
    {
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            string SQL = @"Select ap.CompanyName,ap.StreetAddress,ap.Telephone,ap.ProfilePicture,b.Branch,c.City,c.RegionID  
                        from tbl_Company AS ap 
                        Left JOIN tbl_City AS c
                        ON ap.CityID = C.CityID 
                        Left Join tbl_CompanyBranch as cb
                        ON ap.CompanyID = cb.CompanyID
                        Left Join tbl_Branch as b
                        ON cb.BranchID = b.BranchId
                        Where ap.CompanyID = @CompanyID";
            using (SqlCommand cmd = new SqlCommand(SQL, con))
            {
                cmd.Parameters.AddWithValue("@CompanyID", ID);

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        profpic.ImageUrl = "/CompanyProfpic/" + dr["ProfilePicture"].ToString(); ;
                        user.Text = dr["CompanyName"].ToString();
                        StreetAddress.Text = dr["StreetAddress"].ToString();
                        city.Text = " " + dr["City"].ToString() + ", " + dr["RegionID"].ToString();
                        Branch.Text = " " + dr["Branch"].ToString();
                        Telephone.Text = " " + dr["Telephone"].ToString();
                    }
                }
            }
        }
    }
}