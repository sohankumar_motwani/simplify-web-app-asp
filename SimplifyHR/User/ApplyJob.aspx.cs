﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_ApplyJob : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["ID"] == null)
        {
            Response.Redirect("Home.aspx");
        }
        else
        {
            int authorID = 0;
            bool validAuthor = int.TryParse(Request.QueryString["ID"].ToString(),
                out authorID);
            if (validAuthor)
            {
                if (!IsPostBack)
                {
                    using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
                    {
                        con.Open();
                        string sql = @"select Count(ApplicantID) as ApplicantID, Count(JobPostingID) as JobpostingID from tbl_ApplicantJob where ApplicantID = @ApplicantID AND JobPostingID = @JobPostingID";
                        using (SqlCommand cmd = new SqlCommand(sql, con))
                        {
                            cmd.Parameters.AddWithValue("@ApplicantID", Session["ApplicantID"].ToString());
                            cmd.Parameters.AddWithValue("@JobPostingID", Request.QueryString["ID"].ToString());
                            string iemail = cmd.ExecuteScalar().ToString();
                            if (iemail == "0")
                            {
                                InsertJob(authorID);
                            }
                            else if (iemail == "1")
                            {
                                Response.Write("<script>alert('you already applied for this job'); window.location.href = 'Home.aspx';</script>");
                            }
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("Home.aspx");
            }
        }

    }
    void InsertJob(int ID)
    {
        string[] values = new string[] { "@ApplicantID", "@JobPostingID", "@Status" };
        string[] col = new string[] { "ApplicantID", "JobPostingID", "Status" };
        string[] txt = new string[] { Session["ApplicantID"].ToString(), ID.ToString(), "Applied" };
        Helper.Insert("tbl_ApplicantJob", values, txt, col);
        Response.Write("<script>alert('you have successfully applied for a job. please wait for the company to contact you thank you'); window.location.href = 'Home.aspx';</script>");

    }
}