﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserMaster.master" AutoEventWireup="true" CodeFile="CompanyProfile.aspx.cs" Inherits="Company_CompanyProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" runat="Server">

    <div class="col-lg-10 col-lg-offset-1" style="position:absolute; top:250px; left:50px;">

        <%-- User Frame --%>
        <div class="user-menu-container">
            <div class="user-details white card">
                <div class="row ">
                    <div class="col-md-8 no-pad">
                        <div class="user-pad">
                            <h3 style="color: black;">
                                <asp:Literal runat="server" ID="user"></asp:Literal></h3>
                            <hr />
                            <h4><i class="fa fa-location-arrow"></i>
                                <asp:Literal runat="server" ID="StreetAddress"></asp:Literal>
                                <asp:Literal runat="server" ID="city"></asp:Literal>
                                <asp:Literal runat="server" ID="region"></asp:Literal></h4>
                            <h4><a>
                                <asp:Literal runat="server" ID="Branch"></asp:Literal></a></h4>
                            <h4><i class="fa fa-phone"></i>
                                <asp:Literal runat="server" ID="Telephone"></asp:Literal>
                            </h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class=" img-responsive" style="">
                            <asp:Image runat="server" ID="profpic" alt="Profile picture" Height="300px" Width="300px" class="img-responsive" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

