﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;

public partial class User_Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ApplicantID"] == null & Session["AccessToken"] == null & Session["AccessToken"] == null)
        {
            Response.Redirect("../index.aspx");
        }
        else if (Session["AccessToken"] != null & Session["ApplicantID"] == null)
        {
            Helper.createFacebookBased(Session["AccessToken"].ToString());
            String path = "../images/defaultpic.jpg";
            bool result = Helper.facebookApplicant(Helper.email, "12345678", Helper.firstName, Helper.lastName, path);
            string SQL = @"SELECT jp.JobPostingID,jp.JobTitle, jp.JobDescription, jp.WorkExperience, jp.EducationalAttainment, jp.PriorTraining, jp.Benefit, jp.Responsibilities, 
                                  jp.PayRange, co.CompanyID, ci.City,co.ProfilePicture, S.Skill FROM tbl_JobPosting jp 
                                  INNER JOIN tbl_City ci ON ci.CityID = jp.LocationID 
                                  INNER JOIN tbl_Company co ON co.CompanyID = jp.CompanyID
                                  INNER JOIN tbl_JobSkills JS ON jp.JobPostingID = JS.JobPostingID
                                  INNER JOIN tbl_Skills S ON JS.SkillID = S.SkillID";
            Helper.Select(SQL, lvJobPosting);

            if (result)
            {
                //Response.Redirect("~/User/Home.aspx");
                Response.Write("<script>alert('Local Account created! You may now login into your local account using your Facebook-registered e-mail, with a default password of 12345678');</script>");
            }
            else
            {
                //Response.Redirect("~/User/Home.aspx");
                Response.Write("<script>alert('" + Helper.insertMessage + "')</script>");
                checkFacebookToken();
                //Response.Write("Session:" + Session["ApplicantID"].ToString());
            }

        }
        else if (Session["AccessToken"] == null & Session["ApplicantID"] != null)
        {
            string SQL = @"SELECT jp.JobPostingID,jp.JobTitle, jp.JobDescription, jp.WorkExperience, jp.EducationalAttainment, jp.PriorTraining, jp.Benefit, jp.Responsibilities, 
                                  jp.PayRange, co.CompanyID, ci.City,co.ProfilePicture, S.Skill FROM tbl_JobPosting jp 
                                  INNER JOIN tbl_City ci ON ci.CityID = jp.LocationID 
                                  INNER JOIN tbl_Company co ON co.CompanyID = jp.CompanyID
                                  INNER JOIN tbl_JobSkills JS ON jp.JobPostingID = JS.JobPostingID
                                  INNER JOIN tbl_Skills S ON JS.SkillID = S.SkillID";
            Helper.Select(SQL, lvJobPosting);

        }
        else
        {
            string SQL = @"SELECT jp.JobPostingID,jp.JobTitle, jp.JobDescription, jp.WorkExperience, jp.EducationalAttainment, jp.PriorTraining, jp.Benefit, jp.Responsibilities, 
                                  jp.PayRange, co.CompanyID, ci.City,co.ProfilePicture, S.Skill FROM tbl_JobPosting jp 
                                  INNER JOIN tbl_City ci ON ci.CityID = jp.LocationID 
                                  INNER JOIN tbl_Company co ON co.CompanyID = jp.CompanyID
                                  INNER JOIN tbl_JobSkills JS ON jp.JobPostingID = JS.JobPostingID
                                  INNER JOIN tbl_Skills S ON JS.SkillID = S.SkillID";
            Helper.Select(SQL, lvJobPosting);
        }
    }

    void checkFacebookToken()
    {
        using (SqlConnection con = new SqlConnection(Helper.GetConnection()))
        {
            con.Open();
            string sql = @"SELECT ApplicantID FROM tbl_Applicant WHERE Email = @AccessToken";
            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
                cmd.Parameters.AddWithValue("@AccessToken", Helper.email);
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Session["ApplicantID"] = dr["ApplicantID"].ToString();
                    }//closing while
                }//closing reader
            }//closing of Sql Command
        }//closing of SqlConnection
    }


}