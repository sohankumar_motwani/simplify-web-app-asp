﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserMaster.master" AutoEventWireup="true" CodeFile="UserProfile.aspx.cs" Inherits="User_UserProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Profile</title>
    <link href="../css/UserProfile.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="Server">
    <div id="fb-root"></div>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=114395319284537";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <br />
    <br />

    <asp:UpdatePanel runat="server" ID="updatepanel">
        <ContentTemplate>
            <%-- User Interface --%>
            <div class="col-lg-10 col-lg-offset-1">

                <%-- User Frame --%>
                <div class="user-menu-container">
                    <div class="user-details white card">
                        <div class="row ">
                            <div class="col-md-8 no-pad">
                                <div class="user-pad">
                                    <h3 style="color: black;">
                                        <asp:Literal runat="server" ID="user"></asp:Literal></h3>
                                    <h4><i class="fa fa-location-arrow"></i>
                                        <asp:Literal runat="server" ID="city"></asp:Literal>
                                        <asp:Literal runat="server" ID="region"></asp:Literal></h4>
                                    <h4><a>
                                        <asp:Literal runat="server" ID="personalWebsite"></asp:Literal></a></h4>
                                    <asp:Button runat="server" ID="btnupdateprof" OnClick="btnupdateprof_Click" type="button" class="btn btn-labeled btn-info fa fa-pencil" Text="Update" />
                                    <asp:Button runat="server" ID="btnsave" OnClick="btnsave_Click" type="button" class="btn btn-labeled btn-info fa fa-save" Visible="false" Text="Save" />
                                </div>
                            </div>
                            <div class="row">
                                <br />
                                <div class=" img-responsive">
                                    <asp:Image runat="server" ID="profpic" alt="Profile picture" Height="300px" Width="300px" class="img-responsive" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br />
                <div>
                    <asp:Label ID="Genderla" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="Birthdatela" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="MaritalStatusla" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="Mobilela" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="Nationalityla" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="StreetAddressla" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="CallTimela" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="CallMediumla" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="Summaryla" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="Cetificationla" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="Interestla" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="Languagela" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="Organizationla" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="Patentla" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="Projectla" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="Skillsla" runat="server" Visible="false"></asp:Label>
                </div>

                <%-- User stats --%>
                <div class="col-md-12 user-menu user-pad card white">
                    <div class="user-menu-content active">

                        <%-- USER PROFILE --%>
                        <h2>User Profile</h2>
                        <b>Profile Completion:
                                        <div class="progress">
                                            <asp:Panel ID="Panel1" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                        </b>
                        <hr />
                        <asp:ListView runat="server" ID="UserProfile">
                            <ItemTemplate>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h5>
                                            <b>Gender: </b>
                                            <%# Eval("Gender") %>
                                        </h5>
                                        <h5>
                                            <b>Birthdate: </b>
                                            <%# Eval("Birthdate") %>
                                        </h5>
                                        <h5>
                                            <b>MaritalStatus: </b>
                                            <%# Eval("MaritalStatus") %>
                                        </h5>
                                        <h5>
                                            <b>Telephone: </b>
                                            <%# Eval("Telephone") %>
                                        </h5>
                                        <h5>
                                            <b>Mobile: </b>
                                            <%# Eval("Mobile") %>
                                        </h5>

                                    </div>
                                    <div class="col-lg-6">
                                        <h5>
                                            <b>Nationality: </b>
                                            <%# Eval("Nationality") %>
                                        </h5>
                                        <h5>
                                            <b>StreetAddress: </b>
                                            <%# Eval("StreetAddress") %>
                                        </h5>
                                        <h5>
                                            <b>CallTime:</b>
                                            <%# Eval("CallTime") %>
                                        </h5>
                                        <h5>
                                            <b>CallMedium:</b>
                                            <%# Eval("CallMedium") %>
                                        </h5>
                                        <h5>
                                            <b>Summary:</b>
                                            <%# Eval("Summary") %>
                                        </h5>
                                    </div>

                                </div>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                                <div class="col-lg-6">
                                    <p>
                                        <b>MaritalStatus: </b>
                                    </p>
                                    <p>
                                        <b>StreetAddress: </b>

                                    </p>
                                    <p>
                                        <b>Telephone: </b>

                                    </p>
                                    <p>
                                        <b>Mobile: </b>

                                    </p>
                                    <p>
                                        <b>CallTime:</b>

                                    </p>
                                    <p>
                                        <b>CallMedium:</b>

                                    </p>
                                    <p>
                                        <b>Website: </b>

                                    </p>
                                    <p>
                                        <b>Summary:</b>

                                    </p>
                                </div>

                            </EmptyDataTemplate>
                        </asp:ListView>

                        <%-- SOCIAL MEDIA --%>
                        <h3>Social Media Analysis</h3>
                        <hr />
                        <p>
                            <a href="#" data-toggle="modal" data-target="#personality">Personality:</a>
                            <asp:Literal ID="PersoPercent" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="personalityT" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>

                            <a href="#" data-toggle="modal" data-target="#needs">Needs: </a>
                            <asp:Literal ID="needPercent" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="NeedsT" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>

                            <a href="#" data-toggle="modal" data-target="#values">Values:</a>
                            <asp:Literal ID="ValuesPercent" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="ValuesT" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>

                        </p>

                        <%-- ASSETS --%>
                        <h3>Assets</h3>
                        <div>
                            <div class="dropdown">
                                <hr />
                                <button class="btn btn-primary dropdown-toggle" runat="server" visible="false" id="ddl1" type="button" data-toggle="dropdown">
                                    <i class="fa fa-plus">ADD</i>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" data-toggle="modal" data-target="#cert">Certification</a></li>
                                    <li><a href="#int" data-toggle="modal" data-target="#inter">Interest</a></li>
                                    <li><a href="#lang" data-toggle="modal" data-target="#lang">Language</a></li>
                                    <li><a href="#org" data-toggle="modal" data-target="#org">Organization</a></li>
                                    <li><a href="#pat" data-toggle="modal" data-target="#pat">Patent</a></li>
                                    <li><a href="#proj" data-toggle="modal" data-target="#proj">Project</a></li>
                                    <li><a href="#skill" data-toggle="modal" data-target="#skill">Skills</a></li>
                                </ul>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <h5>
                                        <b>Certification:</b>
                                    </h5>
                                    <asp:ListView ID="certificate" runat="server">
                                        <ItemTemplate>
                                            <h5>
                                                <b><%# Eval("Cetification") %></b>
                                            </h5>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                            <h5>No Certificate </h5>
                                        </EmptyDataTemplate>
                                    </asp:ListView>
                                    <br />
                                    <hr />
                                    <h5>
                                        <b>Interest:</b>
                                    </h5>
                                    <asp:ListView ID="Interest" runat="server">
                                        <ItemTemplate>
                                            <h5>
                                                <b><%# Eval("Interest") %></b>
                                            </h5>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                            <h5>No Interest </h5>
                                        </EmptyDataTemplate>
                                    </asp:ListView>
                                    <br />
                                    <hr />
                                    <h5>
                                        <b>Language:</b>
                                    </h5>
                                    <asp:ListView ID="Language" runat="server">
                                        <ItemTemplate>
                                            <h5>
                                                <b><%# Eval("Language") %></b>
                                            </h5>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                            <h5>No Language </h5>
                                        </EmptyDataTemplate>
                                    </asp:ListView>
                                    <br />
                                    <hr />
                                    <h5>
                                        <b>Organization:</b>
                                    </h5>
                                    <asp:ListView ID="Organization" runat="server">
                                        <ItemTemplate>
                                            <h5>
                                                <b><%# Eval("Organization") %></b>
                                            </h5>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                            <h5>No Organization </h5>
                                        </EmptyDataTemplate>
                                    </asp:ListView>
                                    <br />
                                    <hr />

                                </div>
                                <div class="col-lg-6">
                                    <h5>
                                        <b>Patent:</b>
                                    </h5>
                                    <asp:ListView ID="Patent" runat="server">
                                        <ItemTemplate>
                                            <h5>
                                                <b><%# Eval("Patent") %></b>
                                            </h5>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                            <h5>No Patent </h5>
                                        </EmptyDataTemplate>
                                    </asp:ListView>
                                    <br />
                                    <hr />
                                    <h5>
                                        <b>Project:</b>
                                    </h5>
                                    <asp:ListView ID="Project" runat="server">
                                        <ItemTemplate>
                                            <h5>
                                                <b><%# Eval("Project") %></b>
                                            </h5>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                            <h5>No Project </h5>
                                        </EmptyDataTemplate>
                                    </asp:ListView>
                                    <br />
                                    <hr />
                                    <h5>
                                        <b>Skills:</b>
                                    </h5>
                                    <asp:ListView ID="Skills" runat="server">
                                        <ItemTemplate>
                                            <h5>
                                                <b><%# Eval("Skill") %></b>
                                            </h5>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                            <h5>No Skills </h5>
                                        </EmptyDataTemplate>
                                    </asp:ListView>
                                    <br />
                                    <hr />
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="personality" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <b>Openness:</b>
                    <asp:Literal ID="LPersonality1" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Personality1" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Conscientiousness:</b>
                    <asp:Literal ID="LPersonality2" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Personality2" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Extraversion:</b>
                    <asp:Literal ID="LPersonality3" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Personality3" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Agreeableness:</b>
                    <asp:Literal ID="LPersonality4" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Personality4" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>EmotionalRange:</b>
                    <asp:Literal ID="LPersonality5" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Personality5" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <div id="needs" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <b>Challenge:</b>
                    <asp:Literal ID="LNeeds1" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs1" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Closeness:</b>
                    <asp:Literal ID="LNeeds2" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs2" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Curiosity:</b>
                    <asp:Literal ID="LNeeds3" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs3" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Excitement:</b>
                    <asp:Literal ID="LNeeds4" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs4" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Harmony:</b>
                    <asp:Literal ID="LNeeds5" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs5" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Ideal:</b>
                    <asp:Literal ID="LNeeds6" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs6" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Liberty:</b>
                    <asp:Literal ID="LNeeds7" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs7" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Love:</b>
                    <asp:Literal ID="LNeeds8" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs8" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Practicality:</b>
                    <asp:Literal ID="LNeeds9" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs9" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>SelfExpression:</b>
                    <asp:Literal ID="LNeeds10" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs10" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Stability:</b>
                    <asp:Literal ID="LNeeds11" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs11" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Structure:</b>
                    <asp:Literal ID="LNeeds12" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Needs12" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <div id="values" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <b>Conservation:</b>
                    <asp:Literal ID="Lvalues1" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Values1" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>OpennessToChange:</b>
                    <asp:Literal ID="Lvalues2" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Values2" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>Hedonism:</b>
                    <asp:Literal ID="Lvalues3" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Values3" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>SelfEnhancement:</b>
                    <asp:Literal ID="Lvalues4" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Values4" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>
                    <b>SelfTranscendence:</b>
                    <asp:Literal ID="Lvalues5" runat="server"></asp:Literal>%
                                        <div class="progress">
                                            <asp:Panel ID="Values5" runat="server" CssClass="progress-bar"></asp:Panel>
                                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <%-- Modal Certification --%>
    <div id="cert" class="modal fade" role="form">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Certificate</h4>
                </div>

                <%-- MODAL BODY --%>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Certification:</label>
                        <asp:DropDownList CssClass="form-control" ID="ddlCertification" runat="server">
                            <asp:ListItem>--Choose a Certification--</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Certification Author:</label>
                        <asp:TextBox ID="txtAuthor" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <label class="control-label">License Number:</label>
                        <asp:TextBox ID="txtLicense" CssClass="form-control" runat="server" type="Number" min="1" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Start Month:</label>
                        <asp:DropDownList CssClass="form-control" ID="ddlStartMonth" runat="server">
                            <asp:ListItem>--Choose a Month--</asp:ListItem>
                            <asp:ListItem Value="1">January</asp:ListItem>
                            <asp:ListItem Value="2">February</asp:ListItem>
                            <asp:ListItem Value="3">March</asp:ListItem>
                            <asp:ListItem Value="4">April</asp:ListItem>
                            <asp:ListItem Value="5">May</asp:ListItem>
                            <asp:ListItem Value="6">June</asp:ListItem>
                            <asp:ListItem Value="7">July</asp:ListItem>
                            <asp:ListItem Value="8">August</asp:ListItem>
                            <asp:ListItem Value="9">September</asp:ListItem>
                            <asp:ListItem Value="10">October</asp:ListItem>
                            <asp:ListItem Value="11">November</asp:ListItem>
                            <asp:ListItem Value="12">December</asp:ListItem>
                        </asp:DropDownList>
                    </div>

                    <div class="form-group">
                        <label class="control-label">End Month:</label>
                        <asp:DropDownList CssClass="form-control" ID="ddlEndMonth" runat="server">
                            <asp:ListItem>--Choose a Month--</asp:ListItem>
                            <asp:ListItem Value="1">January</asp:ListItem>
                            <asp:ListItem Value="2">February</asp:ListItem>
                            <asp:ListItem Value="3">March</asp:ListItem>
                            <asp:ListItem Value="4">April</asp:ListItem>
                            <asp:ListItem Value="5">May</asp:ListItem>
                            <asp:ListItem Value="6">June</asp:ListItem>
                            <asp:ListItem Value="7">July</asp:ListItem>
                            <asp:ListItem Value="8">August</asp:ListItem>
                            <asp:ListItem Value="9">September</asp:ListItem>
                            <asp:ListItem Value="10">October</asp:ListItem>
                            <asp:ListItem Value="11">November</asp:ListItem>
                            <asp:ListItem Value="12">December</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Start Year:</label>
                        <asp:TextBox ID="txtStartYear" CssClass="form-control" runat="server" type="Number" min="1" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">End Year:</label>
                        <asp:TextBox ID="txtEndYear" CssClass="form-control" runat="server" type="Number" min="1" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Expires?:</label>
                        <asp:TextBox ID="txtExpires" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Certification URL:</label>
                        <asp:TextBox ID="txtURL" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                    </div>
                </div>

                <%-- MODAL FOOTER --%>
                <div class="modal-footer">
                    <asp:Button ID="btncert" CssClass="btn btn-success btn-lg" Text="Save" runat="server" OnClick="btncert_Click" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal Interest --%>
    <div id="inter" class="modal fade" role="form">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Interest</h4>
                </div>

                <%-- MODAL BODY --%>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Interest:</label>
                        <asp:TextBox ID="txtInterest" runat="server" required></asp:TextBox>
                    </div>
                </div>

                <%-- MODAL FOOTER --%>
                <div class="modal-footer">
                    <asp:Button ID="btninter" CssClass="btn btn-success btn-lg" Text="Save" runat="server" OnClick="btninter_Click" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal Language --%>
    <div id="lang" class="modal fade" role="form">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Language</h4>
                </div>

                <%-- MODAL BODY --%>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Course:</label>
                        <asp:DropDownList CssClass="form-control" ID="ddlLanguage" runat="server">
                            <asp:ListItem>--Choose a Language--</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Proficiency:</label>
                        <asp:TextBox ID="txtProfiency" MaxLength="2" CssClass="form-control" runat="server" type="Number" min="1" required="required"></asp:TextBox>
                    </div>
                </div>

                <%-- MODAL FOOTER --%>
                <div class="modal-footer">
                    <asp:Button ID="btnlang" CssClass="btn btn-success btn-lg" Text="Save" runat="server" OnClick="btnlang_Click" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal Organization --%>
    <div id="org" class="modal fade" role="form">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Organization</h4>
                </div>

                <%-- MODAL BODY --%>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Organization Name:</label>
                        <asp:TextBox ID="txtOrganization" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Position Held:</label>
                        <asp:TextBox ID="txtPosition" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Association:</label>
                        <asp:TextBox ID="txtAssociation" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Start Month:</label>
                        <asp:DropDownList CssClass="form-control" ID="DropDownList1" runat="server">
                            <asp:ListItem>--Choose a Month--</asp:ListItem>
                            <asp:ListItem Value="1">January</asp:ListItem>
                            <asp:ListItem Value="2">February</asp:ListItem>
                            <asp:ListItem Value="3">March</asp:ListItem>
                            <asp:ListItem Value="4">April</asp:ListItem>
                            <asp:ListItem Value="5">May</asp:ListItem>
                            <asp:ListItem Value="6">June</asp:ListItem>
                            <asp:ListItem Value="7">July</asp:ListItem>
                            <asp:ListItem Value="8">August</asp:ListItem>
                            <asp:ListItem Value="9">September</asp:ListItem>
                            <asp:ListItem Value="10">October</asp:ListItem>
                            <asp:ListItem Value="11">November</asp:ListItem>
                            <asp:ListItem Value="12">December</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label class="control-label">End Month:</label>
                        <asp:DropDownList CssClass="form-control" ID="DropDownList2" runat="server">
                            <asp:ListItem>--Choose a Month--</asp:ListItem>
                            <asp:ListItem Value="1">January</asp:ListItem>
                            <asp:ListItem Value="2">February</asp:ListItem>
                            <asp:ListItem Value="3">March</asp:ListItem>
                            <asp:ListItem Value="4">April</asp:ListItem>
                            <asp:ListItem Value="5">May</asp:ListItem>
                            <asp:ListItem Value="6">June</asp:ListItem>
                            <asp:ListItem Value="7">July</asp:ListItem>
                            <asp:ListItem Value="8">August</asp:ListItem>
                            <asp:ListItem Value="9">September</asp:ListItem>
                            <asp:ListItem Value="10">October</asp:ListItem>
                            <asp:ListItem Value="11">November</asp:ListItem>
                            <asp:ListItem Value="12">December</asp:ListItem>

                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Start Year:</label>
                        <asp:TextBox ID="TextBox1" CssClass="form-control" runat="server" type="Number" min="1" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">End Year:</label>
                        <asp:TextBox ID="TextBox2" CssClass="form-control" runat="server" type="Number" min="1" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Description:</label>
                        <asp:TextBox ID="txtDescription" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ongoing:</label>
                        <asp:TextBox ID="txtOngoing" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                    </div>
                </div>

                <%-- MODAL FOOTER --%>
                <div class="modal-footer">
                    <asp:Button ID="btnorg" CssClass="btn-success btn-lg" Text="Save" runat="server" OnClick="btnorg_Click" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal Patent --%>
    <div id="pat" class="modal fade" role="form">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Project</h4>
                </div>

                <%-- MODAL BODY --%>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Patent Title:</label>
                        <asp:TextBox ID="txtTitle" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Application Number:</label>
                        <asp:TextBox ID="txtAppNum" CssClass="form-control" TextMode="Number" runat="server" required="required"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Issue Month:</label>
                        <asp:TextBox ID="txtIMonth" CssClass="form-control" TextMode="Number" runat="server" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Issue Day:</label>
                        <asp:TextBox ID="txtIDay" CssClass="form-control" TextMode="Number" runat="server" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Issue Year:</label>
                        <asp:TextBox ID="txtIYear" CssClass="form-control" TextMode="Number" runat="server" required="required"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <label class="control-label">File Month:</label>
                        <asp:TextBox ID="txtFMonth" CssClass="form-control" TextMode="Number" runat="server" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">File Day:</label>
                        <asp:TextBox ID="txtFDay" CssClass="form-control" TextMode="Number" runat="server" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">File Year:</label>
                        <asp:TextBox ID="txtFYear" CssClass="form-control" TextMode="Number" runat="server" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Patent Url:</label>
                        <asp:TextBox ID="TextBox3" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Status:</label>
                        <asp:TextBox ID="txtStatus" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                    </div>
                </div>

                <%-- MODAL FOOTER --%>
                <div class="modal-footer">
                    <asp:Button ID="btnpat" CssClass="btn btn-succes btn-lg" Text="Submit" runat="server" OnClick="btnpat_Click" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal Project --%>
    <div id="proj" class="modal fade" role="form">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Project</h4>
                </div>

                <%-- MODAL BODY --%>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Project Name:</label>
                        <asp:TextBox ID="txtProject" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Start Month:</label>
                        <asp:DropDownList CssClass="form-control" ID="DropDownList3" runat="server">
                            <asp:ListItem>--Choose a Month--</asp:ListItem>
                            <asp:ListItem Value="1">January</asp:ListItem>
                            <asp:ListItem Value="2">February</asp:ListItem>
                            <asp:ListItem Value="3">March</asp:ListItem>
                            <asp:ListItem Value="4">April</asp:ListItem>
                            <asp:ListItem Value="5">May</asp:ListItem>
                            <asp:ListItem Value="6">June</asp:ListItem>
                            <asp:ListItem Value="7">July</asp:ListItem>
                            <asp:ListItem Value="8">August</asp:ListItem>
                            <asp:ListItem Value="9">September</asp:ListItem>
                            <asp:ListItem Value="10">October</asp:ListItem>
                            <asp:ListItem Value="11">November</asp:ListItem>
                            <asp:ListItem Value="12">December</asp:ListItem>

                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label class="control-label">End Month:</label>
                        <asp:DropDownList CssClass="form-control" ID="DropDownList4" runat="server">
                            <asp:ListItem>--Choose a Month--</asp:ListItem>
                            <asp:ListItem Value="1">January</asp:ListItem>
                            <asp:ListItem Value="2">February</asp:ListItem>
                            <asp:ListItem Value="3">March</asp:ListItem>
                            <asp:ListItem Value="4">April</asp:ListItem>
                            <asp:ListItem Value="5">May</asp:ListItem>
                            <asp:ListItem Value="6">June</asp:ListItem>
                            <asp:ListItem Value="7">July</asp:ListItem>
                            <asp:ListItem Value="8">August</asp:ListItem>
                            <asp:ListItem Value="9">September</asp:ListItem>
                            <asp:ListItem Value="10">October</asp:ListItem>
                            <asp:ListItem Value="11">November</asp:ListItem>
                            <asp:ListItem Value="12">December</asp:ListItem>

                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Start Year:</label>
                        <asp:TextBox ID="TextBox4" CssClass="form-control" runat="server" type="Number" min="1" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">End Year:</label>
                        <asp:TextBox ID="TextBox5" CssClass="form-control" runat="server" type="Number" min="1" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ongoing:</label>
                        <asp:TextBox ID="TextBox6" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Project URL:</label>
                        <asp:TextBox ID="TextBox7" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                    </div>
                </div>

                <%-- MODAL FOOTER --%>
                <div class="modal-footer">
                    <asp:Button ID="btnproj" CssClass="btn-success btn-lg" Text="Save" runat="server" OnClick="btnproj_Click" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal Skills--%>
    <div id="skill" class="modal fade" role="form">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Skills</h4>
                </div>

                <%-- MODAL BODY --%>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Skill:</label>
                        <asp:DropDownList CssClass="form-control" ID="ddlSkills" runat="server">
                            <asp:ListItem>--Choose a Skill--</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <%-- MODAL FOOTER --%>
                <div class="modal-footer">
                    <asp:Button ID="btnskill" CssClass="btn btn-success btn-lg" Text="Save" runat="server" OnClick="btnskill_Click" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

