﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="User_Patent_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Patent</title>
    <link href="../../Content/font-awesome.min.css" rel="stylesheet" />
    <link href="../../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../../Content/materialize.min.css" rel="stylesheet" />
</head>
<body>
    <div class="col-lg-offset-3 col-lg-6">
        <div class="card">
            <div class="card-content">
                <div class="card-title">
                    <br />
                    <h4 class="center">Add Patent</h4>
                </div>
                <form id="form1" runat="server">
                        <div class="form-group">
                            <div class=" ">
                                <label class="control-label">Patent Title:</label>
                            </div>
                            <div class=" ">
                                <asp:TextBox ID="txtTitle" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class=" ">
                                <label class="control-label">Application Number:</label>
                            </div>
                            <div class=" ">
                                <asp:TextBox ID="txtAppNum" CssClass="form-control" TextMode="Number" runat="server" required="required"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class=" ">
                                <label class="control-label">Issue Month:</label>
                            </div>
                            <div class=" ">
                                <asp:TextBox ID="txtIMonth" CssClass="form-control" TextMode="Number" runat="server" required="required"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class=" ">
                                <label class="control-label">Issue Day:</label>
                            </div>
                            <div class=" ">
                                <asp:TextBox ID="txtIDay" CssClass="form-control" TextMode="Number" runat="server" required="required"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class=" ">
                                <label class="control-label">Issue Year:</label>
                            </div>
                            <div class=" ">
                                <asp:TextBox ID="txtIYear" CssClass="form-control" TextMode="Number" runat="server" required="required"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class=" ">
                                <label class="control-label">File Month:</label>
                            </div>
                            <div class=" ">
                                <asp:TextBox ID="txtFMonth" CssClass="form-control" TextMode="Number" runat="server" required="required"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class=" ">
                                <label class="control-label">File Day:</label>
                            </div>
                            <div class=" ">
                                <asp:TextBox ID="txtFDay" CssClass="form-control" TextMode="Number" runat="server" required="required"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class=" ">
                                <label class="control-label">File Year:</label>
                            </div>
                            <div class=" ">
                                <asp:TextBox ID="txtFYear" CssClass="form-control" TextMode="Number" runat="server" required="required"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class=" ">
                                <label class="control-label">Patent Url:</label>
                            </div>
                            <div class=" ">
                                <asp:TextBox ID="txtUrl" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class=" ">
                                <label class="control-label">Status:</label>
                            </div>
                            <div class=" ">
                                <asp:TextBox ID="txtStatus" CssClass="form-control" runat="server" required="required"></asp:TextBox>
                            </div>
                        </div>
                        <div class="card-action">
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <asp:Button ID="btnSubmit" CssClass="btn btn-succes pull-right btn-lg" Text="Submit" runat="server" OnClick="btnSubmit_Click" />
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>

</body>
</html>
