﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Patent_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string[] values = new string[] { "@PatentTitle", "@ApplicationNumber", "@IssueMonth", "@IssueDay", "@IssueYear", "@FileMonth", "@FileDay", "@FileYear", "@PatentUrl", "@Status" };
        string[] col = new string[] { "PatentTitle", "ApplicationNumber", "IssueMonth", "IssueDay", "IssueYear", "FileMonth", "FileDay", "FileYear", "PatentUrl", "Status" };
        Helper.Insert("tbl_Patent", values, col);
    }
}