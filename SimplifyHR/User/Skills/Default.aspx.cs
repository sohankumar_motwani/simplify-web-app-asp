﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Skills_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        getApplicantSkills();
    }

    void getApplicantSkills()
    {
        string sql = @"SELECT        dbo.tbl_Skills.Skill, tbl_Applicant.ApplicantID AS ApplicantID
FROM            dbo.tbl_Applicant INNER JOIN
                         dbo.tbl_ApplicantSkills ON dbo.tbl_Applicant.ApplicantID = dbo.tbl_ApplicantSkills.ApplicantID INNER JOIN
                         dbo.tbl_Skills ON dbo.tbl_ApplicantSkills.ApplicantSkillsID = dbo.tbl_Skills.SKillID WHERE ApplicantID = @ApplicantID";
        string[] session = new string[] { Session["ApplicantID"].ToString() };
        string[] filter = new string[] { "@ApplicantID" };

        Helper.Select(sql, lvSkills, filter, session);
    }
}