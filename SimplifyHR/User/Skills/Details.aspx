﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Details.aspx.cs" Inherits="User_Skills_Details" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../Content/font-awesome.min.css" rel="stylesheet" />
    <link href="../../Content/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <div class="col-lg-offset-3 col-lg-6">

        <form id="RegisterForm" runat="server">





            <div class="form-group">
                <div class="col-lg-4">
                    <label class="control-label">Skill:</label>
                </div>
                <div class="col-lg-8">
                    <asp:DropDownList CssClass="form-control" ID="ddlSkills" runat="server">
                        <asp:ListItem>--Choose a Skill--</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>




            <div class="form-group">
                <div class="col-lg-12">
                    <asp:Button ID="btnRegister" CssClass="btn-success pull-right btn-lg" Text="Save" runat="server" OnClick="btnRegister_Click" />
                </div>
            </div>
        </form>
    </div>
</body>

</html>
