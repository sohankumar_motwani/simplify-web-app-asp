﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Skills_Add : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        getSkills();
    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        string session = Session["ApplicantID"].ToString();
        string[] tablecols = new string[] { "ApplicantID", "ApplicantSkillsID" };
        string[] values = new string[] {"@ApplicantID", "@ApplicantSkillsID" };
        string[] textbox = new string[] { session, ddlSkills.SelectedValue};
        Helper.Insert("tbl_ApplicantSkills2", values, textbox, tablecols);
    }

    void getSkills()
    {
        string SQL = @"SELECT SKillID, Skill FROM tbl_Skills";
        Helper.Select(SQL, ddlSkills, "Skill", "SKillID");
    }
}