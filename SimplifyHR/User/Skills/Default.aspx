﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="User_Skills_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
  <div class="col-lg-12">
        <form id="form1" runat="server" class="form-horizontal">
            <table class="table table-hover">
                <thead>
                    <th>Skills:</th>
          

                    <th></th>
                </thead>
                <tbody>
                    <asp:ListView ID="lvSkills" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td><%# Eval("Skill") %></td>
                             
                                <td><%# Eval("ApplicantID") %></td>

                                <td>
                                    <a href='Details.aspx?ID=<%# Eval("ApplicantID") %>' class="btn btn-xs btn-info">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a href='Delete.aspx?ID=<%# Eval("ApplicantID") %>' class="btn btn-xs btn-info">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <tr colspan="16">
                                <td>
                                    <h2>No Records Available!</h2>
                                </td>
                            </tr>
                        </EmptyDataTemplate>
                    </asp:ListView>
                </tbody>
            </table>
        </form>
    </div>
</body>
</html>
