﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class User_Skills_Details : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ApplicationID"] == null)
        {
            Response.Redirect("~/index.aspx");
        }
        else
        {
            if (Request.QueryString["ID"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            else
            {
                int NewsID = 0;
                bool validNews = int.TryParse(Request.QueryString["ID"], out NewsID);
                if (validNews)
                {
                    if (!IsPostBack)
                    {
                        GetNewsData(NewsID);
                    }
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
        }
    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        string SQL = @"UPDATE tbl_ApplicantSkills SET ApplicantSkillsID = @ApplicantSkillsID WHERE ApplicantID = @ApplicantID";
        string[] cols = new string[] { "@ApplicantSkillsID", "@ApplicantID"};
        string[] values = new string[] { ddlSkills.SelectedValue, Session["ApplicantID"].ToString()};
        Helper.UpdateDelete(SQL, cols, values);

    }
    void GetNewsData(int ID)
    {
        using (SqlConnection conn = new SqlConnection(Helper.GetConnection()))
        {
            conn.Open();
            string SQL = @"SELECT        dbo.tbl_Skills.Skill AS Skill, tbl_Applicant.ApplicantID AS ApplicantID
FROM            dbo.tbl_Applicant INNER JOIN
                         dbo.tbl_ApplicantSkills ON dbo.tbl_Applicant.ApplicantID = dbo.tbl_ApplicantSkills.ApplicantID INNER JOIN
                         dbo.tbl_Skills ON dbo.tbl_ApplicantSkills.ApplicantSkillsID = dbo.tbl_Skills.SKillID WHERE ApplicantID = @ApplicantID";
            using (SqlCommand cmd = new SqlCommand(SQL, conn))
            {
                cmd.Parameters.AddWithValue("@ApplicantID", ID);
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                           
                            ddlSkills.SelectedIndex = ddlSkills.Items.IndexOf(ddlSkills.Items.FindByText(dr["Skill"].ToString()));
                        }
                    }
                    else
                    {
                        Response.Redirect("Default.aspx");
                    }
                }
            }
        }
    }
}