﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserMaster.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="User_Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Home</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="Server">

    <br />
                <div class="col-lg-3 col-lg-offset-1" style="float:left;">
                <div class="card">
                    <br />
                    <h4>&nbsp; Categories</h4>
                    <div class="row">
                        <asp:DropDownList runat="server" ID="ddlcat" CssClass="waves-effect waves-light btn-flat col-lg-8 col-lg-offset-1"
                            Style="border-style: solid; border-radius: 6px;">
                            <asp:ListItem>--- Select Item ----</asp:ListItem>
                            <asp:ListItem>Category1</asp:ListItem>
                            <asp:ListItem>Category2</asp:ListItem>
                            <asp:ListItem>Category3</asp:ListItem>
                            <asp:ListItem>Category4</asp:ListItem>
                            <asp:ListItem>Category5</asp:ListItem>
                            <asp:ListItem>Category6</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <hr />
                    <h4>&nbsp; Sort</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li>
                                    <asp:CheckBox runat="server" ID="test1" Checked="true" />

                                </li>
                                <li>
                                    <asp:CheckBox runat="server" ID="test2" />
                                    <label for="test2">Option 2</label>
                                </li>
                                <li>
                                    <asp:CheckBox runat="server" ID="test3" />
                                    <label for="test3">Option 3</label>
                                </li>
                                <li>
                                    <asp:CheckBox runat="server" ID="test4" />
                                    <label for="test4">Option 4</label>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <li>
                                    <asp:CheckBox runat="server" ID="test5" />
                                    <label for="test5">Option 5</label>
                                </li>
                                <li>
                                    <asp:CheckBox runat="server" ID="test6" />
                                    <label for="test6">Option 6</label>
                                </li>
                                <li>
                                    <asp:CheckBox runat="server" ID="test7" />
                                    <label for="test7">Option 7</label>
                                </li>
                                <li>
                                    <asp:CheckBox runat="server" ID="test8" />
                                    <label for="test8">Option 8</label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    <asp:ListView ID="lvJobPosting" runat="server">
        <ItemTemplate>
            <div class="col-lg-6 col-lg-offset-4" style="float:initial;">
                <div class="card">
                    <div class="card-image waves-effect waves-block waves-light">
                        <img src='<%# "../CompanyProfpic/" + Eval("ProfilePicture") %>' class="img-responsive activator" />
                    </div>
                    <div class="card-content">
                        <span class="card-title activator grey-text text-darken-4">Job: <%# Eval("JobTitle") %></span>
                        <hr />
                        <h5>Description: <%# Eval("JobDescription") %></h5>
                        <hr />
                        <p class="center">
                            <a href="CompanyProfile.aspx?ID=<%# Eval("CompanyID") %>" class="waves-effect waves-light btn-flat"  Style="border-style: solid; border-radius: 6px;">View Company</a>
                            <a class="activator waves-effect waves-light btn-flat"  Style="border-style: solid; border-radius: 6px;">View More</a>
                            <a href='ApplyJob.aspx?ID=<%# Eval("JobPostingID")%>' class="waves-effect waves-light btn-flat"  Style="border-style: solid; border-radius: 6px;" >Apply Job
                                </a>
                        </p>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4"><i class="fa fa-times right">close</i></span>
                        <h2><%# Eval("JobTitle") %></h2>

                        <hr />

                        <h3>More information:</h3>
                        <h5>
                            <br />
                            Educational Attainment: <%# Eval("EducationalAttainment") %><br />
                            Minimum Work Experience: <%#Eval("WorkExperience") %><br />
                            Required Prior Training: <%# Eval("PriorTraining") %><br />
                            Benefits: <%# Eval("Benefit") %><br />
                            Responsibilities: <%# Eval("Responsibilities") %><br />
                            Estimate Pay Range: <%# Eval("PayRange") %><br />
                            Skills Required: <%# Eval("Skill") %><br />
                            Location: <%# Eval("City") %> City
                            <br />
                            <br />
                            <div class="card-action">
                                <a href='ApplyJob.aspx?ID=<%# Eval("JobPostingID")%>' class="btn btn-default pull-right">Apply Job
                                </a>
                            </div>
                        </h5>
                    </div>
                </div>
            </div>
        </ItemTemplate>
        <EmptyDataTemplate>
            <div class="container">
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <h1 class="text-center">No Job Postings to display at the moment. You may want to check back later.</h1>
            </div>
        </EmptyDataTemplate>
    </asp:ListView>


</asp:Content>





