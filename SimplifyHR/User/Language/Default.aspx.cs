﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Language_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        getData();
    }

    void getData()
    {
        string session = Session["ApplicantID"].ToString();
        string[] filter = new string[] { "@UserID" };
        string[] filvalue = new string[] { session };
        string SQL = @"SELECT        dbo.tbl_ApplicantLanguage.Profiency, dbo.tbl_Language.Language, dbo.tbl_ApplicantLanguage.ApplicantID, dbo.tbl_ApplicantLanguage.LanguageID, dbo.tbl_Language.LanguageID AS LangID, 
                         dbo.tbl_Applicant.ApplicantID AS ID, dbo.tbl_ApplicantLanguage.ApplicantLanguageID
        FROM dbo.tbl_Applicant INNER JOIN
                         dbo.tbl_ApplicantLanguage ON dbo.tbl_Applicant.ApplicantID = dbo.tbl_ApplicantLanguage.ApplicantID INNER JOIN
                         dbo.tbl_Language ON dbo.tbl_ApplicantLanguage.LanguageID = dbo.tbl_Language.LanguageID
                           WHERE tbl_Applicant.ApplicantID = @UserID";
        Helper.Select(SQL, lvLanguage, filter , filvalue);
    }
}