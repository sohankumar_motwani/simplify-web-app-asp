﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

public partial class User_Language_Add : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        showLanguage();
    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        string[] values = new string[] { "@ApplicantID", "@LanguageID", "@Profiency" };
        string[] col = new string[] { "ApplicantID", "LanguageID", "Profiency" };
        string[] txt = new string[] { Session["ApplicantID"].ToString(), ddlLanguage.SelectedValue, txtProfiency.Text };
        Helper.Insert("tbl_ApplicantLanguage", values , txt, col);
    }

    void showLanguage()
    {

        string SQL = @"SELECT LanguageID, Language FROM tbl_Language";
        Helper.Select(SQL, ddlLanguage, "Language", "LanguageID");

    }
}