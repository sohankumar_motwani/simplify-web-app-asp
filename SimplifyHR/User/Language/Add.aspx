﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Add.aspx.cs" Inherits="User_Language_Add" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Course</title>
    <link href="../../Content/font-awesome.min.css" rel="stylesheet" />
    <link href="../../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../../Content/materialize.min.css" rel="stylesheet" />
</head>
<body>
    <div class="col-lg-offset-3 col-lg-6">
        <div class="card">
            <div class="card-title">
                <br />
                <h4 class="center">Add Course</h4>
            </div>
            <div class="card-content">
                <form id="RegisterForm" runat="server">
                    <div class="form-group">
                        <div class="col-lg-4">
                            <label class="control-label">Course:</label>
                        </div>
                        <div class="">
                            <asp:DropDownList CssClass="form-control" ID="ddlLanguage" runat="server">
                                <asp:ListItem>--Choose a Language--</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-4">
                            <label class="control-label">Proficiency:</label>
                        </div>
                        <div class="">
                            <asp:TextBox ID="txtProfiency" MaxLength="2" CssClass="form-control" runat="server" type="Number" min="1" required="required"></asp:TextBox>
                        </div>
                    </div>
                    <div class="card-action">
                        <div class="form-group">
                            <div class="col-lg-12">
                                <asp:Button ID="btnRegister" CssClass="btn btn-success pull-right btn-lg" Text="Save" runat="server" OnClick="btnRegister_Click" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
