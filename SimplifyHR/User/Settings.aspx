﻿<%@ Page Title="" Language="C#" MasterPageFile="~/User/UserMaster.master" AutoEventWireup="true" CodeFile="Settings.aspx.cs" Inherits="User_Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Settings</title>
    <link href="../css/AutoComplete.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="Server">
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function () {
            FB.init({
                appId: '114395319284537', // App ID
                status: true, // check login status
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true  // parse XFBML
            });
            // Additional initialization code here
            FB.Event.subscribe('auth.authResponseChange', function (response) {
                if (response.status === 'connected') {

                }
                else if (response.status === 'not_authorized') {
                    // the user is logged in to Facebook, 
                    // but has not authenticated your app
                    $("#btnFbLogin").click(function () {
                        FB.login(function (response) {
                            if (response.authResponse) {
                                // user sucessfully logged in
                                var accessToken = response.authResponse.accessToken;
                                // TODO: Handle the access token
                                // Do a post to the server to finish the logon
                                // This is a form post since we don't want to use AJAX

                                var form = document.createElement("form");
                                form.setAttribute("method", 'post');
                                form.setAttribute("action", 'User/FacebookLogin.ashx');

                                var field = document.createElement("input");
                                field.setAttribute("type", "hidden");
                                field.setAttribute("name", 'accessToken');
                                field.setAttribute("value", accessToken);
                                form.appendChild(field);

                                document.body.appendChild(form);
                                form.submit();

                            }
                        }, { scope: 'public_profile,email,user_posts' });
                    });
                } else {
                    // the user isn't logged in to Facebook.
                }
            });

        };

        // Load the SDK Asynchronously
        (function (d) {
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
        }(document));
    </script>
    <!--below code was taken from: https://developers.facebook.com/docs/facebook-login/web/login-button-->
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=114395319284537";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <div class="col-lg-offset-3 col-lg-6">
        <h2>Settings</h2>
    </div>
    <div class="col-lg-offset-3 col-lg-2">
        <div class="row">
            <ul class="nav nav-tabs  nav-stacked">
                <li class="active"><a data-toggle="tab" href="#menu2">Personal</a></li>
                <li><a data-toggle="tab" href="#menu4">Profile Picture</a></li>
                <li><a data-toggle="tab" href="#menu3">Social Media</a></li>
                <li><a data-toggle="tab" href="#menu5">Education</a></li>
                <li><a data-toggle="tab" href="#gen">Others</a></li>
                <li><a data-toggle="tab" href="#menu6">Tests</a></li>
            </ul>
        </div>
    </div>
    <div class="col-lg-offset-5 row col-lg-6">
        <asp:Label runat="server" ID="notification" CssClass="success"></asp:Label>
        <div class="card col-lg-offset-8">
            <div class="card-content">
                <div class="tab-content">
                    <div id="gen" class="tab-pane fade in ">
                        <h4>General Settings</h4>
                        <hr />
                        <label id="lblemail">Email</label>
                        <asp:TextBox ID="txtemail" runat="server" CssClass="form-control" placeholder="lorem.ipsum@gmail.com" TextMode="Email" Required></asp:TextBox>
                        <label id="lblpass">Change Password</label>
                        <asp:TextBox ID="txtchngePass" runat="server" CssClass="form-control" placeholder="*********" TextMode="Password" Required></asp:TextBox>
                        <label id="lblcpass">Confirm Password</label>
                        <asp:TextBox ID="txtconchngePass" runat="server" CssClass="form-control" placeholder="*********" TextMode="Password" Required></asp:TextBox>
                        <asp:Button ID="btnGen" OnClick="btnGen_Click" runat="server" CssClass="btn btn-primary pull-right" Text="Submit" />
                        <br />
                    </div>
                    <div id="menu4" class="tab-pane fade">
                        <h3>Profile Picture</h3>
                        <label id="lblProP">Profile Picture</label>
                        <input type="file" id="ProfilePicture" runat="server" name="pic" class="input-lg" accept="image/*| file_extension" />
                        <br />
                        <asp:Button ID="btnprofilepic" runat="server" CssClass="btn btn-primary pull-right" OnClick="btnprofilepic_Click" Text="Submit" />
                        <br />
                    </div>
                    <div id="menu2" class="tab-pane fade in active">
                        <h3>Personal</h3>
                        <hr />
                        <label id="lblMarS">MaritalStatus</label>
                        <asp:DropDownList runat="server" ID="ddlMarS" CssClass="form-control">
                            <asp:ListItem>Single</asp:ListItem>
                            <asp:ListItem>In A relationship</asp:ListItem>
                            <asp:ListItem>Married</asp:ListItem>
                        </asp:DropDownList>
                        <label id="lblStrA">StreetAddress</label>
                        <asp:TextBox ID="txtStrA" runat="server" CssClass="form-control" placeholder="12345 Avenida St."></asp:TextBox>
                        <label id="lblcountry">Country</label>
                        <asp:DropDownList runat="server" ID="ddlcountry" CssClass="form-control" Enabled="true" OnSelectedIndexChanged="ddlcountry_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                        <label id="lblregion">Region</label>
                        <asp:DropDownList runat="server" ID="ddlregion" CssClass="form-control" OnSelectedIndexChanged="ddlregion_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                        <label id="lblCity">City</label>
                        <asp:DropDownList runat="server" ID="ddlCity" CssClass="form-control">
                        </asp:DropDownList>
                        <label id="lblZipC">ZipCode</label>
                        <asp:TextBox ID="txtZipC" runat="server" CssClass="form-control" placeholder="1092"></asp:TextBox>
                        <label id="lblTel">Telephone</label>
                        <asp:TextBox ID="txtTel" runat="server" CssClass="form-control" placeholder="73432473"></asp:TextBox>
                        <label id="lblMob">Mobile</label>
                        <asp:TextBox ID="txtMob" runat="server" CssClass="form-control" placeholder="09273434545"></asp:TextBox>
                        <label id="lblCalT">CallTime</label>
                        <asp:TextBox ID="txtCalT" runat="server" CssClass="form-control" placeholder="12:00" TextMode="Time"></asp:TextBox>
                        <label id="lblCalM">CallMedium</label>
                        <asp:TextBox ID="txtCalM" runat="server" CssClass="form-control" placeholder="CellPhone"></asp:TextBox>
                        <label id="lblNat">Nationality</label>
                        <asp:TextBox ID="txtNat" runat="server" CssClass="form-control" placeholder="Filipino"></asp:TextBox>
                        <label id="lblWeb">Website</label>
                        <asp:TextBox ID="txtWeb" runat="server" CssClass="form-control" placeholder="wwww.loremipsum.com" TextMode="Url"></asp:TextBox>
                        <label id="lblSum">Summary</label>
                        <asp:TextBox ID="txtSum" runat="server" CssClass="form-control" placeholder="qwertyuikjhgfdsasdfghjkmgfdsg" TextMode="MultiLine"></asp:TextBox>
                        <br />
                        <asp:Button ID="btnPer" OnClick="btnPer_Click" runat="server" CssClass="btn btn-primary pull-right" Text="Submit" />
                        <br />
                    </div>
                    <div id="menu3" class="tab-pane fade">
                        <h3>Social Media</h3>
                        <div class="fb-login-button" data-max-rows="1" scope="public_profile,email,user_posts" data-size="large" data-button-type="continue_with" data-show-faces="false" onclick="checkLoginState();" data-auto-logout-link="true" data-use-continue-as="false" id="btnFBLogin"></div>
                        <br />
                        <label id="lblTwiT">Twitter (Ex: https://twitter.com/@Username)</label>
                        <asp:TextBox ID="txtTwiT" runat="server" CssClass="form-control"></asp:TextBox>
                        <label id="lblInsT">Instagram</label>
                        <asp:TextBox ID="txtInsT" runat="server" CssClass="form-control"></asp:TextBox>
                        <br />
                        <asp:Button ID="btnSM" runat="server" CssClass="btn btn-primary pull-right" OnClick="btnSM_Click" Text="Submit" />
                        <br />
                    </div>
                    <div id="menu5" class="tab-pane fade">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <h3 class="center">Education</h3>
                        <div class="form-group">
                            <label>Education Level<small>*</small></label>
                            <asp:DropDownList runat="server" ID="ddleduclvl" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label>Primary Education<small>*</small></label>

                            <asp:TextBox ID="txtpri" runat="server" CssClass="col-lg-10" autocomplete="off" placeholder="De La Salle GreenHills" />
                            <ajax:AutoCompleteExtender ID="autoComplete1" runat="server"
                                EnableCaching="true"
                                BehaviorID="AutoCompleteEx"
                                MinimumPrefixLength="1"
                                TargetControlID="txtpri"
                                ServicePath="../AutoComplete.asmx"
                                ServiceMethod="SearchCustomers"
                                CompletionInterval="10"
                                CompletionSetCount="20"
                                CompletionListCssClass="autocomplete_completionListElement"
                                CompletionListItemCssClass="autocomplete_listItem"
                                CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                DelimiterCharacters=";, :"
                                ShowOnlyCurrentWordInCompletionListItem="true">
                            </ajax:AutoCompleteExtender>
                        </div>
                        <div class="form-group">
                            <label>Secondary Education<small>*</small></label>
                            <asp:TextBox runat="server" ID="txtsec" CssClass="col-lg-10" placeholder="De La Salle GreenHills" autocomplete="off" />
                            <ajax:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server"
                                EnableCaching="true"
                                BehaviorID="AutoCompleteEx1"
                                MinimumPrefixLength="1"
                                TargetControlID="txtsec"
                                ServicePath="../AutoComplete.asmx"
                                ServiceMethod="SearchCustomers1"
                                CompletionInterval="10"
                                CompletionSetCount="20"
                                CompletionListCssClass="autocomplete_completionListElement"
                                CompletionListItemCssClass="autocomplete_listItem"
                                CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                DelimiterCharacters=";, :"
                                ShowOnlyCurrentWordInCompletionListItem="true">
                            </ajax:AutoCompleteExtender>
                        </div>

                        <div class="form-group">
                            <label>College<small>*</small></label>
                            <asp:TextBox runat="server" ID="txtcol" CssClass="col-lg-10" placeholder="De La Salle - College Of Saint Benilde" autocomplete="off" />
                            <ajax:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server"
                                EnableCaching="true"
                                BehaviorID="AutoCompleteEx2"
                                MinimumPrefixLength="1"
                                TargetControlID="txtcol"
                                ServicePath="../AutoComplete.asmx"
                                ServiceMethod="SearchCustomers"
                                CompletionInterval="10"
                                CompletionSetCount="20"
                                CompletionListCssClass="autocomplete_completionListElement"
                                CompletionListItemCssClass="autocomplete_listItem"
                                CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                DelimiterCharacters=";, :"
                                ShowOnlyCurrentWordInCompletionListItem="true">
                            </ajax:AutoCompleteExtender>
                        </div>
                        <div class="form-group">
                            <label>Degree Type<small>*</small></label>
                            <asp:DropDownList runat="server" ID="ddldegree" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label>Course<small>*</small></label>
                            <asp:TextBox runat="server" ID="txtcourse" CssClass="col-lg-10" placeholder="Information System" />
                        </div>
                        <br />
                        <br />
                        <br />
                        <br />

                        <asp:Button ID="btnEduc" runat="server" CssClass="btn btn-primary pull-right" OnClick="btnEduc_Click" Text="Submit" />
                        <br />
                        <br />
                        <br />
                    </div>
                    <div id="menu6" class="tab-pane fade">
                        <h3>Tests</h3>
                        <a href="Test.aspx" class="btn btn-flat center" Style="border-style: solid; border-radius: 6px;">General Tests</a>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

