﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="error.aspx.cs" Inherits="error" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/font-awesome.min.css" rel="stylesheet" />
    <link href="../Content/materialize.min.css" rel="stylesheet" />
</head>
<body style="background-color: whitesmoke">
    <nav class="navbar-default" id="navbar">
        <div class="nav-wrapper">
            <a href="index.aspx" class="brand-logo">&nbsp;<img src="images/SimplifyHR(CroppedTransBg).png" width="60%" /></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="index.aspx" style="color: black;">Home</a></li>
                <li><a href="about.aspx" style="color: black;">About</a></li>
            </ul>
        </div>
    </nav>
    <h1 class="center" style="color:red">Aw. Snap </h1>
    <h2 class="center" style="color:red">Something went wrong</h2>
    <h1 class="center" style="color:red">Please try again later</h1>
</body>
</html>
